
require ('json')

GameData = {
    createRoleData = nil,
    saveGameData = nil,
    loadGameData = nil,
    loginTestAccount = nil,
    data = {},
    currentRole = nil,
}

GameData.createRoleData = function(name,job,sex)

    local roleId = 10000001
    if #GameData.data > 0 then
        roleId = 10000002
    end

    local role = {id = roleId, name= name ,job = job,sex = sex, level = 1 ,mp = 400,hp = 50,exp = 0,equip = {},skill = {},map = 1,x = 40, y = 130, direction = 1,del = 0}

    if sex == 1 then
        --武器 上衣  翅膀
        role.equip = {1,4,0,0,0,0,0,0,0,0,0,0,4031,0}
    else
        role.equip = {1,5,0,0,0,0,0,0,0,0,0,0,4031,0}
    end

    if job == 1 then
        role.skill = {1000001,1002001,1003001,1004001,1005001,1006001,1008001,1009001,1010001,1102001}
    elseif job == 2 then
        role.skill = {2001001,2002001,2003001,2004001,2005001,2007001,2008001,2009001,2010001,2011001,2202001}
    elseif job == 3 then
        role.skill = {1001001,3001001,3002001,3003001,3004001,3006001,3007001,3008001,3009001,3010001,3011001,3012001,3303001}
    end

    table.insert(GameData.data,role)

    local userDefault = cc.UserDefault:getInstance()
    userDefault:setStringForKey("data", json.encode(GameData.data))
    cclog("保存角色数据" .. json.encode(GameData.data))
    userDefault:flush()
end

GameData.saveGameData = function()

    for i = 1,#GameData.data do
        if GameData.data[i].id == GameData.currentRole.id then
            GameData.data[i] = GameData.currentRole
        end
    end

    local userDefault = cc.UserDefault:getInstance()
    userDefault:setStringForKey("data", json.encode(GameData.data))
    cclog("保存游戏数据" .. json.encode(GameData.data))
    userDefault:flush()
end


GameData.loadGameData = function()

    local userDefault = cc.UserDefault:getInstance()
    local jsonData = userDefault:getStringForKey("data")
    cclog("读取游戏数据:" .. jsonData)

    if jsonData ~= nil and jsonData ~= "" then
        GameData.data = json.decode(jsonData)
    end

end

GameData.loginTestAccount = function()

    -- 测试账户
    local roleTest =  {id = 10000001, name= "一梦" ,job = 1,sex = 1,level = 1,hp = 200,mp = 25 ,exp = 0,equip = {},skill = {},map = 1,x = 107, y = 123, direction = 1,del = 0}

    roleTest.equip = {1,4,0,0,0,0,0,0,0,0,0,0,4031,0}

    --roleTest.skill = {1000001,1002001,1003001,1004001,1005001,1006001,1008001,1009001,1010001,1102001}
    --roleTest.skill = {2001001,2002001,2003001,2004001,2005001,2007001,2008001,2009001,2010001,2011001,2202001}
    roleTest.skill = {1001001,3001001,3002001,3003001,3004001,3006001,3007001,3008001,3009001,3010001,3011001,3012001,3303001}

    -- table 索引从1开始
    table.insert(GameData.data,roleTest)

    GameData.currentRole = GameData.data[1]

    cclog("加载游戏数据" .. json.encode(GameData.currentRole))

end