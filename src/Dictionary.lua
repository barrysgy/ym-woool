
---
--- Created by zhaoqp2019.
--- DateTime: 2020/2/14 14:05
---

--动作状态
ActionState = {
    none = 0,
    stand = 1,
    walk = 2,
    run = 3,
    attack = 4,
    hurt = 5,
    death = 6,
    casting = 7,
};


--方向
DirectionType = {
    LeftType = 1,
    LeftUpType = 2,
    UpType = 3,
    RightUpType = 4,
    RightType = 5,
    RightDownType = 6,
    DownType = 7,
    LeftDownType = 8,
};



--速度状态
SpeedState = {
    StayState = 0,
    NormalState = 1,
    QuickState = 2,
};

-- 角色类型
RoleType = {
    Player = 1,   -- 玩家
    Monster = 2,  -- 怪物
    Npc = 3,  -- NPC
    Pet = 4,  -- 宠物
};

--技能类型
SkillType = {
    Active = 1,   --主动技能
    UnActive = 2, --被动技能
    Buff = 3,     --状态技能
};

--伤害类型
AttackType = {
    Physics = 1,  --物理
    Magic = 2,    --魔法 道士也是魔法
};

--攻击模式
AttackMode = {
    Active = 1,   --主动
    UnActive = 2, --被动
};


--攻击方式
AttackStyle = {
    attack = 1,  -- 前方 attack
    attackSurround = 2, -- 环绕 attack
    attackMove = 3,  -- 位移技能攻击
    casting = 4,  -- 施法+弹道+击中
    castingSurround = 5,  -- 施法 周围伤害
    castingLoop = 6,  -- 施法 循环
    castingDuration = 7 -- 施法 持续伤害


};

--动画TAG
ActionTag = {
    Move = 1,
    AutoAttack = 2,
    RandomMove = 3
}

MoveStep = 6
TileWidth = 48
TileHeight = 32
TargetViewRange = 20 * 48  -- 脱离目标
TrackBottom = 48  -- 弹道高度


