
--------------------------------------------
---- Main Scene
--------------------------------------------
MainScene = class("MainScene",function() return cc.Scene:create() end)

require "layer/TestLayer"
require "layer/JoystickLayer"
require "layer/SkillLayer"
require "layer/MapLayer"
require "layer/MainUILayer"
require "layer/MinMapLayer"


function MainScene:ctor(mapId)
    self:init(mapId)
end

function MainScene:init(mapId)

    self._mapLayer = MapLayer.new(self,mapId)
    self:addChild(self._mapLayer)

    self._minMapLayer = MinMapLayer.new(self._mapLayer,mapId)
    self:addChild(self._minMapLayer)

    self._joystickLayer = JoystickLayer.new(self._mapLayer)
    self:addChild(self._joystickLayer)

    self._skillLayer = SkillLayer.new(self._mapLayer)
    self:addChild(self._skillLayer)

    self._mainUILayer = MainUILayer.new(self._mapLayer)
    self:addChild(self._mainUILayer)

end

function MainScene:updateMainUI()
    self._mainUILayer:updateUI()
end

