
--------------------------------------------
---- Login Scene
--------------------------------------------

local scale = 1.2

LoginBackgroundLayer = function()

    local bgLayer = cc.Layer:create()

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local bg = cc.Sprite:create("login/login_bg.png")
    bg:setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2)
    local scale = visibleSize.width / bg:getContentSize().width
    bg:setScale(scale)

    bgLayer:addChild(bg)

    local logo = cc.Sprite:create("logo.png")
    logo:setPosition(origin.x + logo:getContentSize().width * 0.5 + 20, origin.y + visibleSize.height - logo:getContentSize().height/2 - 20 )
    bgLayer:addChild(logo)

    -- 靠右边显示版本
    local versionLabel = UIHelper.createTTFLabel("当前版本 v1.0.0",20)
    versionLabel:setTextColor( cc.c3b(0, 250, 154) )
    versionLabel:setPosition( cc.p(visibleSize.width - versionLabel:getContentSize().width * 0.5 - 20,origin.y + visibleSize.height - versionLabel:getContentSize().height/2 -20) )
    bgLayer:addChild(versionLabel)

    return bgLayer
end


LoginInputLayer = function()

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()
    local loginLayer = cc.Layer:create()

    local userNameBackground = cc.Sprite:create("login/input_bg.png")
    userNameBackground:setScale(scale)
    userNameBackground:setPosition(origin.x + visibleSize.width/2,350 * scale)

    local passwordBackground = cc.Sprite:create("login/input_bg.png")
    passwordBackground:setScale(scale)
    passwordBackground:setPosition(origin.x + visibleSize.width/2,270 * scale)


    local editName = UIHelper.createEditBox("请输入用户名",cc.size(432, 64),20)
    local editPassword = UIHelper.createEditBox("请输入密码",cc.size(432, 64),20)
    editName:setPosition(origin.x + visibleSize.width/2 + 60 * scale,345 * scale)
    editPassword:setPosition(origin.x + visibleSize.width/2 + 60 * scale,265 * scale)

    editName:setText("ymgame")

    local startButton = UIHelper.createButton("登录",20,"login/button.png", "login/button_select.png","login/button_disable.png");
    startButton:setScale(scale)
    startButton:setPosition(origin.x + visibleSize.width/2,150 * scale)
    startButton:addClickEventListener(function(sender)
        if editName:getText() ~=  "" then

            GameData.loadGameData()

            UIHelper.toScene(RoleCreateSceneMain())

        else
            UIHelper.showToast(loginLayer,"请输入用户名！")
        end
    end)


    loginLayer:addChild(userNameBackground)
    loginLayer:addChild(passwordBackground)
    loginLayer:addChild(editName)
    loginLayer:addChild(editPassword)
    loginLayer:addChild(startButton)

    return loginLayer
end

function LoginSceneMain()
    local scene = cc.Scene:create()
    local bgLayer = LoginBackgroundLayer()
    local inputLayer = LoginInputLayer()
    scene:addChild(bgLayer, 0)
    scene:addChild(inputLayer, 1)
    return scene
end

