
--------------------------------------------
---- Loading Scene
--------------------------------------------

function LoadingSceneMain(mapId)
    local scene = cc.Scene:create()

    local loadingLayer = LoadingLayer.new(mapId)
    scene:addChild(loadingLayer)

    --预加载资源
    local cache = cc.SpriteFrameCache:getInstance()
    cache:removeUnusedSpriteFrames()

    local pList = {
        "effect/plist/select_player@0.plist",
        "effect/plist/select_monster@0.plist",
        "effect/plist/level_up@0.plist",
        "effect/plist/transform_blue@0.plist",
        "effect/plist/transform_done@0.plist",
        "effect/plist/auto_attack@0.plist",
        "effect/plist/auto_path@0.plist"
    }

    --基础数据
    local npcs = require("config/NPCUpdate")
    local monsters = require("config/MonsterUpdate")

    for i = 1,#npcs do
        if npcs[i].map == mapId then
            table.insert(pList,"npc/plist/m_".. npcs[i].model .."@0.plist")
        end
    end

    for i = 1,#monsters do
        if monsters[i].map == mapId then
            table.insert(pList,"monster/plist/m_".. monsters[i].model .."@0.plist")
        end
    end

    -- 道士宠物
    table.insert(pList,"monster/plist/m_20081@0.plist")
    table.insert(pList,"monster/plist/m_20082@0.plist")
    table.insert(pList,"monster/plist/m_20085@0.plist")

    --获得角色model 根据当前穿着的上衣决定
    --table.insert(pList,"role/plist/r_5110501@0.plist")
    cclog("加载角色模型资源：" .. GameData.currentRole.name .. ",model:" .. GameData.currentRole.equip[PLAYER_EQUIP_UPPERBODY])
    table.insert(pList,"role/plist/r_".. GameData.currentRole.equip[PLAYER_EQUIP_UPPERBODY] .."@0.plist")

    -- 提示
    local tips = require("config/Tips")

    local index = 1
    local loading = false
    Helper.schedule(loadingLayer._progressBar,function()
        if not loading then
            if index <= #pList then
                loading = true
                cache:addSpriteFrames(pList[index])
                cclog("加载资源:" .. pList[index])
                index = index + 1
                loadingLayer._progressBar:setPercentage(loadingLayer._progressBar:getPercentage() + 100/#pList)

                --更换提示
                if tips and index%4 == 0 then
                    local randomIndex = math.random(1, #tips)
                    loadingLayer._tipsLabel:setString(tips[randomIndex])
                end

                loading = false
            else
                loadingLayer._progressBar:stopAllActions()

                -- 开始加载主场景
                UIHelper.toScene(MainScene.new(mapId))
            end
        end

    end,0.2,0)

    return scene
end

