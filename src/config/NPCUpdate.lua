---
--- Created by zhaoqp2019.
--- DateTime: 2020/2/14 14:05
---NPC基础数据
---

local Items = {
    {id = 1,map = 1,x = 33,y = 120,name = '村长',direction = 7,dialog = '今天来往村里的人可真多',model = 10015,head = 10015,voice = 10028},
    {id = 2,map = 1,x = 82,y = 127,name = '红袖',direction = 7,dialog = '巾帼不让须眉',model = 10034,head = 10034,voice = 10012},
    {id = 3,map = 1,x = 21,y = 78,name = '服装店老板',direction = 7,dialog = '衣服可以提高你的生命与防御力',model = 10032,head = 10024,voice = 10005},
    {id = 4,map = 1,x = 9,y = 106,name = '杂货小贩',direction = 7,dialog = '在战斗中可以获得不错的战利品，可以用【拾取】按钮进行操作。',model = 10045,head = 10045,voice = 10003},
    {id = 5,map = 1,x = 53,y = 106,name = '首饰店老板',direction = 7,dialog = '我家首饰，质地上乘，雕琢细致，买来送人再合适不过了。',model = 10039,head = 10039,voice = 10004},
    {id = 6,map = 1,x = 45,y = 118,name = '老兵',direction = 7,dialog = '战斗经验需要积累才能得到提升。',model = 10035,head = 10035,voice = 10001,},
    {id = 7,map = 1,x = 67,y = 54,name = '茶馆老板',direction = 7,dialog = '客官，天气炎热，来碗凉茶吧？',model = 10031,head = 10031,voice = 10013},
    {id = 8,map = 1,x = 5,y = 135,name = '仓库管理员',direction = 7,dialog = '将物品存放在仓库里，去哪都安心了。',model = 10031,head = 10031,voice = 10050},
    {id = 9,map = 1,x = 128,y = 80,name = '异石',direction = 7,collect = 1,model = 10005},
    {id = 10,map = 1,x = 17,y = 42,name = '奇花',direction = 7,collect = 1,model = 10008},
    {id = 11,map = 1,x = 40,y = 26,name = '火莲',direction = 7,collect = 1,model = 10010},

};

return Items

