
---
--- Created by zhaoqp2019.
--- DateTime: 2020/2/14 14:05
--- 全部物品
---

local Items = {

	{id = 1,model = 5010101,name = '硬木剑',kind = 1,sex = 0,attack_min = 42,attack_max = 65},
	{id = 2,model = 5010101,name = '硬木剑',kind = 1,sex = 0,magic_attack_min = 42,magic_attack_max = 65},
	{id = 3,model = 5010101,name = '硬木剑',kind = 1,sex = 0,dc_attack_min = 42,dc_attack_max = 65},

	--通用衣服
	{id = 4,model = 5110501,name = '粗布衣(男)',kind = 5,sex = 1,max_hp = 340,defence_min = 8,defence_max = 12},
	{id = 5,model = 5210501,name = '粗布衣(女)',kind = 5,sex = 2,max_hp = 340,defence_min = 8,defence_max = 12},

	{id = 6,model = 5110502,name = '轻盔(男)',kind = 5,sex = 1,max_hp = 750,defence_min = 18,defence_max = 27},
	{id = 7,model = 5210502,name = '轻盔(女)',kind = 5,sex = 2,max_hp = 750,defence_min = 18,defence_max = 27},

	{id = 8,model = 5110512,name = '中盔(男)',kind = 5,sex = 1,max_hp = 1270,defence_min = 42,defence_max = 64},
	{id = 9,model = 5210512,name = '中盔(女)',kind = 5,sex = 2,max_hp = 1270,defence_min = 42,defence_max = 64},

	{id = 10,model = 5110503,name = '重盔(男)',kind = 5,sex = 1,max_hp = 3410,defence_min = 56,defence_max = 86},
	{id = 11,model = 5210503,name = '重盔(女)',kind = 5,sex = 2,max_hp = 3410,defence_min = 56,defence_max = 86},


	{id = 12,model = 5110513,name = '金鹏宝甲(男)',kind = 5,sex = 1,max_hp = 2410,defence_min = 84,defence_max = 149},
	{id = 13,model = 5210513,name = '金鹏宝甲(女)',kind = 5,sex = 2,max_hp = 2410,defence_min = 84,defence_max = 149},

	{id = 14,model = 5120503,name = '魔袍(男)',kind = 5,sex = 1,max_hp = 1710,defence_min = 56,defence_max = 86},
	{id = 15,model = 5220503,name = '魔袍(女)',kind = 5,sex = 2,max_hp = 1710,defence_min = 56,defence_max = 86},

	{id = 16,model = 5110504,name = '战神(男)',kind = 5,sex = 1,max_hp = 4580,defence_min = 75,defence_max = 116},
	{id = 17,model = 5210504,name = '战神(女)',kind = 5,sex = 2,max_hp = 4580,defence_min = 75,defence_max = 116},
	{id = 18,model = 5120504,name = '恶魔长袍(男)',kind = 5,sex = 1,max_hp = 2290,defence_min = 75,defence_max = 116},
	{id = 19,model = 5220504,name = '恶魔长袍(女)',kind = 5,sex = 2,max_hp = 2290,defence_min = 75,defence_max = 116},
	{id = 20,model = 5130503,name = '幽灵战甲(男)',kind = 5,sex = 1,max_hp = 3210,defence_min = 75,defence_max = 116},
	{id = 21,model = 5230503,name = '幽灵战甲(女)',kind = 5,sex = 2,max_hp = 3210,defence_min = 75,defence_max = 116},


	{id = 22,model = 5110505,name = '天魔战甲',kind = 5,sex = 1,max_hp = 5360,defence_min = 88,defence_max = 136},
	{id = 23,model = 5210505,name = '圣战战甲',kind = 5,sex = 2,max_hp = 5360,defence_min = 88,defence_max = 136},
	{id = 24,model = 5120505,name = '法神披风',kind = 5,sex = 1,max_hp = 2680,defence_min = 88,defence_max = 136},
	{id = 25,model = 5220505,name = '法神羽衣',kind = 5,sex = 2,max_hp = 2680,defence_min = 88,defence_max = 136},
	{id = 26,model = 5130504,name = '天尊道袍',kind = 5,sex = 1,max_hp = 3750,defence_min = 88,defence_max = 136},
	{id = 27,model = 5230504,name = '天师道袍',kind = 5,sex = 2,max_hp = 3750,defence_min = 88,defence_max = 136},

	{id = 28,model = 5130505,name = '天尊道袍*',kind = 5,sex = 1,max_hp = 3750,defence_min = 88,defence_max = 136},
	{id = 29,model = 5230505,name = '天师道袍*',kind = 5,sex = 2,max_hp = 3750,defence_min = 88,defence_max = 136},


	{id = 30,model = 5110511,name = '神武战甲',kind = 5,sex = 1,max_hp = 5850,defence_min = 96,defence_max = 148},
	{id = 31,model = 5210511,name = '神武战袍',kind = 5,sex = 2,max_hp = 5850,defence_min = 96,defence_max = 148},
	{id = 32,model = 5120510,name = '幻魔披风',kind = 5,sex = 1,max_hp = 2930,defence_min = 96,defence_max = 148},
	{id = 33,model = 5220510,name = '幻魔羽衣',kind = 5,sex = 2,max_hp = 2930,defence_min = 96,defence_max = 148},
	{id = 34,model = 5130510,name = '天玄道袍',kind = 5,sex = 1,max_hp = 4100,defence_min = 96,defence_max = 148},
	{id = 35,model = 5230510,name = '天玄道衣',kind = 5,sex = 2,max_hp = 4100,defence_min = 96,defence_max = 148},


	{id = 36,model = 5110507,name = '血煞战甲',kind = 5,sex = 1,max_hp = 7020,defence_min = 115,defence_max = 177},
	{id = 37,model = 5210507,name = '血煞战袍',kind = 5,sex = 2,max_hp = 7020,defence_min = 115,defence_max = 177},
	{id = 38,model = 5120507,name = '魔雷披风',kind = 5,sex = 1,max_hp = 3510,defence_min = 115,defence_max = 177},
	{id = 39,model = 5220507,name = '魔雷羽衣',kind = 5,sex = 2,max_hp = 3510,defence_min = 115,defence_max = 177},
	{id = 40,model = 5130507,name = '幽泉道袍',kind = 5,sex = 1,max_hp = 4910,defence_min = 115,defence_max = 177},
	{id = 41,model = 5230507,name = '幽泉道衣',kind = 5,sex = 2,max_hp = 4910,defence_min = 115,defence_max = 177},


	{id = 42,model = 5110508,name = '黄金战甲',kind = 5,sex = 1,max_hp = 2930,defence_min = 96,defence_max = 148},
	{id = 43,model = 5210508,name = '黄金战袍',kind = 5,sex = 2,max_hp = 2930,defence_min = 96,defence_max = 148},




	{id = 44,model = 2030610,name = '小手镯',kind = 6,sex = 0,dc_attack_min = 5,dc_attack_max = 8},
	{id = 45,model = 2030610,name = '小手镯',kind = 6,sex = 0,magic_attack_min = 5,magic_attack_max = 8},
	{id = 46,model = 2030610,name = '小手镯',kind = 6,sex = 0,attack_min = 5,attack_max = 8},
	{id = 47,model = 2030210,name = '珍珠戒指',kind = 2,sex = 0,dc_attack_min = 7,dc_attack_max = 11},
	{id = 48,model = 2030310,name = '凤凰项链',kind = 3,sex = 0,dc_attack_min = 14,dc_attack_max = 21},
	{id = 49,model = 2020210,name = '魔眼戒指',kind = 2,sex = 0,magic_attack_min = 7,magic_attack_max = 11},
	{id = 50,model = 2010210,name = '黑色水晶戒指',kind = 2,sex = 0,attack_min = 7,attack_max = 11},
	{id = 51,model = 2010310,name = '天鹰项链',kind = 3,sex = 0,attack_min = 14,attack_max = 21},
	{id = 52,model = 2030410,name = '鹿皮靴子',kind = 4,sex = 0,defence_min = 8,defence_max = 13},
	{id = 53,model = 2030810,name = '鹿皮腰带',kind = 8,sex = 0,max_hp = 200,magic_defence_min = 8,magic_defence_max = 12},
	{id = 54,model = 2020310,name = '魔镜',kind = 3,sex = 0,magic_attack_min = 14,magic_attack_max = 21},
	{id = 55,model = 2030702,name = '青铜头盔',kind = 7,sex = 0,magic_defence_min = 18,magic_defence_max = 28},

	--法师
	{id = 56,model = 5020103,name = '魔法权杖',kind = 1,sex = 0,magic_attack_min = 399,magic_attack_max = 614},
	{id = 57,model = 5020104,name = '骨玉',kind = 1,sex = 0,magic_attack_min = 467,magic_attack_max = 718},

	{id = 58,model = "fs_2",name = '法神头盔',kind = 7,sex = 0,magic_defence_min = 77,magic_defence_max = 119},
	{id = 59,model = "fs_3",name = '法神项链',kind = 3,sex = 0,magic_attack_min = 127,magic_attack_max = 196},
	{id = 60,model = "fs_4",name = '法神手镯',kind = 6,sex = 0,magic_attack_min = 47,magic_attack_max = 72},
	{id = 61,model = "fs_5",name = '法神戒指',kind = 2,sex = 0,magic_attack_min = 68,magic_attack_max = 105},
	{id = 62,model = "fs_6",name = '法神腰带',kind = 8,sex = 0,max_hp = 1310,magic_defence_min = 75,magic_defence_max = 116},
	{id = 63,model = "fs_7",name = '法神靴子',kind = 4,sex = 0,defence_min = 77,defence_max = 119},


	--道士
	{id = 64,model = 5030104,name = '龙纹',kind = 1,sex = 0,dc_attack_min = 467,dc_attack_max = 718},
	{id = 65,model = 5030110,name = '无极',kind = 1,sex = 0,dc_attack_min = 399,dc_attack_max = 614},

	{id = 66,model = "tz_2",name = '天尊头盔',kind = 7,sex = 0,magic_defence_min = 77,magic_defence_max = 119},
	{id = 67,model = "tz_3",name = '天尊项链',kind = 3,sex = 0,dc_attack_min = 127,dc_attack_max = 196},
	{id = 68,model = "tz_4",name = '天尊手镯',kind = 6,sex = 0,dc_attack_min = 47,dc_attack_max = 72},
	{id = 69,model = "tz_5",name = '天尊戒指',kind = 2,sex = 0,dc_attack_min = 68,dc_attack_max = 105},
	{id = 70,model = "tz_6",name = '天尊腰带',kind = 8,sex = 0,max_hp = 1840,magic_defence_min = 75,magic_defence_max = 116},
	{id = 71,model = "tz_7",name = '天尊道靴',kind = 4,sex = 0,defence_min = 77,defence_max = 119},


	--战士
	{id = 72,model = 5010110,name = '铜锤',kind = 1,sex = 0,attack_min = 399,attack_max = 614},
	{id = 73,model = 5010104,name = '裁决',kind = 1,sex = 0,attack_min = 467,attack_max = 718},

	{id = 74,model = "sz_2",name = '圣战头盔',kind = 7,sex = 0,magic_defence_min = 77,magic_defence_max = 119},
	{id = 75,model = "sz_3",name = '圣战项链',kind = 3,sex = 0,attack_min = 127,attack_max = 196},
	{id = 76,model = "sz_4",name = '圣战手镯',kind = 6,sex = 0,attack_min = 47,attack_max = 72},
	{id = 77,model = "sz_5",name = '圣战戒指',kind = 2,sex = 0,attack_min = 68,attack_max = 105},
	{id = 78,model = "sz_6",name = '圣战腰带',kind = 8,sex = 0,max_hp = 2620,magic_defence_min = 75,magic_defence_max = 116},
	{id = 79,model = "sz_7",name = '圣战靴子',kind = 4,sex = 0,defence_min = 77,defence_max = 119},


};
return Items
