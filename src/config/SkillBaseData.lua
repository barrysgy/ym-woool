---
--- Created by zhaoqp2019.
--- DateTime: 2020/2/15 12:29
---

local Items = {

    {id = 1000,name = '普通攻击',job = 0,icon = 1001,sound = 5,skill_type = 1,book = 0,attack_type = 1,use_distance = 1,attack_style = 1,hurt_range = 1,hate = 100},
    {id = 1001,name = '普通攻击',job = 0,icon = 1001,sound = 5,skill_type = 1,book = 0,attack_type = 1,use_distance = 1,attack_style = 1,hurt_range = 1,hate = 100},

    {id = 1002,name = '攻杀剑术',job = 1,icon = 1002,sound = 7121,skill_type = 1,book = 1511,attack_type = 1,use_distance = 3,attack_style = 1,hurt_range = 3,hate = 100},
    {id = 1003,name = '刺杀剑术',job = 1,icon = 1003,sound = 1003,skill_type = 1,book = 1512,attack_type = 1,use_distance = 3,attack_style = 1,hurt_range = 3,hate = 100},
    {id = 1004,name = '抱月刀',job = 1,icon = 1004,sound = 1004,skill_type = 1,book = 1513,attack_type = 1,use_distance = 2,attack_style = 2,hurt_range = 2,hate = 100},
    {id = 1005,name = '野蛮冲撞',job = 1,icon = 1005,sound = 1005,skill_type = 1,book = 6003,attack_type = 1,use_distance = 4,attack_style = 3,hurt_range = 1,hate = 100},
    {id = 1006,name = '烈火剑法',job = 1,icon = 1006,sound = 1006,skill_type = 1,book = 6000,attack_type = 1,use_distance = 3,attack_style = 1,hurt_range = 3,hate = 100},
    {id = 1008,name = '金刚护体',job = 1,icon = 1008,sound = 1008,skill_type = 3,book = 6009,attack_type = 1,use_distance = 9,attack_style = 6,hurt_range = 1,hate = 100},
    {id = 1009,name = '破盾斩',job = 1,icon = 1009,sound = 70571,skill_type = 1,book = 6200010,attack_type = 1,use_distance = 2,attack_style = 2,hurt_range = 1,hate = 100},
    {id = 1010,name = '突斩',job = 1,icon = 1010,sound = 1010,skill_type = 1,book = 6200009,attack_type = 1,use_distance = 4,attack_style = 3,hurt_range = 1,hate = 100},
    {id = 1102,name = '强化攻杀',job = 1,icon = 1102,sound = 1002,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 5,attack_style = 1,hurt_range = 5,hate = 100},

    {id = 2001,name = '小火球',job = 2,icon = 2001,sound = 70191,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 2002,name = '雷电术',job = 2,icon = 2002,sound = 70231,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 2003,name = '地狱雷光',job = 2,icon = 2003,sound = 70291,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 2,attack_style = 5,hurt_range = 2,hate = 100},
    {id = 2004,name = '魔法盾',job = 2,icon = 2004,sound = 2004,skill_type = 3,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 6,hurt_range = 0,hate = 100},
    {id = 2005,name = '抗拒火环',job = 2,icon = 2005,sound = 2005,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 2,attack_style = 5,hurt_range = 2,hate = 100},
    {id = 2007,name = '火墙',job = 2,icon = 2007,sound = 70271,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 7,hurt_range = 2,hate = 100},
    {id = 2008,name = '冰咆哮',job = 2,icon = 2008,sound = 70321,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 2,hate = 100},
    {id = 2009,name = '风影盾',job = 2,icon = 2009,sound = 70581,skill_type = 3,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 6,hurt_range = 0,hate = 100},
    {id = 2010,name = '狂龙紫电',job = 2,icon = 2010,sound = 2010,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 2011,name = '流星火雨',job = 2,icon = 2011,sound = 2011,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 7,hurt_range = 2,hate = 100},
    {id = 2202,name = '强化火球',job = 2,icon = 2202,sound = 70191,skill_type = 1,book = 6200008,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},

    {id = 3001,name = '集体隐身术',job = 3,icon = 3001,sound = 70121,skill_type = 3,book = 1518,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 2,hate = 100},
    {id = 3002,name = '灵魂道符',job = 3,icon = 3002,sound = 70091,skill_type = 1,book = 1519,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 3003,name = '群体治愈术',job = 3,icon = 3003,sound = 70171,skill_type = 3,book = 6002,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 2,hate = 100},
    {id = 3004,name = '施毒术',job = 3,icon = 3004,sound = 3004,skill_type = 1,book = 1520,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 3006,name = '神圣战甲术',job = 3,icon = 3006,sound = 3006,skill_type = 3,book = 1521,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 2,hate = 100},
    {id = 3007,name = '召唤神兽',job = 3,icon = 3007,sound = 3007,skill_type = 3,book = 5999,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 3008,name = '骷髅召唤术',job = 3,icon = 3008,sound = 3008,skill_type = 3,book = 1523,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 3009,name = '狮子吼',job = 3,icon = 3009,sound = 3009,skill_type = 1,book = 1522,attack_type = 1,use_distance = 4,attack_style = 4,hurt_range = 1,hate = 10},
    {id = 3010,name = '斗转星移',job = 3,icon = 3010,sound = 70191,skill_type = 3,book = 6008,attack_type = 1,use_distance = 10,attack_style = 6,hurt_range = 1,hate = 100},
    {id = 3011,name = '幽冥火咒',job = 3,icon = 3011,sound = 3011,skill_type = 1,book = 6200022,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 3012,name = '强化骷髅',job = 3,icon = 3012,sound = 70623,skill_type = 3,book = 6200023,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},
    {id = 3303,name = '强化施毒',job = 3,icon = 3303,sound = 70081,skill_type = 1,book = 6200021,attack_type = 1,use_distance = 10,attack_style = 4,hurt_range = 1,hate = 100},


};
return Items
