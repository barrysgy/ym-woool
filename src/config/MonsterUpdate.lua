---
--- Created by zhaoqp2019.
--- DateTime: 2020/2/14 14:05
---地图刷新怪物配置
---

local Items = {

    -- 大刀守卫
    {id = 1000,map = 1,model= "20036",base_id = 1,monster_count = 1,x = 108,y = 115,radius = 0,direction = 3,move_speed = 215},
    {id = 1001,map = 1,model= "20036",base_id = 1,monster_count = 1,x = 115,y = 122,radius = 0,direction = 3,move_speed = 215},

    {id = 1010,map = 1,model= "20097",base_id = 6,monster_count = 5,x = 95,y = 130,radius = 10,direction = 3,move_speed = 215},
    {id = 1020,map = 1,model= "20046",base_id = 7,monster_count = 5,x = 18,y = 88,radius = 10,direction = 3,move_speed = 215},
    {id = 1030,map = 1,model= "20112",base_id = 5,monster_count = 5,x = 57,y = 125,radius = 20,direction = 1,move_speed = 215},
    {id = 1040,map = 1,model= "20038",base_id = 3,monster_count = 5,x = 66,y = 66,radius = 10,direction = 5,move_speed = 215},
    {id = 1050,map = 1,model= "20038",base_id = 3,monster_count = 5,x = 85,y = 60,radius = 10,direction = 5,move_speed = 215},
    {id = 1060,map = 1,model= "20072",base_id = 4,monster_count = 5,x = 20,y = 130,radius = 10,direction = 5,move_speed = 215},
    {id = 1070,map = 1,model= "20113",base_id = 8,monster_count = 5,x = 110,y = 60,radius = 10,direction = 5,move_speed = 215},
    {id = 1080,map = 1,model= "20113",base_id = 8,monster_count = 5,x = 120,y = 65,radius = 10,direction = 5,move_speed = 215},
    {id = 1090,map = 1,model= "20111",base_id = 9,monster_count = 5,x = 115,y = 100,radius = 10,direction = 5,move_speed = 215},
    {id = 1100,map = 1,model= "20111",base_id = 9,monster_count = 5,x = 100,y = 75,radius = 10,direction = 5,move_speed = 215},
    {id = 1110,map = 1,model= "20066",base_id = 10,monster_count = 5,x = 75,y = 56,radius = 10,direction = 5,move_speed = 215},
    {id = 1120,map = 1,model= "20066",base_id = 10,monster_count = 5,x = 49,y = 50,radius = 10,direction = 5,move_speed = 215},

    -- 弓箭手


};

return Items

