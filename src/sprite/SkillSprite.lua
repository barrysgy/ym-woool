
--------------------------------------------
---- SkillSprite
--------------------------------------------
SkillSprite = class("SkillSprite", function()
    return cc.Sprite:create()
end)

function SkillSprite:ctor(mapLayer,effectLayer)
    self._mapLayer = mapLayer
    self._effectLayer = effectLayer
    self:init()
end


function SkillSprite:init()
    self._mainSprite = cc.Sprite:create()
    self._mainSprite:setAnchorPoint(cc.p(0.5,0.48))
    self._mainSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._mainSprite)

    --空闲状态
    self:setVisible(false)
end


function SkillSprite:updateEffect(masterSprite,skill)

    self._skill = skill
    self._direction = masterSprite._roleData.direction

    cclog("施放技能者：" .. masterSprite._roleData.name)
    cclog("技能名称：" .. self._skill.name .."," .. self._skill.base_id)
    cclog("施放技能者方向：" .. masterSprite._roleData.direction)

    --精灵技能发生移动后回到零点上
    self._mainSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self._mainSprite:setVisible(false)
    self._mainSprite:setTexture("main/sprite.png")
    local rad,rolePngName,flippedX = Helper.getSpriteFrameProperty(self._direction)

    UIHelper.addEffectWithMode(self._mainSprite,2)

    --技能特效动画
    local castingPath = nil
    if self._skill.attack_style == AttackStyle.casting
            or self._skill.attack_style == AttackStyle.castingSurround
            or self._skill.attack_style == AttackStyle.castingLoop
            or self._skill.attack_style == AttackStyle.castingDuration then
        castingPath = self._skill.base_id .. "/casting/000"
        rolePngName = "000"
    else
        castingPath = self._skill.base_id .. "/attack/" .. rolePngName  --1002/attack/00000.png
    end

    --攻击速度或者施法速度
    local frameSpeed,actionSpeed = Numerical.getAttackFrameSpeed(masterSprite._roleData.base_attack_speed,masterSprite._roleData.attack_speed)

    local animationCache = cc.AnimationCache:getInstance()

    -- 获取casting 或者 attack
    local castingAnimation = animationCache:getAnimation(castingPath)
    if castingAnimation == nil then
        local animFramesCasting = Helper.getAnimFrames(castingPath,15)
        cclog("animFramesCasting：" .. castingPath)
        if #animFramesCasting > 0 then
            local frameTime = frameSpeed/#animFramesCasting
            if self._skill.base_id == 1010 or self._skill.base_id == 1005 then
                frameTime  = frameTime * 0.5
            end
            cclog("攻击施法速度：" .. frameTime)
            castingAnimation = cc.Animation:createWithSpriteFrames(animFramesCasting, frameTime)
            animationCache:addAnimation(castingAnimation,castingPath)
        end
    end

    local castingAnimate = nil
    if castingAnimation ~= nil then
        castingAnimate = cc.Animate:create(castingAnimation)
    else
        cclog("攻击施法延迟" .. frameSpeed)
        castingAnimate = cc.DelayTime:create(frameSpeed * 0.5)
    end

    local hitPath = self._skill.base_id .. "/hit/000"
    local hitAnimation = animationCache:getAnimation(hitPath)
    if hitAnimation == nil then
        local animFramesHit =  Helper.getAnimFrames(hitPath,15)
        hitAnimation = cc.Animation:createWithSpriteFrames(animFramesHit, 0.10)
        animationCache:addAnimation(hitAnimation,hitPath)
    end


    --设置为可见
    self._mainSprite:setFlippedX(flippedX)
    self:setVisible(true)

    local targetSprite = masterSprite._targetSprite

    ----------------------------------------------------------------------
    if self._skill.attack_style == AttackStyle.attack then

        if hitAnimation ~= nil then
            local hitAnimate = cc.Animate:create(hitAnimation)
            local sequenceHit = cc.Sequence:create(castingAnimate,hitAnimate,cc.CallFunc:create(function()
                self:skillHurt(masterSprite)
                self:skillFinish()

            end))
            self._mainSprite:runAction(sequenceHit)
            self._mainSprite:setVisible(true)
        else

            if castingAnimation ~= nil then
                local sequenceCasting = cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()
                    self:skillHurt(masterSprite)
                    self:skillFinish()
                end))
                self._mainSprite:runAction(sequenceCasting)
                self._mainSprite:setVisible(true)
            else
                --没有特效的技能普通攻击
                self:skillHurt(masterSprite)
                self:skillFinish()
            end
        end
    elseif self._skill.attack_style == AttackStyle.attackMove then

        if hitAnimation ~= nil then
            local hitAnimate = cc.Animate:create(hitAnimation)
            local sequenceHit = cc.Sequence:create(castingAnimate,hitAnimate,cc.CallFunc:create(function()
                self:skillHurt(masterSprite)
                self:skillFinish()

            end))
            self._mainSprite:runAction(sequenceHit)
            self._mainSprite:setVisible(true)
        else

            if castingAnimation ~= nil then
                local sequenceHit = cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()
                    self:skillHurt(masterSprite)
                    self:skillFinish()
                end))
                self._mainSprite:runAction(sequenceHit)
                self._mainSprite:setVisible(true)
            else
                --没有特效只有移动的技能  野蛮冲撞  没有伤害
                self:skillFinish()
            end

        end

    elseif self._skill.attack_style == AttackStyle.attackSurround then

        local sequenceCasting = cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()
            self:skillHurt(masterSprite)
            self:skillFinish()
        end))
        self._mainSprite:runAction(sequenceCasting)
        self._mainSprite:setVisible(true)

    elseif self._skill.attack_style == AttackStyle.casting then

        --默认目标位置
        local posX,posY = self:getPosition()
        local distance = self._skill.use_distance * TileWidth
        local targetX = posX + math.cos(rad) * distance
        local targetY = posY + math.sin(rad) * distance * 2/3
        cclog("默认目标的位置:" .. targetX .. "," .. targetY)

        if self._skill.move_speed  > 0 then

            --有没有弹道
            local trackPath = self._skill.base_id .. "/track/000"
            local trackAnimation = animationCache:getAnimation(trackPath)
            if trackAnimation == nil then
                local animFramesTrack =  Helper.getAnimFrames(trackPath,10)
                cclog("弹道帧数：" .. #animFramesTrack)
                trackAnimation = cc.Animation:createWithSpriteFrames(animFramesTrack, frameSpeed/#animFramesTrack)
                animationCache:addAnimation(trackAnimation,trackPath)

            end

            --弹道移动函数
            local function moveAction()

                cclog("弹道上移.................")
                local posX,posY = self:getPosition()
                self:setPosition(posX,posY + TrackBottom)

                local step = 24
                local count = distance / step
                local current = 0
                Helper.schedule(self,function()

                    local posX,posY = self:getPosition()

                    if targetSprite ~= nil then
                        targetX,targetY = targetSprite:getPosition()
                        cclog("目标精灵的位置:" .. targetX .. "," .. targetY)
                        --追踪转向 总距离不变
                        rad = Helper.getRad(cc.p(posX,posY),cc.p(targetX,targetY + TrackBottom))
                    end

                    local stepX = math.cos(rad) * step
                    local stepY = math.sin(rad) * step * 2/3

                    self:setPosition(posX + stepX,posY + stepY)

                    local posX2,posY2 = self:getPosition()

                    current = current + 1

                    --有碰撞提前结束飞行
                    local hurtRect = Helper.computeHurtRect(cc.p(posX2,posY2 - TrackBottom),self._direction,self._skill)
                    --self._effectLayer:updateHurtRangRect(hurtRect)
                    local collision,position = self._mapLayer:isSkillCollision(masterSprite,hurtRect)

                    if collision and position ~= nil then
                        posX2 = position.x
                        posY2 = position.y + TrackBottom
                        self:setPosition(posX2,posY2)
                    end

                    --提前与目标发生碰撞
                    if current == count or collision then

                        -- 停止移动
                        self:stopAllActions()


                        self:skillHurt(masterSprite,cc.p(posX2,posY2 - TrackBottom))

                        --击中
                        if hitAnimation ~= nil then

                            --特殊处理的技能是 hit状态的是个圆形 需要定位到地面上
                            if self._skill.base_id == 3006 then
                                self:setPosition(posX2,posY2 - TrackBottom)
                                cclog("弹道下移.................")

                            end

                            local hitAnimate = cc.Animate:create(hitAnimation)
                            local sequenceHit = cc.Sequence:create(hitAnimate,cc.CallFunc:create(function()
                                -- 产生伤害
                                if self._skill.base_id ~= 3006 then
                                    self:setPosition(posX2,posY2 - TrackBottom)
                                    cclog("弹道下移.................")

                                end

                                self:skillFinish()
                            end))

                            self._mainSprite:stopAllActions()
                            self._mainSprite:runAction(sequenceHit)
                            self._mainSprite:setVisible(true)
                        else
                            local posX,posY = self:getPosition()
                            self:setPosition(posX,posY - TrackBottom)
                            cclog("弹道下移.................")
                            self:skillHurt(masterSprite)
                            self:skillFinish()
                        end

                    end
                end,self._skill.move_speed * actionSpeed,0)
            end

            if trackAnimation ~= nil then
                cclog("有弹道...............................")
                local trackAnimate = cc.Animate:create(trackAnimation)

                --施法位置有偏移的  幽冥火咒  灵魂道符
                if  self._skill.base_id == 3002 then
                    self:setPosition(posX,posY + 30)
                elseif self._skill.base_id == 3011 then
                    self:setPosition(posX,posY + 20)
                end

                self._mainSprite:runAction(cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()
                    if self._skill.base_id == 3011 or self._skill.base_id == 3002 then
                        self:setPosition(posX,posY)
                    end
                    moveAction()

                end),trackAnimate))
                self._mainSprite:setVisible(true)

            else
                --施法
                self._mainSprite:runAction(castingAnimate,cc.CallFunc:create(function()
                    moveAction()
                end))
            end


        else
            --没有弹道的情况 施法 + 击中

            --默认目标位置
            local posX,posY = self:getPosition()
            local distance = self._skill.use_distance * TileWidth
            local targetX = posX + math.cos(rad) * distance
            local targetY = posY + math.sin(rad) * distance * 2/3
            cclog("默认目标的位置:" .. targetX .. "," .. targetY)

            local hitAnimate = cc.Animate:create(hitAnimation)

            --施法 + 击中
            local sequenceHit = cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()

                if targetSprite ~= nil then
                    local posX,posY = targetSprite:getPosition()
                    targetX = posX
                    targetY = posY
                end

                -- 伤害
                self:skillHurt(masterSprite,cc.p(targetX,targetY))

                --紫电  狮子吼  图片有偏移
                if self._skill.base_id == 2010 or self._skill.base_id == 3009  or self._skill.base_id == 3004 or self._skill.base_id == 3303 then
                    cclog("移动到人物的区域 加1")
                    targetY = targetY + TrackBottom
                elseif self._skill.base_id == 3008 then
                    --骷髅召唤往上面移动一点
                    targetY = targetY + 18
                elseif self._skill.base_id == 3012 then
                    --强化骷髅往上面移动一点
                    targetY = targetY + 16
                end
                self:setPosition(targetX,targetY)

            end),hitAnimate,cc.CallFunc:create(function()

                if self._skill.base_id == 2010 or self._skill.base_id == 3009 or self._skill.base_id == 3004  or self._skill.base_id == 3303 then
                    cclog("移动到人物的区域 减1")
                    self:setPosition(targetX,targetY - TrackBottom)
                elseif self._skill.base_id == 3008 then
                    self:setPosition(targetX,targetY - 18)
                elseif self._skill.base_id == 3012 then
                    self:setPosition(targetX,targetY - 16)
                end
                self:skillFinish()
            end))
            self._mainSprite:runAction(sequenceHit)
            self._mainSprite:setVisible(true)

        end

    elseif self._skill.attack_style == AttackStyle.castingSurround then

        local hitAnimate = cc.Animate:create(hitAnimation)
        -- 施法 + 击中
        local sequenceCasting = cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()
            self:skillHurt(masterSprite)
        end),hitAnimate,cc.CallFunc:create(function()
            self:skillFinish()
        end))
        self._mainSprite:runAction(sequenceCasting)
        self._mainSprite:setVisible(true)

    elseif self._skill.attack_style == AttackStyle.castingDuration then

        local posX,posY = self:getPosition()
        local distance = self._skill.use_distance * TileWidth
        local targetX = posX + math.cos(rad) * distance
        local targetY = posY + math.sin(rad) * distance * 2/3
        cclog("默认目标的位置:" .. targetX .. "," .. targetY)

        -- 火墙
        if self._skill.base_id == 2007 then

            local sequenceHit = cc.Sequence:create(castingAnimate,cc.CallFunc:create(function()

                if targetSprite ~= nil then
                    local posX,posY = targetSprite:getPosition()
                    targetX = posX
                    targetY = posY
                end

                self:setPosition(targetX,targetY)

                --生成周围矩形
                for i = 1,9 do
                    local sprite1 = cc.Sprite:create();
                    UIHelper.addEffectWithMode(sprite1,2)
                    local hitAnimate = cc.Animate:create(hitAnimation)
                    if i==1 then
                        sprite1:setPosition(targetX-TileWidth,targetY + TileHeight)
                    elseif i==2 then
                        sprite1:setPosition(targetX,targetY + TileHeight)
                    elseif i==3 then
                        sprite1:setPosition(targetX+TileWidth,targetY + TileHeight)
                    elseif i==4 then
                        sprite1:setPosition(targetX-TileWidth,targetY)
                    elseif i==5 then
                        sprite1:setPosition(targetX,targetY)
                    elseif i==6 then
                        sprite1:setPosition(targetX+TileWidth,targetY)
                    elseif i==7 then
                        sprite1:setPosition(targetX-TileWidth,targetY - TileHeight)
                    elseif i==8 then
                        sprite1:setPosition(targetX,targetY - TileHeight)
                    elseif i==9 then
                        sprite1:setPosition(targetX+TileWidth,targetY - TileHeight)
                    end
                    sprite1:runAction(cc.RepeatForever:create(hitAnimate))
                    self._mapLayer:addChild(sprite1,0,100+i)
                    self._mapLayer:setCameraMask(cc.CameraFlag.USER1)

                end

            end))
            self._mainSprite:runAction(sequenceHit)
            self._mainSprite:setVisible(true)

            local durationTime = 0
            Helper.schedule(self._mainSprite,function()

                if durationTime > self._skill.duration_time then
                    for i = 1,9 do
                        local sprite1 = self._mapLayer:getChildByTag(100 + i)
                        if sprite1 then
                            sprite1:removeFromParent()
                        end
                    end

                    self:skillFinish()
                end

                durationTime = durationTime + self._skill.per_time
                self:skillHurt(masterSprite)

            end,self._skill.per_time,0)

        elseif self._skill.base_id == 2011 then   -- 流行火雨

            if targetSprite ~= nil then
                local posX,posY = targetSprite:getPosition()
                targetX = posX
                targetY = posY
            end
            self:setPosition(targetX,targetY)

            --施法
            local posX1,posY1 = self._mainSprite:getPosition()
            self._mainSprite:setPosition(posX1,posY1 + 6 * TileHeight)
            self._mainSprite:runAction(castingAnimate)
            self._mainSprite:setVisible(true)

            local moveAnimate = cc.MoveBy:create(self._skill.move_speed * frameSpeed * 5, cc.p(0,-5 * TileHeight))
            self._mainSprite:runAction(moveAnimate)

            -- 流行火雨 24秒  每2秒伤害
            local durationTime = 0
            Helper.schedule(self._mainSprite,function()
                if durationTime > self._skill.duration_time then
                    self:skillFinish()
                end

                durationTime = durationTime + self._skill.per_time
                self:skillHurt(masterSprite,cc.p(targetX,targetY))

            end,self._skill.per_time,0)

            if hitAnimation ~= nil then
                local hitAnimate = cc.Animate:create(hitAnimation)
                self._mainSprite:runAction(cc.RepeatForever:create(hitAnimate))
            end
        end

    elseif self._skill.attack_style == AttackStyle.castingLoop then
        cclog("castingLoop castingLoop  castingLoop")
        local loopPath = self._skill.base_id .. "/loop/000"
        local loopAnimation = animationCache:getAnimation(loopPath)
        if loopAnimation == nil then
            local animFramesLoop = Helper.getAnimFrames(loopPath,10)
            loopAnimation = cc.Animation:createWithSpriteFrames(animFramesLoop, 0.10)
            animationCache:addAnimation(loopAnimation,loopPath)
        end

        masterSprite._effectSprite:stopAllActions()
        masterSprite._effectSprite:setVisible(true)

        local loopAnimate = cc.Animate:create(loopAnimation)
        if castingAnimation ~= nil then
            local sequenceLoop = cc.Sequence:create(castingAnimate,cc.RepeatForever:create(loopAnimate))
            masterSprite._effectSprite:runAction(sequenceLoop)
        else
            masterSprite._effectSprite:runAction(cc.RepeatForever:create(loopAnimate))
        end

        -- 持续时间到
        Helper.performWithDelay(masterSprite._effectSprite,function()
            masterSprite._effectSprite:stopAllActions()
            masterSprite._effectSprite:setVisible(false)
        end,self._skill.duration_time)

        self:skillFinish()
    end

end

-- 执行技能伤害
function SkillSprite:skillHurt(masterSprite,position)
    cclog("SkillSprite  执行技能伤害............")
    local posX,posY = self:getPosition()
    if position then
        posX = position.x
        posY = position.y
    end

    -- 召唤骷髅
    if self._skill.base_id == 3008 then
        self._mapLayer:createPet(90000,cc.p(posX,posY))
        -- 召唤神兽
    elseif self._skill.base_id == 3007 then
        self._mapLayer:createPet(91000,cc.p(posX,posY))
        -- 召唤强化骷髅
    elseif self._skill.base_id == 3012 then
        self._mapLayer:createPet(92000,cc.p(posX,posY))
    else
        local hurtRect = Helper.computeHurtRect(cc.p(posX,posY),self._direction,self._skill)
        self._effectLayer:updateHurtRangRect(hurtRect)
        self._mapLayer:skillHurt(masterSprite,self._skill,hurtRect)
    end
end

function SkillSprite:skillFinish()
    cclog("SkillSprite  技能特效显示完成............")
    self._mainSprite:stopAllActions()
    self:setVisible(false)

end
