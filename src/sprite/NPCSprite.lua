
--------------------------------------------
---- NPCSprite
--------------------------------------------

NPCSprite = class("NPCSprite", function()
    return cc.Sprite:create("main/sprite.png")
end)

function NPCSprite:ctor(parent,npc)
    self._mapLayer = parent
    self:init(npc)
end


function NPCSprite:init(npc)

    self._collisionRect = nil
    self._roleData = npc
    cclog("创建NPC:" .. npc.id)

    self:initSprite()
    self:initRoleData()
    self:stand()
end

function NPCSprite:initRoleData()
    self._roleData.role_type = RoleType.Npc
end

function NPCSprite:initSprite()

    self._state = ActionState.stand

    --精灵
    self._mainSprite = cc.Sprite:createWithSpriteFrameName(self._roleData.model .. "/stand/40000.png")
    self._mainSprite:setAnchorPoint(cc.p(0.5,0.45))
    self._mainSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._mainSprite)

    -- 名字
    self._roleNameLabel = UIHelper.createTTFLabel(self._roleData.name,26)
    self._roleNameLabel:setTextColor( cc.c3b(255, 255, 255) )
    self._roleNameLabel:setPosition(self._mainSprite:getContentSize().width/2, self._mainSprite:getContentSize().height/2 + 110)
    self._roleNameLabel:setAnchorPoint(cc.p(0.5,0.5))
    self._roleNameLabel:setScale(0.8)
    self._mainSprite:addChild(self._roleNameLabel)

    --绘制碰撞盒子
    --self._collisionRectSprite = cc.DrawNode:create()
    --self._collisionRectSprite:setAnchorPoint(cc.p(0.5,0.5))
    --self:addChild(self._collisionRectSprite)
    --self:updateCollisionRect()

end

function NPCSprite:updateCollisionRect()

    self._collisionRectSprite:clear()

    local collisionRect = Helper.getCollisionRect(cc.p(self:getContentSize().width/2, self:getContentSize().height/2))

    local points = { cc.p(collisionRect.x, collisionRect.y), cc.p(collisionRect.x + collisionRect.width, collisionRect.y),
                     cc.p(collisionRect.x + collisionRect.width, collisionRect.y + collisionRect.height),cc.p(collisionRect.x, collisionRect.y + collisionRect.height)}

    self._collisionRectSprite:drawPolygon(points, table.getn(points), cc.c4f(1,0,0,0), 1, cc.c4f(1,0,0,1))

end

--位置变化调用这个
function NPCSprite:updatePosition(position)
    self:setPosition(position)
    --地图上坐标的碰撞盒子
    self._collisionRect = Helper.getCollisionRect(position)
end

function NPCSprite:playAction(state)

    self._state = state
    self._mainSprite:stopAllActions()

    local stateName = ""
    local speedTime = 0

    if self._state == ActionState.stand then
        stateName = "stand"
        frameCount = 4
        speedTime = 2
    end

    local rolePath = self._roleData.model .."/"..stateName.."/"
    local rad,rolePngName,flippedX = Helper.getSpriteFrameProperty(self._roleData.direction)
    local animationCache = cc.AnimationCache:getInstance()
    local framePath = rolePath .. rolePngName
    local animation = animationCache:getAnimation(framePath)
    if animation == nil then
        local animFrames = Helper.getAnimFrames(framePath,6)
        animation = cc.Animation:createWithSpriteFrames(animFrames, speedTime/#animFrames)
        animationCache:addAnimation(animation,framePath)
    end

    local act1 = cc.Animate:create(animation)
    self._mainSprite:runAction(cc.RepeatForever:create(act1))
end

function NPCSprite:stand()
    self:playAction(ActionState.stand)
end
