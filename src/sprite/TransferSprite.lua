
--------------------------------------------
---- TransferSprite
--------------------------------------------

TransferSprite = class("TransferSprite", function()
    return cc.Sprite:create()
end)

function TransferSprite:ctor(parent,mapName)
    self._mapLayer = parent
    self._mapName = mapName
    self:init()
end


function TransferSprite:init()

    self._collisionRect = nil

    self:initSprite()
    self:stand()
end

function TransferSprite:initSprite()
    self._mainSprite = cc.Sprite:createWithSpriteFrameName("transform_blue/00000.png")
    self._mainSprite:setAnchorPoint(cc.p(0.5,0.5))
    self._mainSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._mainSprite)

    if self._mapName ~= nil and  self._mapName ~= "" then
        local mapNameSprite = cc.Sprite:create("map/transform/" .. self._mapName .. ".png")
        mapNameSprite:setAnchorPoint(cc.p(0.5,0.5))
        mapNameSprite:setPosition(self._mainSprite:getContentSize().width/2, self._mainSprite:getContentSize().height/2 + mapNameSprite:getContentSize().height/2)
        self._mainSprite:addChild(mapNameSprite)
    end

    --绘制碰撞盒子
    self._collisionRectSprite = cc.DrawNode:create()
    self._collisionRectSprite:setAnchorPoint(cc.p(0.5,0.5))
    self:addChild(self._collisionRectSprite)
    self:updateCollisionRect()

end

function TransferSprite:updateCollisionRect()

    self._collisionRectSprite:clear()

    local collisionRect = Helper.getCollisionRect(cc.p(self:getContentSize().width/2, self:getContentSize().height/2))

    local points = { cc.p(collisionRect.x, collisionRect.y), cc.p(collisionRect.x + collisionRect.width, collisionRect.y),
                     cc.p(collisionRect.x + collisionRect.width, collisionRect.y + collisionRect.height),cc.p(collisionRect.x, collisionRect.y + collisionRect.height)}

    self._collisionRectSprite:drawPolygon(points, table.getn(points), cc.c4f(1,0,0,0), 1, cc.c4f(1,0,0,1))

end

--位置变化调用这个
function TransferSprite:updatePosition(position)
    self:setPosition(position)
    --地图上坐标的碰撞盒子
    self._collisionRect = Helper.getCollisionRect(position)
end

function TransferSprite:stand()

    self._mainSprite:stopAllActions()

    local animationCache = cc.AnimationCache:getInstance()
    local framePath = "transform_blue/000";
    local animation = animationCache:getAnimation(framePath)
    if animation == nil then
        local animFrames = Helper.getAnimFrames(framePath,16)
        animation = cc.Animation:createWithSpriteFrames(animFrames, 0.25)
        animationCache:addAnimation(animation,framePath)
    end

    self._mainSprite:runAction( cc.RepeatForever:create( cc.Animate:create(animation) ) )

end

function TransferSprite:done()

    self._mainSprite:stopAllActions()
    local cache = cc.SpriteFrameCache:getInstance()
    local framePath = "transform_done/";
    local frameCount = 10

    local animFrames = {}
    for j = 0,frameCount do
        local frame = cache:getSpriteFrame( string.format(framePath  .. "000%02d.png", j) )
        animFrames[j] = frame
    end
    local animation = cc.Animation:createWithSpriteFrames(animFrames, 0.25)
    self._mainSprite:runAction( cc.RepeatForever:create( cc.Animate:create(animation) ) )

end

