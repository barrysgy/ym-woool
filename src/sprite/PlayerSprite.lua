
--------------------------------------------
---- PlayerSprite
--------------------------------------------


PlayerSprite = class("PlayerSprite", function()
    return cc.Sprite:create("main/sprite.png")
end)

function PlayerSprite:ctor(parent,role)
    self._mapLayer = parent
    self:init(role)
end

function PlayerSprite:init(role)

    -- 角色当前数据
    self._roleData = role

    -- 碰撞矩形
    self._collisionRect = nil

    -- 被谁选作目标
    self._selectedSprite = nil

    -- 经验归属者
    self._masterSprite = nil

    -- 把谁作为目标
    self._targetSprite = nil

    --
    self._isDeath = false

    -- 自动攻击开关
    self._autoAttackOpen = false

    -- 方向控制器当前方向
    self._joystickDirection = 0

    self:initRoleData()

    self:initSprite()

    self:stand()
end

function PlayerSprite:initRoleData()

    local roleBaseData = require("config/RoleBaseData")
    local roleBase = nil
    --根据job和level匹配
    for i=1,#roleBaseData do
        if roleBaseData[i].job == self._roleData.job and roleBaseData[i].level == self._roleData.level then
            roleBase = roleBaseData[i]
            break
        end
    end

    if self._roleData.sex == 1 then
        self._roleData.model = 5110501
        self._roleData.upper_body_model = 5210501
    else
        self._roleData.model = 5210501
        self._roleData.upper_body_model = 5210501
    end

    self._roleData.role_type = RoleType.Player
    self._roleData.weapon_model = 0

    self._roleData.max_hp = roleBase.hp
    self._roleData.max_mp = roleBase.mp
    self._roleData.max_exp = roleBase.exp

    --战斗属性
    self._roleData.attack_min = roleBase.attack_min
    self._roleData.attack_max = roleBase.attack_max
    self._roleData.magic_attack_min = roleBase.magic_attack_min
    self._roleData.magic_attack_max = roleBase.magic_attack_max
    self._roleData.dc_attack_min = roleBase.dc_attack_min
    self._roleData.dc_attack_max = roleBase.dc_attack_max

    self._roleData.defence_min = roleBase.defence_min
    self._roleData.defence_max = roleBase.defence_max
    self._roleData.magic_defence_min = roleBase.magic_defence_min
    self._roleData.magic_defence_max = roleBase.magic_defence_max

    self._roleData.luck = roleBase.luck  --幸运
    self._roleData.hit = roleBase.hit   --命中
    self._roleData.dodge = roleBase.dodge --闪避
    self._roleData.crit = roleBase.crit  --暴击
    self._roleData.tenacity = roleBase.tenacity  --韧性
    self._roleData.crit_damage = 100  --暴击伤害

    self._roleData.attack_speed = roleBase.attack_speed
    self._roleData.move_speed = roleBase.move_speed
    self._roleData.base_attack_speed = roleBase.attack_speed
    self._roleData.base_move_speed = roleBase.move_speed
    self._roleData.auto_recover_hp = roleBase.auto_recover_hp
    self._roleData.auto_recover_mp = roleBase.auto_recover_mp
    self._roleData.battle = roleBase.battle

    -- 装备数据
    self:initEquip()

    -- 配置的技能
    self._skillTable = {}
    local roleSkill = self._roleData.skill
    local skillLevelData = require("config/SkillLevelData")
    local skillBaseData = require("config/SkillBaseData")
    for i = 1,#roleSkill do
        for j = 1,#skillLevelData do
            if skillLevelData[j].id == roleSkill[i] then
                self._skillTable[i] = skillLevelData[j]
                for k = 1,#skillBaseData do
                    if skillBaseData[k].id == skillLevelData[j].base_id then
                        self._skillTable[i].icon = skillBaseData[k].icon
                        self._skillTable[i].sound = skillBaseData[k].sound
                        self._skillTable[i].skill_type = skillBaseData[k].skill_type
                        self._skillTable[i].attack_type = skillBaseData[k].attack_type
                        self._skillTable[i].use_distance = skillBaseData[k].use_distance
                        self._skillTable[i].attack_style = skillBaseData[k].attack_style
                        self._skillTable[i].hurt_range = skillBaseData[k].hurt_range
                        self._skillTable[i].is_cool_time = false
                        break
                    end
                end
                break
            end
        end
    end
end


function PlayerSprite:initEquip()
    local cache = cc.SpriteFrameCache:getInstance()
    --所有物品
    local equipItems = require("config/ItemBaseData")
    for i = 1,#equipItems do
        if equipItems[i].id == self._roleData.equip[PLAYER_EQUIP_WEAPON] then
            self._roleData.weapon_model = equipItems[i].model

            if equipItems[i].attack_min then
                self._roleData.attack_min = self._roleData.attack_min + equipItems[i].attack_min
                self._roleData.attack_max = self._roleData.attack_max + equipItems[i].attack_max
            end

            if equipItems[i].magic_attack_min then
                self._roleData.magic_attack_min = self._roleData.magic_attack_min + equipItems[i].magic_attack_min
                self._roleData.magic_attack_max = self._roleData.magic_attack_max + equipItems[i].magic_attack_max
            end

            if equipItems[i].dc_attack_min then
                self._roleData.dc_attack_min = self._roleData.dc_attack_min + equipItems[i].dc_attack_min
                self._roleData.dc_attack_max = self._roleData.dc_attack_max + equipItems[i].dc_attack_max
            end

        elseif equipItems[i].id == self._roleData.equip[PLAYER_EQUIP_UPPERBODY] then
            -- 精灵 根据上衣显示
            self._roleData.upper_body_model = equipItems[i].model
            if equipItems[i].defence_min then
                self._roleData.defence_min = self._roleData.defence_min + equipItems[i].defence_min
                self._roleData.defence_max = self._roleData.defence_max + equipItems[i].defence_max
            end

            if equipItems[i].magic_defence_min then
                self._roleData.magic_defence_min = self._roleData.magic_defence_min + equipItems[i].magic_defence_min
                self._roleData.magic_defence_max = self._roleData.magic_defence_max + equipItems[i].magic_defence_max
            end
        end
    end

    cache:addSpriteFrames("weapon/plist/w_" .. self._roleData.weapon_model .. "@0.plist")
    cache:addSpriteFrames("role/plist/r_".. self._roleData.upper_body_model .. "@0.plist")
    cache:addSpriteFrames("role/plist/r_".. self._roleData.upper_body_model .. "@1.plist")
    cache:addSpriteFrames("role/plist/r_".. self._roleData.upper_body_model .. "@2.plist")
end

function PlayerSprite:initSprite()

    self._state = ActionState.none
    self._speedState = SpeedState.StayState

    -- 光圈
    self._roleCircle = cc.Sprite:createWithSpriteFrameName("select_player/00000.png")
    self._roleCircle:setAnchorPoint(cc.p(0.5,0.5))
    self._roleCircle:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._roleCircle)

    local animFrames = Helper.getAnimFrames("select_player/000",7)
    local animation = cc.Animation:createWithSpriteFrames(animFrames, 0.5)
    local act1 = cc.Animate:create(animation)
    self._roleCircle:runAction(cc.RepeatForever:create(act1))


    --精灵
    self._mainSprite = cc.Sprite:createWithSpriteFrameName("role/".. self._roleData.upper_body_model .. "/stand/00000.png")
    self._mainSprite:setAnchorPoint(cc.p(0.5,0.45))
    self._mainSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self._mainSprite:setLocalZOrder(9)
    self:addChild(self._mainSprite)

    -- 名字和血量
    self._roleNameLabel = UIHelper.createTTFLabel(self._roleData.name,26)
    self._roleNameLabel:setTextColor( cc.c3b(255, 255, 255) )
    self._roleNameLabel:setPosition(self._mainSprite:getContentSize().width/2, self._mainSprite:getContentSize().height/2 + 120)
    self._roleNameLabel:setAnchorPoint(cc.p(0.5,0.5))
    self._roleNameLabel:setScale(0.8)
    self._mainSprite:addChild(self._roleNameLabel)

    --47x18
    local hpBackground = cc.Sprite:create("main/hp_bar.png",cc.rect(0, 10, 47, 8))
    hpBackground:setPosition(self._mainSprite:getContentSize().width/2, self._mainSprite:getContentSize().height/2 + 100)
    hpBackground:setAnchorPoint(cc.p(0.5,0.5))
    self._mainSprite:addChild(hpBackground)

    self._hpProgress = cc.ProgressTimer:create(cc.Sprite:create("main/hp_bar.png",cc.rect(0, 0, 47, 8)))
    self._hpProgress:setPosition(0, hpBackground:getContentSize().height/2)
    self._hpProgress:setAnchorPoint(cc.p(0.0,0.5))
    self._hpProgress:setMidpoint(cc.p(0,1))
    self._hpProgress:setBarChangeRate(cc.p(1, 0))
    self._hpProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)

    cclog("当前角色HP：" ..self._roleData.hp.."/"..self._roleData.max_hp)
    self._hpProgress:setPercentage(self._roleData.hp/self._roleData.max_hp * 100)
    --self._hpProgress:runAction(cc.RepeatForever:create(cc.ProgressTo:create(10, 100)))
    hpBackground:addChild(self._hpProgress)

    -- 武器
    if self._roleData.weapon_model > 0 then
        local cache = cc.SpriteFrameCache:getInstance()
        cache:addSpriteFrames("weapon/plist/w_" .. self._roleData.weapon_model .. "@0.plist")

        self._roleWeapon = cc.Sprite:createWithSpriteFrameName("weapon/".. self._roleData.weapon_model .. "/stand/00000.png")
        self._roleWeapon:setAnchorPoint(cc.p(0.5,0.5))
        self._roleWeapon:setPosition(self._mainSprite:getContentSize().width/2,self._mainSprite:getContentSize().height/2)
        self._mainSprite:addChild(self._roleWeapon)
    end

    -- 其他效果 如魔法盾
    self._effectSprite = cc.Sprite:create()
    self._effectSprite:setAnchorPoint(cc.p(0.5,0.42))
    self._effectSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._effectSprite)
    UIHelper.addEffectWithMode(self._effectSprite,2)

    --绘制碰撞盒子
    --self._collisionRectSprite = cc.DrawNode:create()
    --self._collisionRectSprite:setAnchorPoint(cc.p(0.5,0.5))
    --self:addChild(self._collisionRectSprite)
    --self:updateCollisionRect()

    cclog("角色创建完成...............................................")
end

function PlayerSprite:updateCollisionRect()

    self._collisionRectSprite:clear()

    local collisionRect = Helper.getCollisionRect(cc.p(self:getContentSize().width/2, self:getContentSize().height/2))

    local points = { cc.p(collisionRect.x, collisionRect.y), cc.p(collisionRect.x + collisionRect.width, collisionRect.y),
                     cc.p(collisionRect.x + collisionRect.width, collisionRect.y + collisionRect.height),cc.p(collisionRect.x, collisionRect.y + collisionRect.height)}

    self._collisionRectSprite:drawPolygon(points, table.getn(points), cc.c4f(1,0,0,0), 1, cc.c4f(1,0,0,1))

end


-- 位置变化调用这个
function PlayerSprite:updatePosition(position)

    self:setPosition(position)
    -- 地图上坐标的碰撞盒子
    self._collisionRect = Helper.getCollisionRect(position)

    -- 镜头跟踪
    self._mapLayer:cameraUpdate()

    local posX,posY = self:getPosition()

    -- 目标判断
    if self._targetSprite ~= nil then
        local targetX,targetY = self._targetSprite:getPosition()
        local dis = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
        if dis > TargetViewRange then
            self._targetSprite:setSelected(false)
            self._targetSprite = nil
        end
    end

    -- 宠物的位置
    for i = 1,#self._mapLayer._petTable do
        local targetX,targetY = self._mapLayer._petTable[i]:getPosition()
        local dis = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
        if dis > 20 * TileHeight then
            self._mapLayer._petTable[i]._targetSprite = nil
            self._mapLayer._petTable[i]:updatePosition(position)
        end
    end
end


function PlayerSprite:playAction(state)

    self._state = state

    self._mainSprite:stopAllActions()
    self._roleWeapon:stopAllActions()

    local stateName = ""
    local frameSpeed,actionSpeed = 0

    if self._state == ActionState.stand then
        stateName = "stand"
        frameSpeed,actionSpeed = 2,2
    elseif self._state == ActionState.run then
        stateName = "run"
        frameSpeed,actionSpeed = Numerical.getMoveFrameSpeed(self._roleData.base_move_speed,self._roleData.move_speed)
        if self._speedState == SpeedState.QuickState then
            frameSpeed = frameSpeed * 0.8
            actionSpeed = actionSpeed * 0.8
        end
    elseif self._state == ActionState.attack then
        stateName = "attack"
        frameSpeed,actionSpeed = Numerical.getAttackFrameSpeed(self._roleData.base_attack_speed,self._roleData.attack_speed)
    elseif self._state == ActionState.casting then
        stateName = "casting"
        frameSpeed,actionSpeed = Numerical.getAttackFrameSpeed(self._roleData.base_attack_speed,self._roleData.attack_speed)
    elseif self._state == ActionState.hurt then
        stateName = "stand"
        frameSpeed,actionSpeed = 0.25,0.25
    end

    local rad,rolePngName,flippedX = Helper.getSpriteFrameProperty(self._roleData.direction)

    local rolePath = "role/".. self._roleData.upper_body_model .."/"..stateName.."/" .. rolePngName

    local animationCache = cc.AnimationCache:getInstance()

    self._mainSprite:setFlippedX(flippedX)
    self._roleWeapon:setFlippedX(flippedX)

    local animationRole = animationCache:getAnimation(rolePath)

    if animationRole == nil then
        local animFramesRole = Helper.getAnimFrames(rolePath,30)
        if #animFramesRole > 0 then
            animationRole = cc.Animation:createWithSpriteFrames(animFramesRole,frameSpeed/#animFramesRole)
            animationCache:addAnimation(animationRole,rolePath)
        end

    end

    local roleAnimate = nil
    if animationRole ~= nil then
        roleAnimate = cc.Animate:create(animationRole)
    else
        roleAnimate = cc.DelayTime:create(0.25)
    end

    --武器动画
    local weaponPath = "weapon/"..self._roleData.weapon_model.."/"..stateName.."/" .. rolePngName
    local animationWeapon = animationCache:getAnimation(weaponPath)
    if animationWeapon == nil then
        local animFramesWeapon = Helper.getAnimFrames(weaponPath,20)
        if #animFramesWeapon > 0 then
            animationWeapon = cc.Animation:createWithSpriteFrames(animFramesWeapon,frameSpeed/#animFramesWeapon)
            animationCache:addAnimation(animationWeapon,weaponPath)
        end
    end

    local weaponAnimate = nil
    if animationWeapon ~= nil then
        weaponAnimate = cc.Animate:create(animationWeapon)
    else
        weaponAnimate = cc.DelayTime:create(0.25)
    end


    if self._state == ActionState.attack or self._state == ActionState.casting then

        cclog("RoleSprite Run  Attack Action.............")
        local sequence = cc.Sequence:create(roleAnimate,cc.CallFunc:create(function()
            --恢复状态
            self._state = ActionState.none  --强制
            self:stand()
        end))
        self._mainSprite:runAction(sequence)
        self._roleWeapon:runAction(weaponAnimate)

    elseif self._state == ActionState.hurt then
        self._mainSprite:setColor(cc.c3b(255, 0, 0))
        local sequence = cc.Sequence:create(roleAnimate, cc.CallFunc:create(function()
            self._mainSprite:setColor(cc.c3b(255, 255, 255))
            self:playAction(ActionState.stand)
        end))
        self._mainSprite:runAction(sequence)
        self._roleWeapon:runAction(weaponAnimate)
    else
        --跑
        self._mainSprite:runAction(cc.RepeatForever:create(roleAnimate))
        self._roleWeapon:runAction(cc.RepeatForever:create(weaponAnimate))
    end

    return rad

end

function PlayerSprite:playAttackMoveAction(skill)

    --中断角色移动
    self:stopActionByTag(ActionTag.Move)

    self._state = ActionState.attack

    self._mainSprite:stopAllActions()
    self._roleWeapon:stopAllActions()

    local rad,rolePngName,flippedX = Helper.getSpriteFrameProperty(self._roleData.direction)

    local step = 24
    local stepX = math.cos(rad) * step
    local stepY = math.sin(rad) * step * 2/3
    local count = (skill.use_distance -1) * TileWidth / step
    local current = 0
    local collision = false
    Helper.schedule(self,function()

        local posX,posY = self:getPosition()
        local target = cc.p(posX + stepX, posY + stepY)

        --碰撞检测
        if self._mapLayer:isSpriteCollision(self,target) then
            collision = true
            self:playAction(ActionState.attack)
            self._mapLayer._effectLayer:updateEffect(self,skill)
        else
            --复制
            local sprite1 = cc.Sprite:createWithSpriteFrame(self._mainSprite:getSpriteFrame());
            sprite1:setPosition(posX,posY + 22)
            sprite1:setFlippedX(flippedX)
            sprite1:setOpacity(255 - (count-current) * 20)
            self._mapLayer:addChild(sprite1)
            self._mapLayer:setCameraMask(cc.CameraFlag.USER1)

            Helper.performWithDelay(sprite1,function()
                sprite1:removeFromParent()
            end,0.3)

            self:updatePosition(target)

            current = current + 1
        end

        if  current == count then
            if not collision then
                self:playAction(ActionState.attack)
                self._mapLayer._effectLayer:updateEffect(self,skill)
            end
            self:stopActionByTag(ActionTag.Move)
        end

    end,skill.move_speed,ActionTag.Move)
end

function PlayerSprite:stand()

    if self._state == ActionState.attack
            or self._state == ActionState.hurt
            or self._state == ActionState.death then
        return
    end

    -- 中断角色移动
    self:stopActionByTag(ActionTag.Move)
    self:playAction(ActionState.stand)
end


function PlayerSprite:run()

    if self._state == ActionState.attack or self._state == ActionState.casting then
        return
    end

    --中断角色移动
    self:stopActionByTag(ActionTag.Move)

    --移动速度
    local frameSpeed,actionSpeed = Numerical.getMoveFrameSpeed(self._roleData.base_move_speed,self._roleData.move_speed)

    local rad = self:playAction(ActionState.run)
    local stepX = math.cos(rad) * MoveStep
    local stepY = math.sin(rad) * MoveStep * 2/3

    local function moveStep()

        local posX,posY = self:getPosition()
        local target = cc.p(posX + stepX, posY + stepY)
        if self._mapLayer:isSpriteCollision(self,target) then
            return nil
        else
            return target
        end
    end

    Helper.schedule(self,function()

        local target = moveStep()
        if target ~= nil then
            self:updatePosition(target)
        else

            self:stopActionByTag(ActionTag.Move)

            --换个方向
            --上 =   右
            --左上 = 左上
            --左   = 上
            --左下 = 右下
            --下 =   左
            --右下 = 右上
            --右 =   下
            --右上 = 左上
            local direction = DirectionType.UpType
            if self._roleData.direction == DirectionType.UpType then
                direction = DirectionType.RightType
            elseif self._roleData.direction == DirectionType.LeftUpType then
                direction = DirectionType.LeftDownType
            elseif self._roleData.direction == DirectionType.LeftType then
                direction = DirectionType.UpType
            elseif self._roleData.direction == DirectionType.LeftDownType then
                direction = DirectionType.RightDownType
            elseif self._roleData.direction == DirectionType.DownType then
                direction = DirectionType.LeftType
            elseif self._roleData.direction == DirectionType.RightDownType then
                direction = DirectionType.RightUpType
            elseif self._roleData.direction == DirectionType.RightType then
                direction = DirectionType.DownType
            elseif self._roleData.direction == DirectionType.RightUpType then
                direction = DirectionType.LeftUpType
            end

            self._roleData.direction = direction
            self:run()
        end

    end,actionSpeed,ActionTag.Move)

end

function PlayerSprite:runToTargetSprite(targetSprite,skill,callback)

    --中断角色移动
    self:stopActionByTag(ActionTag.Move)

    --重新寻路
    self._targetMapCoordinate = cc.p(0,0)

    --移动速度
    local frameSpeed,actionSpeed = Numerical.getMoveFrameSpeed(self._roleData.base_move_speed,self._roleData.move_speed)
    local findPath = require ("core/AStarFindPath")
    local optimalPath = {}

    local function moveStep()

        --目标丢失
        if targetSprite == nil or targetSprite._isDeath then
            cclog("目标丢失.......................")
            self:stopActionByTag(ActionTag.Move)
            return nil
        end

        local posX,posY = self:getPosition()
        local targetX,targetY = targetSprite:getPosition()
        local distance = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
        local rad = Helper.getRad(cc.p(posX,posY),cc.p(targetX,targetY))

        local attackDistance = 0
        if targetX == posX then
            attackDistance =  skill.use_distance * TileHeight
        else
            attackDistance = math.abs(skill.use_distance * TileWidth * math.cos(rad))
        end
        if distance <= attackDistance then
            self._mapLayer._planPath:clear()
            callback(1)
            return nil
        end


        --目标位置发生变化就重新寻路
        local startPosition = Helper.positionToMapCoordinate(cc.p(posX,posY),self._mapLayer._mapSize)
        local endPosition = Helper.positionToMapCoordinate(cc.p(targetX,targetY),self._mapLayer._mapSize)

        cclog("本次起点坐标：" .. startPosition.x .. "," .. startPosition.y)
        cclog("最终点坐标：" .. endPosition.x .. "," .. endPosition.y)

        --目标位置发生变化重新寻路 否则还是按照原来路线
        if math.abs(self._targetMapCoordinate.x - endPosition.x) > 2  or math.abs(self._targetMapCoordinate.y - endPosition.y) > 2 then
            local optimalPathNew = findPath:find(startPosition,endPosition,self._mapLayer._mapPath,function(open,close) end)
            if #optimalPathNew <= 20 then
                table.remove(optimalPathNew,#optimalPathNew)
                optimalPath = optimalPathNew
                cclog("路径规划成功：" .. json.encode(optimalPath))  --[{"x":107,"y":125,"h":8,"f":8,"g":0}]
            else
                cclog("路径规划失败，长度：" .. #optimalPathNew)
                optimalPath = {}
            end
            self._targetMapCoordinate = endPosition
        end

        --没有路径就直接结束移动
        if #optimalPath == 0 then
            cclog("没有路径........")
            self._mapLayer._planPath:clear()
            callback(0)
            return nil
        else
            --在地图上显示
            self._mapLayer:updatePlanPath(optimalPath)
        end

        cclog("开始移动.............................................")


        local pathIndex = #optimalPath
        cclog("本次移动目标点：" .. optimalPath[pathIndex].x .. "," .. optimalPath[pathIndex].y .. ",index:" ..pathIndex)

        local target = Helper.mapCoordinateToPosition(cc.p(optimalPath[pathIndex].x,optimalPath[pathIndex].y),self._mapLayer._mapSize)

        --获得玩家角色方向和移动角度
        local rad = Helper.getRad(cc.p(posX,posY),target)
        local stepX = math.cos(rad) * MoveStep
        local stepY = math.sin(rad) * MoveStep * 2/3

        cclog("OffsetX:" .. math.abs(target.x - posX))
        cclog("OffsetY:" .. math.abs(target.y - posY))
        cclog("stepX:" .. math.abs(stepX))
        cclog("stepY:" .. math.abs(stepY))

        --如果是一个接近目标点就不控制方向  防止角色方向变化抖动
        if math.abs(target.x - posX) < math.abs(stepX) + 1 and math.abs(target.y - posY) < math.abs(stepY) + 1 then
            cclog("移除目标点.....................................................")
            --不够一步的就只走这么多
            table.remove(optimalPath,pathIndex)
        else
            --本次移动后的位置
            target = cc.p(posX + stepX, posY + stepY)
        end

        local direction = Helper.getDirection(cc.p(posX,posY),target)
        if self._roleData.direction ~= direction then
            self._roleData.direction = direction
            self:playAction(ActionState.run)
        else
            if self._state ~= ActionState.run then
                self:playAction(ActionState.run)
            end

        end

        cclog(".........................................................................")
        return target
    end

    Helper.schedule(self,function()

        local target = moveStep()

        if target then
            --更新位置
            self:updatePosition(target)
        end

    end,actionSpeed ,ActionTag.Move)

end


function PlayerSprite:attack(skill,attackCallback)

    if self._state == ActionState.attack or self._state == ActionState.casting then
        return
    end

    --判断攻击动作 分为attack和casting
    local attackState = ActionState.attack

    if skill.attack_style == AttackStyle.attack or skill.attack_style == AttackStyle.attackSurround
            or skill.attack_style == AttackStyle.attackMove then
        attackState = ActionState.attack
    elseif skill.attack_style == AttackStyle.casting
            or skill.attack_style == AttackStyle.castingSurround
            or skill.attack_style == AttackStyle.castingDuration
            or skill.attack_style == AttackStyle.castingLoop then
        attackState = ActionState.casting
    end


    local function playAttack()

        -- 先停住才能释放技能
        self:stand()

        -- 控制技能冷却的
        attackCallback()

        --位移推人技能特殊处理
        if skill.attack_style == AttackStyle.attackMove then
            self:playAttackMoveAction(skill)
        else
            --显示角色动作和技能特效
            self:playAction(attackState)
            self._mapLayer._effectLayer:updateEffect(self,skill)
        end
    end


    --该技能是否需要目标
    if skill.base_id ~= 1008 and skill.base_id ~= 2004 and skill.base_id ~= 2009 and skill.base_id ~= 3010
        and skill.base_id ~= 3007 and skill.base_id ~= 3008 and skill.base_id ~= 3010 and skill.base_id ~= 3012
            and skill.base_id ~= 2003 and skill.base_id ~= 2005 then
        local posX,posY = self:getPosition()

        if self._targetSprite == nil  or self._targetSprite._isDeath then

            local minDistance = TargetViewRange
            for k = 1,#self._mapLayer._monsterTable do
                if not self._mapLayer._monsterTable[k]._isDeath then
                    local targetX,targetY = self._mapLayer._monsterTable[k]:getPosition()
                    local dis = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
                    if dis < minDistance and not self._mapLayer._monsterTable[k]._isDeath then
                        self._targetSprite = self._mapLayer._monsterTable[k]
                        minDistance = dis
                    end
                end

            end

            if self._targetSprite ~= nil and not self._targetSprite._isDeath then
                self._targetSprite:setSelected(true)
                cclog("自动锁定目标：" .. self._targetSprite._roleData.name)
            end
        end

        if self._targetSprite ~= nil and not self._targetSprite._isDeath then

            local targetX,targetY = self._targetSprite:getPosition()
            local direction = Helper.getDirection(cc.p(posX,posY),cc.p(targetX,targetY))
            local distance = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
            local rad = Helper.getRad(cc.p(posX,posY),cc.p(targetX,targetY))

            --有目标先转向
            self._roleData.direction = direction

            --不在技能攻击范围 自动移动到攻击方位 A*寻路
            local attackDistance = 0
            if targetX == posX then
                attackDistance =  skill.use_distance * TileHeight
            else
                attackDistance = math.abs(skill.use_distance * TileWidth * math.cos(rad))
            end

            if distance > attackDistance then
                self:runToTargetSprite(self._targetSprite,skill,function(code)
                    if code == 1 then
                        playAttack()
                    else
                        UIHelper.showToast(self._mapLayer,"自动寻路失败")
                    end

                end)
            else
                cclog("目标" .. self._targetSprite._roleData.name .. "在攻击范围，开始攻击")
                playAttack()
            end
        else
            cclog("角色无目标 .........")
            UIHelper.showToast(self._mapLayer,"没有目标")
        end

    else
         playAttack()
    end

end


function PlayerSprite:hurt(masterSprite,skill)

    if self._isDeath then
        return
    end

    -- 经验归属者
    if self._masterSprite == nil then
        self._masterSprite = masterSprite
    end

    local hp,hurt = Numerical.getHurtNumber(masterSprite._roleData,self._roleData,skill)

    cclog("真实伤害：" .. hurt .. ",减血：" .. hp)

    local posX,posY = self:getPosition()

    self._mapLayer:showHurtNumber(hurt,cc.p(posX,posY + 150))


    self._roleData.hp = self._roleData.hp - hp
    if self._roleData.hp < 0 then
        self._roleData.hp = 0
    end

    -- 更新血条
    self._hpProgress:setPercentage(self._roleData.hp/self._roleData.max_hp * 100)
    --self._hpLabel:setString(self._roleData.hp.. "/" .. self._roleData.max_hp)

    if self._roleData.hp == 0 then
        --self:death()
    else

        if self._state == ActionState.hurt
                or self._state == ActionState.death then
            -- 被其他人攻击时什么都不用做
        else
            self:playAction(ActionState.hurt)
        end

    end

end

--移动发生变化
function PlayerSprite:updateAction(direction,speedState)

    if speedState == SpeedState.StayState then
        self._joystickDirection = 0
        if self._state ~= ActionState.stand or self._roleData.direction ~= direction or self._speedState ~= speedState then
            self._roleData.direction = direction
            self._speedState = speedState
            cclog("stand stand stand stand stand .......................")
            self:stand()
        end
    elseif speedState == SpeedState.NormalState then
        if self._joystickDirection ~= direction and (self._state ~= ActionState.run or self._roleData.direction ~= direction or self._speedState ~= speedState) then
            self._roleData.direction = direction
            self._speedState = speedState
            cclog("run run run run run .......................")
            self._joystickDirection = direction
            self:run()
        end
    else
        if self._joystickDirection ~= direction and (self._state ~= ActionState.run or self._roleData.direction ~= direction or self._speedState ~= speedState) then
            self._roleData.direction = direction
            self._speedState = speedState
            cclog("run run run run run .......................")
            self._joystickDirection = direction
            self:run()
        end
    end


    cclog("updateAction ....end...................")
end

--技能施放
function PlayerSprite:updateAttackAction(skill,attackCallback)
    return self:attack(skill,attackCallback)
end

function PlayerSprite:updateRoleData(hp,mp,exp)
    self._roleData.hp = self._roleData.hp + hp
    self._roleData.mp = self._roleData.mp + mp
    self._roleData.exp = self._roleData.exp + exp

    if self._roleData.exp > self._roleData.max_exp then
        cclog("升级................................")
        self:levelUp()
        self._roleData.level = self._roleData.level + 1
        self:initRoleData()
        self._roleData.hp = self._roleData.max_hp
        self._roleData.mp = self._roleData.max_mp
        self._roleData.exp = self._roleData.exp - self._roleData.max_exp
    end


    self._hpProgress:setPercentage(self._roleData.hp/self._roleData.max_hp * 100)
    cclog("更新血条：" ..self._roleData.hp.."/"..self._roleData.max_hp)
    --self._hpLabel:setString(self._roleData.hp.. "/" .. self._roleData.max_hp)

    self._mapLayer._scene:updateMainUI()
end

-- 显示升级特效
function PlayerSprite:levelUp()
    cclog("升级特效................................")
    self:playEffect("level_up/000",cc.p(0.5,0.24),cc.p(self:getContentSize().width/2, self:getContentSize().height/2))
end

-- 显示特效
function PlayerSprite:playEffect(path,anchorPoint,position)
    local effectSprite = cc.Sprite:create()
    effectSprite:setAnchorPoint(anchorPoint)
    effectSprite:setPosition(position)
    effectSprite:setLocalZOrder(1)
    self:addChild(effectSprite)
    self._mapLayer:setCameraMask(cc.CameraFlag.USER1)

    local animFrames = Helper.getAnimFrames(path,22)
    local animate = cc.Animate:create(cc.Animation:createWithSpriteFrames(animFrames, 0.10))
    local sequence = cc.Sequence:create(animate,cc.CallFunc:create(function()
    end),cc.RemoveSelf:create())
    effectSprite:runAction(sequence)
end

-- 换装装备 只有武器和衣服影响着装
function PlayerSprite:changeClothes(item)
    self._state = ActionState.none
    if item.kind == 1 then
        self._roleData.equip[PLAYER_EQUIP_WEAPON] = item.id
    elseif item.kind == 5 then
        self._roleData.equip[PLAYER_EQUIP_UPPERBODY] = item.id
    end
    self:initRoleData()
    self:stand()
end


function PlayerSprite:startAutoAttack(skillLayer)

    if self._autoAttackOpen then
        return
    end

    self._autoAttackOpen = true

    self:stopAllActions()

    local function coolTime(skill,skillCDSprite)
        skill.is_cool_time = true
        skillCDSprite:setPercentage(100)
        local sequenceCD = cc.Sequence:create(cc.ProgressTo:create(skill.cool_time/1000,0),cc.CallFunc:create(function()
            skill.is_cool_time = false
            skillCDSprite:setPercentage(0)
        end))
        self:runAction(sequenceCD)
    end

    cclog("角色开启自动攻击...............")
    Helper.schedule(self,function()

        if not self._isDeath and self._state ~= ActionState.hurt then

            cclog("000000000000000000角色选择技能攻击...........................")
            for i = 1,#self._skillTable do
                local skill = self._skillTable[i]
                local skillCDSprite = skillLayer._skillCDSpriteTable[i]
                if not skill.is_cool_time and skill.skill_type == 1 then
                    cclog("角色选择技能攻击...........................")
                    --执行技能攻击
                    self:updateAttackAction(skill,function(code)
                        if code == 1 then
                            coolTime(skill,skillCDSprite)
                        end
                    end)
                    return
                end
            end

        end

    end,1,ActionTag.AutoAttack)
end

function PlayerSprite:closeAutoAttack()
    self._autoAttackOpen = false
    self:stopActionByTag(ActionTag.AutoAttack)
end