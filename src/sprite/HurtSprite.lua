--------------------------------------------
---- HurtSprite
---  显示伤害数值
--------------------------------------------



local HurtSprite = class("HurtSprite", function() return  cc.Sprite:create() end)

-- 资源路径  数字  symbol是否有+-符号
function HurtSprite:ctor(imageName,number,rect,numWidth,symbol,span)
	local numberTexture = cc.Director:getInstance():getTextureCache():addImage(imageName)
	if numberTexture then
		local mumSpan = span or 0
		local numberText = tostring(number)
		local length = string.len(numberText)
		local numTab = {}
		for i = 1,length do
			numTab[i] = tonumber(string.sub(numberText,i,i))
		end
		self:setTexture(numberTexture)
		if symbol then
			self:setTextureRect(cc.rect(rect[1],rect[2],numWidth,rect[4]))
			for i = 1,length do
				local num_sp = cc.Sprite:createWithTexture(numberTexture,cc.rect(rect[1] + numWidth * (numTab[i]+1),rect[2],numWidth,rect[4]))
				num_sp:setPosition(cc.p((numWidth + mumSpan )*i,0))
				num_sp:setAnchorPoint(cc.p(0.0,0.0))
				self:addChild(num_sp)
			end
		else
			self:setTextureRect(cc.rect(rect[1] + numWidth * numTab[1],rect[2],numWidth,rect[4]))
			for i = 2,length do
				local num_sp = cc.Sprite:createWithTexture(numberTexture,cc.rect(rect[1] + numWidth * numTab[i],rect[2],numWidth,rect[4]))
				num_sp:setPosition(cc.p((numWidth + mumSpan )*(i-1),0))
				num_sp:setAnchorPoint(cc.p(0.0,0.0))
				self:addChild(num_sp)
			end
		end
	end
end

return HurtSprite