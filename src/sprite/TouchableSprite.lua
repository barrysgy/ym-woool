---
--- Created by zhaoqp2019.
--- DateTime: 2020/2/25 8:18
--- 可点击的精灵
---

TouchableSprite = class("TouchableSprite")
TouchableSprite.__index = TouchableSprite
TouchableSprite._listener = nil
TouchableSprite._fixedPriority = 0
TouchableSprite._useNodePriority = false
TouchableSprite._removeListenerOnTouchEnded = false

function TouchableSprite.extend(target)
    local t = tolua.getpeer(target)
    if not t then
        t = {}
        tolua.setpeer(target, t)
    end
    setmetatable(t, TouchableSprite)
    return target
end

function TouchableSprite:onEnter()
    local eventDispatcher = self:getEventDispatcher()

    local function onTouchBegan(touch, event)
        local locationInNode = self:convertToNodeSpace(touch:getLocation())
        local s = self:getContentSize()
        local rect = cc.rect(0, 0, s.width, s.height)
        cclog(string.format("sprite rect... w = %f, h = %f", rect.width, rect.height))
        cclog(string.format("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y))
        if cc.rectContainsPoint(rect, locationInNode) then
            self:setColor(cc.c3b(255, 0, 0))
            return true
        end

        return false
    end

    local function onTouchMoved(touch, event)
        local target = event:getCurrentTarget()
        local x,y = target:getPosition()
        target:setPosition(cc.p(x + touch:getDelta().x, y + touch:getDelta().y))
    end

    local  function onTouchEnded(touch, event)
        self:setColor(cc.c3b(255, 255, 255))
        if self._removeListenerOnTouchEnded then
            eventDispatcher:removeEventListener(self._listener)
            self._listener = nil
        end

    end

    local listener = cc.EventListenerTouchOneByOne:create()
    self._listener = listener
    listener:setSwallowTouches(true)

    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )

    if 0 == self._fixedPriority then
        eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self)
    else
        eventDispatcher:addEventListenerWithFixedPriority(listener,self._fixedPriority)
    end
end

function TouchableSprite:onExit()
    if self._listener ~= nil then
        local eventDispatcher = self:getEventDispatcher()
        eventDispatcher:removeEventListener(self._listener)
    end
end

function TouchableSprite:setPriority(fixedPriority)
    self._fixedPriority = fixedPriority
    self._useNodePriority = false
end

function TouchableSprite:removeListenerOnTouchEnded(toRemove)
    self._removeListenerOnTouchEnded = toRemove
end

function TouchableSprite:setPriorityWithThis(useNodePriority)
    self._fixedPriority = 0
    self._useNodePriority = useNodePriority
end

function TouchableSprite.new()
    local touchableSprite = TouchableSprite.extend(cc.Sprite:create())

    local function onNodeEvent(event)
        if event == "enter" then
            touchableSprite:onEnter()
        elseif event == "exit" then
            touchableSprite:onExit()
        end
    end

    touchableSprite:registerScriptHandler(onNodeEvent)
    return touchableSprite
end