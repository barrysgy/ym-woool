
--------------------------------------------
---- MonsterSprite
--------------------------------------------

MonsterSprite = class("MonsterSprite", function()
    return cc.Sprite:create("main/sprite.png")
end)

function MonsterSprite:ctor(parent,monster)
    self._mapLayer = parent
    self:init(monster)
end


function MonsterSprite:init(monster)

    self._roleData = monster
    self._collisionRect = nil

    -- 把谁作为目标
    self._targetSprite = nil

    -- 经验归属者
    self._masterSprite = nil

    self._isDeath = false

    self._autoAttackState = false

    --每个怪物生成唯一的ID
    cclog("创建怪物:" .. monster.id)

    self:initRoleData()

    self:initSprite()

    self:stand()

    self:startRandomMove()
end

function MonsterSprite:initRoleData()

    local monsterBaseData = require("config/MonsterBaseData")

    local monsterBase = nil
    for i=1,#monsterBaseData do
        if monsterBaseData[i].id == self._roleData.base_id then
            monsterBase = monsterBaseData[i]
            break
        end
    end

    if monsterBase.id >= 90000 then
        self._roleData.role_type = RoleType.Pet
    else
        self._roleData.role_type = RoleType.Monster
    end

    self._roleData.id = self._roleData.id
    self._roleData.model = monsterBase.model
    self._roleData.name = monsterBase.name
    self._roleData.level = monsterBase.level
    self._roleData.hp = monsterBase.hp
    self._roleData.mp = monsterBase.mp
    self._roleData.max_hp = monsterBase.hp
    self._roleData.max_mp = monsterBase.mp
    self._roleData.exp = monsterBase.exp

    self._roleData.view_range = monsterBase.view_range

    --战斗属性
    self._roleData.attack_min = monsterBase.attack_min
    self._roleData.attack_max = monsterBase.attack_max
    self._roleData.magic_attack_min = monsterBase.magic_attack_min
    self._roleData.magic_attack_max = monsterBase.magic_attack_max

    self._roleData.defence_min = monsterBase.defence_min
    self._roleData.defence_max = monsterBase.defence_max
    self._roleData.magic_defence_min = monsterBase.magic_defence_min
    self._roleData.magic_defence_max = monsterBase.magic_defence_max

    self._roleData.hit = monsterBase.hit   -- 命中
    self._roleData.tenacity = monsterBase.tenacity  -- 韧性

    self._roleData.attack_speed = monsterBase.attack_speed
    self._roleData.move_speed = monsterBase.move_speed * 0.2
    self._roleData.base_attack_speed = monsterBase.attack_speed
    self._roleData.base_move_speed = monsterBase.move_speed
    self._roleData.auto_recover_hp = monsterBase.auto_recover_hp
    self._roleData.auto_recover_mp = monsterBase.auto_recover_mp

    -- 配置的技能
    self._skillTable = {}
    local roleSkill = {1000001}
    local skillLevelData = require("config/SkillLevelData")
    local skillBaseData = require("config/SkillBaseData")
    for i = 1,#roleSkill do
        for j = 1,#skillLevelData do
            if skillLevelData[j].id == roleSkill[i] then
                self._skillTable[i] = skillLevelData[j]
                for k = 1,#skillBaseData do
                    if skillBaseData[k].id == skillLevelData[j].base_id then
                        self._skillTable[i].icon = skillBaseData[k].icon
                        self._skillTable[i].sound = skillBaseData[k].sound
                        self._skillTable[i].skill_type = skillBaseData[k].skill_type
                        self._skillTable[i].attack_type = skillBaseData[k].attack_type
                        self._skillTable[i].use_distance = skillBaseData[k].use_distance
                        self._skillTable[i].attack_style = skillBaseData[k].attack_style
                        self._skillTable[i].hurt_range = skillBaseData[k].hurt_range
                        self._skillTable[i].is_cool_time = false
                        break
                    end
                end
                break
            end
        end
    end

end

-- 复活怪物
function MonsterSprite:revive(position)

    cclog("复活怪物:" .. self._roleData.id)

    self._targetSprite = nil
    self._masterSprite = nil
    self._isDeath = false
    self._autoAttackState = false

    self._roleData.hp = self._roleData.max_hp
    self._roleData.mp = self._roleData.max_mp

    -- 更新血条
    self._hpProgress:setPercentage(100)
    self._hpLabel:setString(self._roleData.hp.. "/" .. self._roleData.max_hp)
    self._mainSprite:setColor(cc.c3b(255, 255, 255))
    self._state = ActionState.none
    self._roleCircle:setVisible(false)

    self:updatePosition(position)
    self:stand()
    self:setVisible(true)
    self:startRandomMove()
end

function MonsterSprite:initSprite()

    -- 光圈
    self._roleCircle = cc.Sprite:createWithSpriteFrameName("select_monster/00000.png")
    UIHelper.addEffectWithMode(self._roleCircle,2)
    self._roleCircle:setAnchorPoint(cc.p(0.5,0.5))
    self._roleCircle:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._roleCircle)
    self._roleCircle:setVisible(false)

    local cache = cc.SpriteFrameCache:getInstance()
    self._roleCircleFrames = {}
    for j = 0,8 do
        local frame = cache:getSpriteFrame( string.format("select_monster/000%02d.png", j) )
        self._roleCircleFrames[j] = frame
    end

    --精灵
    self._mainSprite = cc.Sprite:createWithSpriteFrameName(self._roleData.model .. "/stand/00000.png")
    self._mainSprite:setAnchorPoint(cc.p(0.5,0.45))
    self._mainSprite:setPosition(self:getContentSize().width/2, self:getContentSize().height/2)
    self:addChild(self._mainSprite)

    -- 名字和血量
    self._roleNameLabel = UIHelper.createTTFLabel(self._roleData.name,18)
    self._roleNameLabel:setTextColor( cc.c3b(255, 255, 255) )
    self._roleNameLabel:setPosition(self._mainSprite:getContentSize().width/2, self._mainSprite:getContentSize().height/2)
    self._roleNameLabel:setAnchorPoint(cc.p(0.5,0.5))
    self._mainSprite:addChild(self._roleNameLabel)

    --47x18
    local hpBackground = cc.Sprite:create("main/hp_bar_monster.png",cc.rect(0, 10, 47, 8))
    hpBackground:setPosition(self._mainSprite:getContentSize().width/2, self._mainSprite:getContentSize().height/2 + 60)
    hpBackground:setAnchorPoint(cc.p(0.5,0.5))
    self._mainSprite:addChild(hpBackground)

    self._hpProgress = cc.ProgressTimer:create(cc.Sprite:create("main/hp_bar_monster.png",cc.rect(0, 0, 47, 8)))
    self._hpProgress:setPosition(0, hpBackground:getContentSize().height/2)
    self._hpProgress:setAnchorPoint(cc.p(0.0,0.5))
    self._hpProgress:setMidpoint(cc.p(0,1))
    self._hpProgress:setBarChangeRate(cc.p(1, 0))
    self._hpProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)

    cclog("当前怪物HP：" ..self._roleData.hp.."/"..self._roleData.max_hp)
    self._hpProgress:setPercentage(self._roleData.hp/self._roleData.max_hp * 100)
    --self._hpProgress:runAction(cc.RepeatForever:create(cc.ProgressTo:create(10, 100)))
    hpBackground:addChild(self._hpProgress)

    self._hpLabel = UIHelper.createTTFLabel(self._roleData.hp.."/"..self._roleData.max_hp,20)
    self._hpLabel:setTextColor( cc.c3b(255, 255, 255) )
    self._hpLabel:setPosition(self._hpProgress:getContentSize().width/2, self._hpProgress:getContentSize().height + 10)
    self._hpLabel:setAnchorPoint(cc.p(0.5,0.5))
    self._hpLabel:setScale(0.8)
    hpBackground:addChild(self._hpLabel)


    --绘制碰撞盒子
    --self._collisionRectSprite = cc.DrawNode:create()
    --self._collisionRectSprite:setAnchorPoint(cc.p(0.5,0.5))
    --self:addChild(self._collisionRectSprite)
    --self:updateCollisionRect()

end

function MonsterSprite:updateCollisionRect()

    self._collisionRectSprite:clear()

    local collisionRect = Helper.getCollisionRect(cc.p(self:getContentSize().width/2, self:getContentSize().height/2))

    local points = { cc.p(collisionRect.x, collisionRect.y), cc.p(collisionRect.x + collisionRect.width, collisionRect.y),
                     cc.p(collisionRect.x + collisionRect.width, collisionRect.y + collisionRect.height),cc.p(collisionRect.x, collisionRect.y + collisionRect.height)}

    self._collisionRectSprite:drawPolygon(points, table.getn(points), cc.c4f(1,0,0,0), 1, cc.c4f(1,0,0,1))
end

--位置变化调用这个
function MonsterSprite:updatePosition(position)
    self:setPosition(position)
    --地图上坐标的碰撞盒子
    self._collisionRect = Helper.getCollisionRect(position)

    local posX,posY = self:getPosition()

    --目标判断
    if self._targetSprite ~= nil then
        local targetX,targetY = self._targetSprite:getPosition()
        local dis = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
        if dis > TargetViewRange then
            self._targetSprite = nil
        end
    end

end

function MonsterSprite:playAction(state)

    self._state = state
    self._mainSprite:stopAllActions()

    local rad,rolePngName,flippedX = Helper.getSpriteFrameProperty(self._roleData.direction)

    local stateName = ""
    local frameSpeed,actionSpeed = 0

    if self._state == ActionState.stand then
        stateName = "stand"
        frameSpeed,actionSpeed = 2,2
    elseif self._state == ActionState.walk then
        stateName = "walk"
        frameSpeed,actionSpeed = Numerical.getMoveFrameSpeed(self._roleData.base_move_speed,self._roleData.move_speed)
        frameSpeed = frameSpeed * 0.3
    elseif self._state == ActionState.attack then
        stateName = "attack"
        frameSpeed,actionSpeed = Numerical.getAttackFrameSpeed(self._roleData.base_attack_speed,self._roleData.attack_speed)
    elseif self._state == ActionState.casting then
        stateName = "casting"
        frameSpeed,actionSpeed = Numerical.getAttackFrameSpeed(self._roleData.base_attack_speed,self._roleData.attack_speed)
    elseif self._state == ActionState.hurt then
        stateName = "hurt"  --如果素材里没有这个hurt状态的图 也没关系最后执行到stand上
        frameSpeed,actionSpeed = 0.25,0.25
    elseif self._state == ActionState.death then
        stateName = "death"
        rolePngName = "300"
        frameSpeed,actionSpeed = 0.25,0.25
    end

    self._mainSprite:setFlippedX(flippedX)

    local animationCache = cc.AnimationCache:getInstance()

    local framePath = self._roleData.model .. "/" ..stateName.."/" .. rolePngName

    local animation = animationCache:getAnimation(framePath)
    if animation == nil then
        local animFrames = Helper.getAnimFrames(framePath,20)
        if #animFrames > 0 then
            animation = cc.Animation:createWithSpriteFrames(animFrames,frameSpeed/#animFrames)
            animationCache:addAnimation(animation,framePath)
        end
    end

    local roleAnimate = nil
    if animation ~= nil then
        roleAnimate = cc.Animate:create(animation)
    else
        roleAnimate = cc.DelayTime:create(0.25)
    end

    if self._state == ActionState.attack or self._state == ActionState.casting then

        cclog("MonsterSprite Run  Attack Action.............")
        local sequence = cc.Sequence:create(roleAnimate,cc.CallFunc:create(function()
            cclog("MonsterSprite Run  Attack Action Finish .............")
            --恢复状态
            self._state = ActionState.none  --强制
            self:stand()
            cclog("MonsterSprite Run  Attack Action Finish stand............:" .. self._state)

        end))
        self._mainSprite:runAction(sequence)

    elseif self._state == ActionState.hurt then
        self._mainSprite:setColor(cc.c3b(255, 0, 0))
        local sequence = cc.Sequence:create(roleAnimate, cc.CallFunc:create(function()
            self._mainSprite:setColor(cc.c3b(255, 255, 255))
            self:playAction(ActionState.stand)
        end))
        self._mainSprite:runAction(sequence)

    elseif self._state == ActionState.death then
        local sequence = cc.Sequence:create(roleAnimate, cc.CallFunc:create(function()

            -- 结算经验
            self._masterSprite:updateRoleData(5,5,self._roleData.exp)

            -- 设置为不可见
            self:setVisible(false)

        end))
        self._mainSprite:runAction(sequence)
    else
        self._mainSprite:runAction(cc.RepeatForever:create(roleAnimate))
    end

    return rad

end

function MonsterSprite:stand()

    if self._state == ActionState.attack
            or self._state == ActionState.hurt
            or self._state == ActionState.death then
        return
    end

    self:stopActionByTag(ActionTag.Move)
    self:playAction(ActionState.stand)

end

function MonsterSprite:walk()

    if self._state == ActionState.attack
            or self._state == ActionState.casting
            or self._state == ActionState.hurt
            or self._state == ActionState.death then
        return
    end

    -- 中断角色移动
    self:stopActionByTag(ActionTag.Move)

    --移动速度
    local frameSpeed,actionSpeed = Numerical.getMoveFrameSpeed(self._roleData.base_move_speed,self._roleData.move_speed)

    local rad = self:playAction(ActionState.walk)
    local stepX = math.cos(rad) * MoveStep
    local stepY = math.sin(rad) * MoveStep * 2/3

    local function moveStep()

        local posX,posY = self:getPosition()
        local target = cc.p(posX + stepX, posY + stepY)
        if self._mapLayer:isSpriteCollision(self,target) then
            return nil
        else
            return target
        end
    end
    local stepCount = math.floor(TileWidth * 1/3)
    Helper.schedule(self,function()

        if stepCount == 0 then
            self:stand()
        end

        local target = moveStep()
        if target ~= nil then
            stepCount = stepCount - 1
            self:updatePosition(target)
        else

            self:stopActionByTag(ActionTag.Move)

            --换个方向
            --上 =   右
            --左上 = 左上
            --左   = 上
            --左下 = 右下
            --下 =   左
            --右下 = 右上
            --右 =   下
            --右上 = 左上
            local direction = DirectionType.UpType
            if self._roleData.direction == DirectionType.UpType then
                direction = DirectionType.RightType
            elseif self._roleData.direction == DirectionType.LeftUpType then
                direction = DirectionType.LeftDownType
            elseif self._roleData.direction == DirectionType.LeftType then
                direction = DirectionType.UpType
            elseif self._roleData.direction == DirectionType.LeftDownType then
                direction = DirectionType.RightDownType
            elseif self._roleData.direction == DirectionType.DownType then
                direction = DirectionType.LeftType
            elseif self._roleData.direction == DirectionType.RightDownType then
                direction = DirectionType.RightUpType
            elseif self._roleData.direction == DirectionType.RightType then
                direction = DirectionType.DownType
            elseif self._roleData.direction == DirectionType.RightUpType then
                direction = DirectionType.LeftUpType
            end

            self._roleData.direction = direction
            self:walk()
        end

    end,actionSpeed,ActionTag.Move)

end


function MonsterSprite:runToTargetSprite(targetSprite,skill,callback)

    --中断角色移动
    self:stopActionByTag(ActionTag.Move)

    --重新寻路
    self._targetMapCoordinate = cc.p(0,0)

    --移动速度
    local frameSpeed,actionSpeed = Numerical.getMoveFrameSpeed(self._roleData.base_move_speed,self._roleData.move_speed)
    local findPath = require ("core/AStarFindPath")
    local optimalPath = {}

    local function moveStep()

        --目标丢失
        if targetSprite == nil or targetSprite._isDeath then
            cclog("目标丢失,停止当前移动动作.......................")
            self:stopActionByTag(ActionTag.Move)
            return nil
        end

        local posX,posY = self:getPosition()
        local targetX,targetY = targetSprite:getPosition()
        local distance = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
        local rad = Helper.getRad(cc.p(posX,posY),cc.p(targetX,targetY))

        local attackDistance = 0
        if targetX == posX then
            attackDistance =  skill.use_distance * TileHeight
        else
            attackDistance = math.abs(skill.use_distance * TileWidth * math.cos(rad))
        end
        if distance <= attackDistance then
            self._mapLayer._planPath:clear()
            callback(1)
            return nil
        end

        -- 目标位置发生变化就重新寻路
        local startPosition = Helper.positionToMapCoordinate(cc.p(posX,posY),self._mapLayer._mapSize)
        local endPosition = Helper.positionToMapCoordinate(cc.p(targetX,targetY),self._mapLayer._mapSize)

        cclog("怪物本次起点坐标：" .. startPosition.x .. "," .. startPosition.y)
        cclog("怪物最终点坐标：" .. endPosition.x .. "," .. endPosition.y)

        if math.abs(self._targetMapCoordinate.x - endPosition.x) > 2  or math.abs(self._targetMapCoordinate.y - endPosition.y) > 2 then
            local optimalPathNew = findPath:find(startPosition,endPosition,self._mapLayer._mapPath,function(open,close) end)
            if #optimalPathNew <= 20 then
                table.remove(optimalPathNew,#optimalPathNew)
                optimalPath = optimalPathNew
                cclog("怪物路径规划成功：" .. json.encode(optimalPath))  --[{"x":107,"y":125,"h":8,"f":8,"g":0}]
            else
                cclog("怪物路径规划失败，长度：" .. #optimalPathNew)
                optimalPath = {}

                -- 临时 停止所有动作 这里有个问题  就是寻路失败会导致界面卡
                self:startRandomMove()
            end
            self._targetMapCoordinate = endPosition
        end

        --没有路径就直接结束移动
        if #optimalPath == 0 then
            cclog("怪物没有路径放弃攻击........")
            self._mapLayer._planPath:clear()
            callback(0)
            return nil
        else
            --在地图上显示
            self._mapLayer:updatePlanPath(optimalPath)
        end

        cclog("怪物开始移动.............................................")

        local pathIndex = #optimalPath
        cclog("怪物本次移动目标点：" .. optimalPath[pathIndex].x .. "," .. optimalPath[pathIndex].y .. ",index:" ..pathIndex)

        local target = Helper.mapCoordinateToPosition(cc.p(optimalPath[pathIndex].x,optimalPath[pathIndex].y),self._mapLayer._mapSize)

        --获得玩家角色方向和移动角度
        local rad = Helper.getRad(cc.p(posX,posY),target)
        local stepX = math.cos(rad) * MoveStep
        local stepY = math.sin(rad) * MoveStep * 2/3

        --cclog("OffsetX:" .. math.abs(target.x - posX))
        --cclog("OffsetY:" .. math.abs(target.y - posY))
        --cclog("stepX:" .. math.abs(stepX))
        --cclog("stepY:" .. math.abs(stepY))

        --如果是一个接近目标点就不控制方向  防止角色方向变化抖动
        if math.abs(target.x - posX) < math.abs(stepX) + 1 and math.abs(target.y - posY) < math.abs(stepY) + 1 then
            cclog("怪物移除目标点.....................................................")
            --不够一步的就只走这么多
            table.remove(optimalPath,pathIndex)
        else
            --本次移动后的位置
            target = cc.p(posX + stepX, posY + stepY)
        end

        local direction = Helper.getDirection(cc.p(posX,posY),target)
        if self._roleData.direction ~= direction then
            self._roleData.direction = direction
            self:playAction(ActionState.walk)
        else
            if self._state ~= ActionState.walk then
                self:playAction(ActionState.walk)
            end

        end

        cclog(".........................................................................")
        return target
    end

    Helper.schedule(self,function()

        -- 优先受伤状态显示
        if self._state == ActionState.hurt then
            return
        end

        local target = moveStep()

        if target then
            --更新位置
            self:updatePosition(target)
        end

    end,actionSpeed,ActionTag.Move)

end

function MonsterSprite:attack(skill,attackCallback)

    if self._state == ActionState.attack or self._state == ActionState.casting then
        cclog("执行攻击，但是已经是攻击状态...................")
        return
    end

    --判断攻击动作 分为attack和casting
    local attackState = ActionState.attack

    if skill.attack_style == AttackStyle.attack or skill.attack_style == AttackStyle.attackSurround
            or skill.attack_style == AttackStyle.attackMove then
        attackState = ActionState.attack
    elseif skill.attack_style == AttackStyle.casting or skill.attack_style == AttackStyle.castingSurround
            or skill.attack_style == AttackStyle.castingDuration
            or skill.attack_style == AttackStyle.castingLoop then
        attackState = ActionState.casting
    end


    local function playAttack()

        cclog("怪物释放技能.......................")

        -- 先停住才能释放技能
        self:stand()

        -- 控制技能冷却的
        attackCallback()

        -- 位移推人技能特殊处理
        if skill.attack_style == AttackStyle.attackMove then
            --self:playAttackMoveAction(skill)
        else
            --显示角色动作和技能特效
            self:playAction(attackState)
            self._mapLayer._effectLayer:updateEffect(self,skill)
        end
    end

    local posX,posY = self:getPosition()

    -- 宠物自动锁定目标
    if self._roleData.role_type == RoleType.Pet then
        self._targetSprite = self._mapLayer._playerSprite._targetSprite
        -- 优先角色的目标
        if self._targetSprite == nil or self._targetSprite._isDeath then
            local minDistance = self._roleData.view_range * TileWidth
            for k = 1,#self._mapLayer._monsterTable do
                if not self._mapLayer._monsterTable[k]._isDeath and self._mapLayer._monsterTable[k] ~= self then
                    local targetX,targetY = self._mapLayer._monsterTable[k]:getPosition()
                    local dis = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
                    if dis < minDistance and not self._mapLayer._monsterTable[k]._isDeath then
                        self._targetSprite = self._mapLayer._monsterTable[k]
                        minDistance = dis
                    end
                end
            end

            if self._targetSprite ~= nil and not self._targetSprite._isDeath then
                self._targetSprite:setSelected(true)
                cclog("自动锁定目标：" .. self._targetSprite._roleData.name)
            end
        end

    end

    if self._targetSprite ~= nil and not self._targetSprite._isDeath then

        local targetX,targetY = self._targetSprite:getPosition()
        local direction = Helper.getDirection(cc.p(posX,posY),cc.p(targetX,targetY))
        local distance = Helper.getDistance(cc.p(posX,posY),cc.p(targetX,targetY))
        local rad = Helper.getRad(cc.p(posX,posY),cc.p(targetX,targetY))

        --有目标先转向
        self._roleData.direction = direction

        --不在技能攻击范围 自动移动到攻击方位 A*寻路
        local attackDistance = 0
        if targetX == posX then
            attackDistance =  skill.use_distance * TileHeight
        else
            attackDistance = math.abs(skill.use_distance * TileWidth * math.cos(rad))
        end

        if distance > attackDistance then
            cclog("目标不在怪物攻击范围，开始寻路................")
            self:runToTargetSprite(self._targetSprite,skill,function(code)
                if code == 1 then
                    playAttack()
                else
                    cclog("怪物自动寻路失败 ........")
                    self:stand()
                    self:startRandomMove()
                end
            end)
        else
            playAttack()
        end
    else
        cclog("怪物无目标 .....")
        self:stand()
        self:startRandomMove()
    end

end

function MonsterSprite:hurt(masterSprite,skill)

    if self._isDeath then
        return
    end

    -- 经验归属者
    if self._masterSprite == nil then
        self._masterSprite = masterSprite
    end

    local hp,hurt = Numerical.getHurtNumber(masterSprite._roleData,self._roleData,skill)

    cclog("真实伤害：" .. hurt .. ",减血：" .. hp)

    local posX,posY = self:getPosition()

    self._mapLayer:showHurtNumber(hurt,cc.p(posX,posY + 150))


    self._roleData.hp = self._roleData.hp - hp
    if self._roleData.hp < 0 then
        self._roleData.hp = 0
    end

    -- 更新血条
    self._hpProgress:setPercentage(self._roleData.hp/self._roleData.max_hp * 100)
    self._hpLabel:setString(self._roleData.hp.. "/" .. self._roleData.max_hp)

    if self._roleData.hp == 0 then
        self:death()
    else

        if self._state == ActionState.hurt
                or self._state == ActionState.death then
            -- 被其他人攻击时什么都不用做
        else
            self:playAction(ActionState.hurt)
        end

        -- 自动反击 玩家和宠物
        if masterSprite._roleData.role_type == RoleType.Player or masterSprite._roleData.role_type == RoleType.Pet then
            self:startAutoAttack(masterSprite)
        end

    end

end

function MonsterSprite:death()
    if self._isDeath then
        return
    end
    self._isDeath = true
    cclog("怪物死亡：" .. self._roleData.id .. "," ..  self._roleData.name)
    self:stopAllActions()
    self._roleCircle:stopAllActions()
    self._mapLayer:removeTargetFromSprite(self)
    self:playAction(ActionState.death)
end


function MonsterSprite:updateAction(directionType,actionState)

    if actionState == ActionState.stand then

        if self._state ~= ActionState.stand or self._roleData.direction ~= directionType then
            self._roleData.direction = directionType
            self:stand()
        end

    elseif actionState == ActionState.walk then
        if self._state ~= ActionState.walk or self._roleData.direction ~= directionType then
            self._roleData.direction = directionType
            self:walk()
        end
    end
end

--技能施放
function MonsterSprite:updateAttackAction(skill,attackCallback)
    return self:attack(skill,attackCallback)
end

function MonsterSprite:setSelected(selected)

    if selected == true then
        if not self._roleCircle:isVisible() then
            cclog("目标setSelected:" .. self._roleData.id..","..self._roleData.name)
            local animation = cc.Animation:createWithSpriteFrames(self._roleCircleFrames, 0.5)
            local act = cc.Animate:create(animation)
            self._roleCircle:runAction(cc.RepeatForever:create(act))
            self._roleCircle:setVisible(selected)

            --其他怪物反选
            for i=1,#self._mapLayer._monsterTable do
                if not self._mapLayer._monsterTable[i]._isDeath and self._mapLayer._monsterTable[i] ~= self then
                    self._mapLayer._monsterTable[i]:setSelected(false)
                end
            end

        end
    else
        self._roleCircle:stopAllActions()
        self._roleCircle:setVisible(selected)
    end

end

function MonsterSprite:startRandomMove()
    self._autoAttackState  = false
    self:stopAllActions()

    for i = 1,#self._skillTable do
        self._skillTable[i].is_cool_time = false
    end

    -- 活动半径大于0才移动
    if self._roleData.radius > 0 then
        -- 随机移动怪物
        Helper.schedule(self,function(node)
            local randomDirection = math.random(1, 8)
            local randomMotion = math.random(1, 2)

            self:stopActionByTag(ActionTag.Move)
            self:updateAction(randomDirection,randomMotion)
        end,12,ActionTag.RandomMove)
    end
end

function MonsterSprite:startAutoAttack(targetSprite)

    if self._autoAttackState then
        return
    end

    self._autoAttackState  = true
    self._targetSprite = targetSprite
    self:stopAllActions()

    cclog("怪物开启自动攻击...............")
    Helper.schedule(self,function()

        if not self._isDeath and self._state ~= ActionState.hurt then

            cclog("000000000000000000怪物选择技能攻击...........................")
            for i = 1,#self._skillTable do
                local skill = self._skillTable[i]
                if not skill.is_cool_time and skill.skill_type == 1 then
                    cclog("怪物选择技能攻击...........................")
                    -- 执行技能攻击
                    self:updateAttackAction(skill,function()
                        skill.is_cool_time = true
                        local sequenceCD = cc.Sequence:create(cc.DelayTime:create(skill.cool_time/1000),cc.CallFunc:create(function()
                            skill.is_cool_time = false
                        end))
                        self:runAction(sequenceCD)
                    end)
                    return
                end
            end
        end

    end,1,ActionTag.AutoAttack)
end

function MonsterSprite:closeAutoAttack()
    self._autoAttackState = false
    self:stopActionByTag(ActionTag.AutoAttack)
end

-- 怪物回满血和蓝
function MonsterSprite:updateRoleData(hp,mp,exp)
    if self._roleData.hp < self._roleData.max_hp then
        self._roleData.hp = self._roleData.hp + hp
    end
    if self._roleData.mp < self._roleData.max_mp then
        self._roleData.mp = self._roleData.mp + mp
    end
end