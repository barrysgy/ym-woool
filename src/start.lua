--------------------------------------------
---- Start Scene
--------------------------------------------

-- CC_USE_DEPRECATED_API = true
require "cocos.init"
require "Dictionary"
require "Helper"
require "UIHelper"
require "Numerical"
require "config/RoleProperty"
require "GameData"
require "layer/SplashLayer"
require "layer/LoadingLayer"

require "login/LoginScene"
require "role/RoleCreateScene"
require "loading/LoadingScene"
require "main/MainScene"

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    cclog("----------------------------------------")
    cclog("LUA ERROR: " .. tostring(msg) .. "\n")
    cclog(debug.traceback())
    cclog("----------------------------------------")
end

local function initGLView()
    local director = cc.Director:getInstance()
    local glView = director:getOpenGLView()
    if nil == glView then
        glView = cc.GLViewImpl:create("Test")
        director:setOpenGLView(glView)
    end

    director:setOpenGLView(glView)

    glView:setDesignResolutionSize(1920, 1080, cc.ResolutionPolicy.NO_BORDER)

    --turn on display FPS
    director:setDisplayStats(true)

    --set FPS. the default value is 1.0/60 if you don't call this
    director:setAnimationInterval(1.0 / 60)
end


local function main()
    -- avoid memory leak
    collectgarbage("setpause", 100)
    collectgarbage("setstepmul", 5000)

    initGLView()

    -- run
    local sceneGame = cc.Scene:create()
    sceneGame:addChild(SplashLayer.new())
    cc.Director:getInstance():runWithScene(sceneGame)

    -- 监听返回键
    local function onKeyReleased(keyCode, event)
        if keyCode == cc.KeyCode.KEY_BACK then

            --保存游戏
            GameData.saveGameData()

            --local target = cc.Application:getInstance():getTargetPlatform()
            cc.Director:getInstance():endToLua()
        end
    end

    local listener = cc.EventListenerKeyboard:create()
    listener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED )
    local dispatcher = cc.Director:getInstance():getEventDispatcher()
    dispatcher:addEventListenerWithFixedPriority(listener, 1)

end

xpcall(main, __G__TRACKBACK__)
