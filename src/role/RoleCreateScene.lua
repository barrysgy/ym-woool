
--------------------------------------------
---- Role Create Scene
--------------------------------------------



local scene = nil

local backgroundLayer = nil
local roleInputLayer = nil
local roleCreateLayer = nil
local roleSelectLayer = nil
local roleInfo = {}
local roleSpriteTable = {}

local roleSelectSpriteTable = {}
local roleSelectNameTable = {}
local roleSelectLvTable = {}

local currentCreateRole = nil
local currentSelectRole = nil

local editName = nil
local sexButton1 = nil
local sexButton2 = nil
local jobButton1 = nil
local jobButton2 = nil
local jobButton3 = nil
local jobTips = nil
local startButton = nil
local scale = 1.0

local currentLayerIndex = 1

BackgroundLayer = function()

    local bgLayer = cc.Layer:create()

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local bg = cc.Sprite:create("create_role/bg.jpg")
    bg:setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2)
    scale = visibleSize.width / bg:getContentSize().width
    bg:setScale(scale)

    local bgBottom = cc.Sprite:create("create_role/bg_bottom.png")
    bgBottom:setAnchorPoint(cc.p(0.5, 0.5) )
    bgBottom:setPosition(origin.x + bgBottom:getContentSize().width / 2 * scale, origin.y + bgBottom:getContentSize().height / 2 * scale)
    bgBottom:setScale(scale)

    -- 角色Id 和 等级
    local roleIdLv1 = cc.Sprite:create("create_role/idlv.png")
    roleIdLv1:setPosition(origin.x + visibleSize.width * 0.25,origin.y + 40 * scale)
    roleIdLv1:setAnchorPoint(cc.p(0.5, 0.5))
    roleIdLv1:setScale(scale)

    local roleIdLv2 = cc.Sprite:create("create_role/idlv.png")
    roleIdLv2:setPosition(origin.x + visibleSize.width * 0.75 ,origin.y + 40 * scale)
    roleIdLv2:setAnchorPoint(cc.p(0.5, 0.5))
    roleIdLv2:setScale(scale)


    local roleIdRight = cc.Sprite:create("create_role/modillion.png")
    roleIdRight:setPosition(origin.x + visibleSize.width - roleIdRight:getContentSize().width/2 * scale,origin.y + roleIdRight:getContentSize().height/2 * scale)
    roleIdRight:setAnchorPoint(cc.p(0.5, 0.5))
    roleIdRight:setScale(scale)

    local roleIdLeft = cc.Sprite:create("create_role/modillion.png")
    roleIdLeft:setAnchorPoint(cc.p(0.5, 0.5))
    roleIdLeft:setPosition(origin.x + roleIdLeft:getContentSize().width/2 * scale,origin.y + roleIdLeft:getContentSize().height/2 * scale)
    roleIdLeft:setFlippedX(true)
    roleIdLeft:setScale(scale)

    -- 返回按钮  回到登录
    local backButton = UIHelper.createScaleImageButton("create_role/back_btn.png",function(node)
        if currentLayerIndex == 3 then
            roleSelectLayer:removeFromParent()
            roleSelectLayer = nil
            UIHelper.toScene(LoginSceneMain())
        elseif currentLayerIndex == 4 then
            if roleInputLayer ~=nil then
                roleInputLayer:removeFromParent()
                roleInputLayer = nil
            end

            roleCreateLayer:removeFromParent()
            roleCreateLayer = nil

            roleSelectLayer = RoleSelectLayer()
            scene:addChild(roleSelectLayer)
            currentLayerIndex = 3



        end
    end);
    backButton:setPosition(origin.x + backButton:getContentSize().width/2 * 1 + 20,origin.y + visibleSize.height - backButton:getContentSize().height/2 * 1 - 20)
    backButton:setScale(1)

    bgLayer:addChild(bg)
    bgLayer:addChild(bgBottom)
    bgLayer:addChild(roleIdLv1)
    bgLayer:addChild(roleIdLv2)
    bgLayer:addChild(roleIdLeft)
    bgLayer:addChild(roleIdRight)
    bgLayer:addChild(backButton)

    return bgLayer
end

RoleSelectLayer = function()

    local origin = cc.Director:getInstance():getVisibleOrigin()
    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local layer = cc.Layer:create()
    local roleData = GameData.data
    local cache = cc.SpriteFrameCache:getInstance()
    local roleCreateButton = UIHelper.createScaleImageButton("create_role/new_role.png",function(node)

        if #GameData.data > 1 then
            UIHelper.showToast(layer,"最多能创建2个角色哦！")
            return
        end
        roleSelectLayer:removeFromParent()
        roleSelectLayer = nil

        roleCreateLayer = RoleCreateLayer()
        scene:addChild(roleCreateLayer)
        currentLayerIndex = 4

    end);
    roleCreateButton:setPosition(visibleSize.width - roleCreateButton:getContentSize().width/2 * 1 - 20,origin.y + visibleSize.height - roleCreateButton:getContentSize().height/2 * 1 - 20)
    roleCreateButton:setScale(1)
    layer:addChild(roleCreateButton)


    for i=1, #roleData do
        local rolePath = "create_role_" .. roleData[i].job .. "_" .. roleData[i].sex .."_stand";
        cclog("rolePath:" .. rolePath)
        local roleSprite = cc.Sprite:createWithSpriteFrameName(rolePath .. "/00000.png")
        if i==1 then
            roleSprite:setPosition(visibleSize.width/2 - 400,visibleSize.height/2 -50)
        else
            roleSprite:setPosition(visibleSize.width/2 + 400,visibleSize.height/2 -50)
        end
        local animFrames = {}
        for j = 0,31 do
            local frame = cache:getSpriteFrame( string.format(rolePath .."/000%02d.png", j) )
            animFrames[j+1] = frame
        end
        local animation = cc.Animation:createWithSpriteFrames(animFrames, 0.3)
        roleSprite:runAction( cc.RepeatForever:create( cc.Animate:create(animation) ) )
        layer:addChild(roleSprite)
        roleSelectSpriteTable[i] = roleSprite
        addSpriteTouchEvent(roleData[i],i)

        local loc = 0.25
        if i == 1 then
            loc = 0.25
        else
            loc = 0.75
        end

        local nameLabel = UIHelper.createTTFLabel(roleData[i].name,20)
        nameLabel:setTextColor( cc.c3b(153, 153, 153) )
        nameLabel:setPosition( cc.p(origin.x + visibleSize.width * loc - 30 * scale,origin.y + 42 * scale) )
        layer:addChild(nameLabel)
        roleSelectNameTable[i] = nameLabel

        local lvLabel = UIHelper.createTTFLabel(roleData[i].level,20)
        lvLabel:setTextColor( cc.c3b(153, 153, 153) )
        lvLabel:setPosition( cc.p(origin.x + visibleSize.width * loc + 146 * scale,origin.y + 42 * scale) )
        layer:addChild(lvLabel)
        roleSelectLvTable[i] = lvLabel

        startButton = UIHelper.createButton("",20,"create_role/start_btn.png", "create_role/start_btn_disable.png");
        startButton:setPosition(cc.p(origin.x + visibleSize.width/2,origin.y + 32 * scale))
        startButton:setScale(scale)
        startButton:setLocalZOrder(9)
        startButton:addClickEventListener(function(sender)

            GameData.currentRole = currentSelectRole
            UIHelper.toScene(LoadingSceneMain(GameData.currentRole.map))
        end)
        layer:addChild(startButton)


    end


    return layer
end

RoleCreateLayer = function()

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local layer = cc.Layer:create()

    roleInfo = {
        {id = 1, job = 1, sex = 1, x = visibleSize.width/2 -150, y = visibleSize.height/2 + 190,scale= 0.6, interval = 110, touch_rect = cc.rect(295, 96, 200, 331)},
        {id = 2, job = 1, sex = 2, x = visibleSize.width/2 + 250, y = visibleSize.height/2 + 200,scale= 0.6, interval = 118, touch_rect = cc.rect(512, 391, 172, 215)},
        {id = 3, job = 2, sex = 1, x = visibleSize.width/2 - 550, y = visibleSize.height/2 - 50,scale= 0.85, interval = 115, touch_rect = cc.rect(0, 72, 185, 345)},
        {id = 4, job = 2, sex = 2, x = visibleSize.width/2 + 450, y = visibleSize.height/2 - 50,scale= 0.85, interval = 105, touch_rect = cc.rect(748, 60, 208, 313)},
        {id = 5, job = 3, sex = 1, x = visibleSize.width/2 - 400, y = visibleSize.height/2 + 120,scale= 0.65, interval = 108, touch_rect = cc.rect(185, 249, 109, 262)},
        {id = 6, job = 3, sex = 2, x = visibleSize.width/2 + 350, y = visibleSize.height/2 + 120,scale= 0.65, interval = 112, touch_rect = cc.rect(595, 195, 155, 214)},
    }

    for i=1, #roleInfo do

        local rolePath = "create_role_" .. roleInfo[i].job .. "_" .. roleInfo[i].sex .. "_stand";
        local roleSprite = cc.Sprite:createWithSpriteFrameName(rolePath .. "/00000.png")
        roleSprite:setPosition(cc.p(roleInfo[i].x, roleInfo[i].y))
        roleSprite:setScale(roleInfo[i].scale)
        runSpriteAction(roleSprite,roleInfo[i],"stand")
        layer:addChild(roleSprite)
        roleSpriteTable[i] = roleSprite
        addSpriteTouchEvent(roleInfo[i],0)

    end

    return layer
end

RoleInputLayer = function()
    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()
    local layer = cc.Layer:create()
    local bg = cc.Sprite:create("create_role/bg_dialog.png")
    local x = origin.x + visibleSize.width - bg:getContentSize().width/2 * scale - 100
    local y = origin.y + visibleSize.height / 2
    bg:setPosition(x, y)
    bg:setAnchorPoint(cc.p(0.5,0.5))
    bg:setScale(scale)
    layer:addChild(bg)

    local labelTitle = cc.Sprite:create("create_role/label_title.png")
    labelTitle:setPosition(bg:getContentSize().width/2,bg:getContentSize().height - labelTitle:getContentSize().height/2 - 33)
    bg:addChild(labelTitle)

    local bgWidth = bg:getContentSize().width * scale

    local nameBackground = cc.Sprite:create("main/ui/edit_box_bg.png",cc.size(bgWidth * 0.6, 36))
    nameBackground:setAnchorPoint(cc.p(0,0.5))
    nameBackground:setPosition(45,bg:getContentSize().height - 115)
    bg:addChild(nameBackground)

    editName = UIHelper.createEditBox("请输入角色名称",cc.size(bgWidth * 0.6, 36),20)
    editName:setAnchorPoint(cc.p(0,0.5))
    editName:setPosition(45,bg:getContentSize().height - 115)
    bg:addChild(editName)


    local label1 = cc.Sprite:create("create_role/label_1.png")
    label1:setAnchorPoint(cc.p(0,0.5))
    label1:setPosition(45,bg:getContentSize().height - 175)
    bg:addChild(label1)

    sexButton1 = UIHelper.createButton("",20,"create_role/1_1.png", "create_role/1_0.png");
    sexButton1:setPosition(145,bg:getContentSize().height - 175)
    sexButton1:addClickEventListener(function(sender)
        if currentCreateRole.sex ~= 1 then
            createRole(roleInfo[currentCreateRole.id-1])
        else
            sexButton1:setBrightStyle(1)
        end
    end)
    bg:addChild(sexButton1)

    sexButton2 = UIHelper.createButton("",20,"create_role/2_1.png", "create_role/2_0.png");
    sexButton2:setPosition(200,bg:getContentSize().height - 175)
    sexButton2:addClickEventListener(function(sender)
        if currentCreateRole.sex ~= 2 then
            createRole(roleInfo[currentCreateRole.id+1])
        else
            sexButton2:setBrightStyle(1)
        end
    end)
    bg:addChild(sexButton2)

    -- 职业

    local label2 = cc.Sprite:create("create_role/label_2.png")
    label2:setAnchorPoint(cc.p(0,0.5))
    label2:setPosition(45,bg:getContentSize().height - 230)
    bg:addChild(label2)

    jobButton1 = UIHelper.createButton("",20,"create_role/3_1.png", "create_role/3_0.png");
    jobButton1:setPosition(145,bg:getContentSize().height - 230)
    jobButton1:addClickEventListener(function(sender)
        if currentCreateRole.job ~= 1 then
            if currentCreateRole.job == 2 then
                createRole(roleInfo[currentCreateRole.id - 2])
            else
                createRole(roleInfo[currentCreateRole.id - 4])
            end
        else
            jobButton1:setBrightStyle(1)
        end
    end)
    bg:addChild(jobButton1)

    local jobLabel1 = cc.Sprite:create("create_role/job_1.png")
    jobLabel1:setAnchorPoint(cc.p(0,0.5))
    jobLabel1:setPosition(190,bg:getContentSize().height - 230)
    bg:addChild(jobLabel1)

    jobButton2 = UIHelper.createButton("",20,"create_role/4_1.png", "create_role/4_0.png","create_role/4_1.png");
    jobButton2:setPosition(145,bg:getContentSize().height - 285)
    jobButton2:addClickEventListener(function(sender)

        if currentCreateRole.job ~= 2 then
            if currentCreateRole.job == 1 then
                createRole(roleInfo[currentCreateRole.id + 2])
            else
                createRole(roleInfo[currentCreateRole.id - 2])
            end
        else
            jobButton2:setBrightStyle(1)
        end
    end)
    bg:addChild(jobButton2)

    local jobLabel2 = cc.Sprite:create("create_role/job_2.png")
    jobLabel2:setAnchorPoint(cc.p(0,0.5))
    jobLabel2:setPosition(190,bg:getContentSize().height - 285)
    bg:addChild(jobLabel2)

    jobButton3 = UIHelper.createButton("",20,"create_role/5_1.png", "create_role/5_0.png","create_role/5_1.png");
    jobButton3:setPosition(145,bg:getContentSize().height -340)
    jobButton3:addClickEventListener(function(sender)
        if currentCreateRole.job ~= 3 then
            if currentCreateRole.job == 1 then
                createRole(roleInfo[currentCreateRole.id + 4])
            else
                createRole(roleInfo[currentCreateRole.id + 2])
            end
        else
            jobButton3:setBrightStyle(1)
        end
    end)
    bg:addChild(jobButton3)

    local jobLabel3 = cc.Sprite:create("create_role/job_3.png")
    jobLabel3:setAnchorPoint(cc.p(0,0.5))
    jobLabel3:setPosition(190,bg:getContentSize().height -340)
    bg:addChild(jobLabel3)


    jobTips = ccui.ImageView:create("create_role/tips_1.png");
    jobTips:setAnchorPoint(cc.p(0.5,0.5))
    jobTips:setPosition(bg:getContentSize().width/2,bg:getContentSize().height -430)
    bg:addChild(jobTips)

    local createButton = UIHelper.createScaleButton("确定",20,"create_role/create_btn.png",function(node)
        if editName:getText() ~=  "" then
            createRoleFinish()
        else
            UIHelper.showToast(layer,"请输入角色名称！")
        end
    end);
    createButton:setPosition(bg:getContentSize().width/2,bg:getContentSize().height -530)
    bg:addChild(createButton)

    return layer
end

function addSpriteTouchEvent(role,id)

    --触摸处理函数
    local function onTouchBegan( touch,event )
        local target = event:getCurrentTarget()
        local rect   = target:getBoundingBox()
        if cc.rectContainsPoint(rect, touch:getLocation()) then
            if id == 0 then
                createRole(role)
            else
                selectRole(role,id)
            end

            return true
        end
        --必须设置不然拖动屏幕精灵也会移动
        return false

    end

    local function onTouchEnded( touch,event )

    end

    local function onTouchMoved(touch,event )
        local target = event:getCurrentTarget()
        local x,y = target:getPosition()
        --target:setPosition(cc.p(x + touch:getDelta().x, y + touch:getDelta().y))
    end

    -- 创建触摸监听事件
    local listener = cc.EventListenerTouchOneByOne:create()
    -- 吞没事件
    listener:setSwallowTouches(true);
    --将触摸结束事件与处理函数绑定
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED)

    --获取eventDispatcher
    local eventDispatcher = scene:getEventDispatcher()
    --将监听事件绑定到精灵上
    if id == 0 then
        eventDispatcher:addEventListenerWithSceneGraphPriority(listener, roleSpriteTable[role.id])
    else
        eventDispatcher:addEventListenerWithSceneGraphPriority(listener, roleSelectSpriteTable[id])
    end

end


function createRole(role)
    local visibleSize = cc.Director:getInstance():getVisibleSize()
    currentCreateRole = role

    if roleInputLayer == nil then
        roleInputLayer = RoleInputLayer()
        scene:addChild(roleInputLayer)
    end

    currentLayerIndex = 4
    for i= 1, #roleSpriteTable do

        if i == role.id then
            roleSpriteTable[i]:setPosition( cc.p( visibleSize.width/2, visibleSize.height/2 - 50) )
            roleSpriteTable[i]:setScale(1.0)
            roleSpriteTable[i]:setLocalZOrder(9)
            runSpriteAction(roleSpriteTable[i],role,"show")

            if role.sex == 1 then
                sexButton1:setBrightStyle(1)
                sexButton2:setBrightStyle(0)
            else
                sexButton1:setBrightStyle(0)
                sexButton2:setBrightStyle(1)
            end

            if role.job == 1 then
                jobButton1:setBrightStyle(1) --选中样式
                jobButton2:setBrightStyle(0) --不选中样式
                jobButton3:setBrightStyle(0) --不选中样式
                jobTips:loadTexture("create_role/tips_1.png")

            elseif role.job == 2 then
                jobButton1:setBrightStyle(0)
                jobButton2:setBrightStyle(1)
                jobButton3:setBrightStyle(0)
                jobTips:loadTexture("create_role/tips_2.png");
            else
                jobButton1:setBrightStyle(0)
                jobButton2:setBrightStyle(0)
                jobButton3:setBrightStyle(1)
                jobTips:loadTexture("create_role/tips_3.png");
            end

        else
            roleSpriteTable[i]:setPosition(cc.p(roleInfo[i].x, roleInfo[i].y))
            roleSpriteTable[i]:setScale(roleInfo[i].scale)
            roleSpriteTable[i]:setLocalZOrder(1)
            runSpriteAction(roleSpriteTable[i],roleInfo[i],"stand")
        end

    end

end

function selectRole(role,index)
    cclog("选择角色：" .. role.name .. "," .. index)
    currentSelectRole = role
    for i = 1,#roleSelectSpriteTable do
        if i == index then
            roleSelectSpriteTable[i]:setLocalZOrder(2)
            -- 设置回原来的颜色
            roleSelectSpriteTable[i]:setColor(cc.c3b(255, 255, 255))
            roleSelectSpriteTable[i]:setOpacity(255)
            runSpriteAction(roleSelectSpriteTable[i],role,"show")

            roleSelectNameTable[i]:setTextColor( cc.c3b(0, 250, 154) )
            roleSelectLvTable[i]:setTextColor( cc.c3b(0, 250, 154) )
        else
            roleSelectSpriteTable[i]:setColor(cc.c3b(153, 153, 153))
            roleSelectNameTable[i]:setTextColor( cc.c3b(153, 153, 153) )
            roleSelectLvTable[i]:setTextColor( cc.c3b(153, 153, 153) )
            roleSelectSpriteTable[i]:setOpacity(200)
            roleSelectSpriteTable[i]:setLocalZOrder(1)
        end
    end
end

function runSpriteAction(sprite,role,state)
    sprite:stopAllActions()
    local cache = cc.SpriteFrameCache:getInstance()
    local rolePath = "create_role_" .. role.job .. "_" .. role.sex .. "_" .. state;
    local animFrames = {}
    for j = 0,40 do
        local frame = cache:getSpriteFrame( string.format(rolePath .."/000%02d.png", j) )
        if frame then
            animFrames[j] = frame
        else
            break
        end
    end
    local animation = cc.Animation:createWithSpriteFrames(animFrames, 0.1)
    if state == "stand" then
        sprite:runAction(cc.RepeatForever:create( cc.Animate:create(animation) ))
    else
        local sequence = cc.Sequence:create(cc.Animate:create(animation),cc.CallFunc:create(function()
            runSpriteAction(sprite,role,"stand")
        end))
        sprite:runAction(sequence)
    end

end


function initSpriteFrameCache()

    local cache = cc.SpriteFrameCache:getInstance()

    -- 战士
    cache:addSpriteFrames("create_role/plist/create_role_1_1_stand@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_1_2_stand@0.plist")

    -- 法师
    cache:addSpriteFrames("create_role/plist/create_role_2_1_stand@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_2_2_stand@0.plist")

    -- 道士
    cache:addSpriteFrames("create_role/plist/create_role_3_1_stand@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_3_2_stand@0.plist")

    cache:addSpriteFrames("create_role/plist/create_role_1_1_show@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_1_2_show@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_2_1_show@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_2_2_show@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_3_1_show@0.plist")
    cache:addSpriteFrames("create_role/plist/create_role_3_2_show@0.plist")

end

function createRoleFinish()

    -- 保存角色信息
    GameData.createRoleData(editName:getText(),currentCreateRole.job,currentCreateRole.sex)

    UIHelper.showToast(backgroundLayer,"角色创建成功")

    roleInputLayer:removeFromParent()
    roleInputLayer = nil
    roleCreateLayer:removeFromParent()
    roleCreateLayer = nil

    roleSelectLayer = RoleSelectLayer()
    scene:addChild(roleSelectLayer)
    selectRole(GameData.data[#GameData.data],#GameData.data)

    currentLayerIndex = 3

end

function RoleCreateSceneMain()
    scene = cc.Scene:create()

    initSpriteFrameCache()

    backgroundLayer = BackgroundLayer()
    scene:addChild(backgroundLayer)

    if #GameData.data > 0 then

        roleSelectLayer = RoleSelectLayer()
        scene:addChild(roleSelectLayer)
        selectRole(GameData.data[1],1)
        currentLayerIndex = 3

    else
        roleCreateLayer = RoleCreateLayer()
        scene:addChild(roleCreateLayer)
        currentLayerIndex = 4
    end

    return scene
end

