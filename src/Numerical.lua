

Numerical = {

    getMoveFrameSpeed = nil,
    getAttackFrameSpeed = nil,
    getHurtNumber = nil,
}


Numerical.getMoveFrameSpeed = function(baseMoveSpeed,moveSpeed)

    --移动速度
    local frameSpeed = baseMoveSpeed/moveSpeed * 0.8
    return frameSpeed,frameSpeed * 0.03
end

Numerical.getAttackFrameSpeed = function(baseAttackSpeed,attackSpeed)

    --攻击速度
    local frameSpeed = baseAttackSpeed/attackSpeed
    return frameSpeed * 0.5,frameSpeed * 0.5
end

Numerical.getHurtNumber = function(masterRoleData,targetRoleData,skill)
    --[[
    attack_min = 10,attack_max = 16
    magic_attack_min = 0,magic_attack_max = 0,
    dc_attack_min = 0,dc_attack_max = 0,
    defense_min = 6,defense_max = 9,
    magic_defence_min = 6,magic_defence_max = 9
    ]]

    -- 真实伤害数值 攻击力 - 防御力 * 0.15
    local hp = 0
    local hurt = 0
    if not skill.add_attack then
        skill.add_attack = 0
    end
    if skill.attack_type == 1 then
        hurt = math.random(masterRoleData.attack_min,masterRoleData.attack_max) + skill.add_attack - math.random(targetRoleData.defence_min,targetRoleData.defence_max)
    elseif skill.attack_type == 2 then
        if skill.job == 2 then
            hurt = math.random(masterRoleData.magic_attack_min,masterRoleData.magic_attack_max) + skill.add_attack - math.random(targetRoleData.magic_defence_min,targetRoleData.magic_defence_max)
        elseif skill.job == 3 then
            hurt = math.random(masterRoleData.dc_attack_min,masterRoleData.dc_attack_max) + skill.add_attack - math.random(targetRoleData.magic_defence_min,targetRoleData.magic_defence_max)
        end
    end
    if hurt <= 0 then
        hurt = 1
        hp = 1
    else
        hp = math.floor(hurt * 0.15 + 0.5)
    end

    return hp,hurt
end