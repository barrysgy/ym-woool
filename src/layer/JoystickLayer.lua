
--------------------------------------------
---- JoystickLayer
--------------------------------------------
JoystickLayer = class("JoystickLayer", function()
    return cc.Layer:create()
end)

function JoystickLayer:ctor(parent)
    self._mapLayer = parent
    self._joystickNode = nil
    self._joystick = nil
    self._speedState = 0
    self._directionType = 0
    self:init()
end

function JoystickLayer:init()

    local origin = cc.Director:getInstance():getVisibleOrigin()

    self._joystickNode = cc.Sprite:create("main/joystick_bg.png")
    self._joystickNode:setPosition(origin.x + self._joystickNode:getContentSize().width/2 + 80, origin.y + self._joystickNode:getContentSize().height + 80)
    self._joystickNode:setAnchorPoint(cc.p(0.5,0.5))

    self._joystick = cc.Sprite:create("main/joystick.png")
    self._joystick:setPosition(self._joystickNode:getContentSize().width/2, self._joystickNode:getContentSize().height/2)
    self._joystickNode:addChild(self._joystick)

    self:addChild(self._joystickNode)

    local function onTouchBegan( touch,event )
        local rect   = cc.rect(0, 0, 500, 500)
        if cc.rectContainsPoint(rect, touch:getLocation()) then
            self._joystickNode:setPosition(touch:getLocation())
            return true
        end
        return false

    end

    local function onTouchEnded( touch,event )

        local origin = cc.Director:getInstance():getVisibleOrigin()
        self._joystickNode:setPosition(origin.x + self._joystickNode:getContentSize().width/2 + 80, origin.y + self._joystickNode:getContentSize().height + 80)
        self._joystick:setPosition(self._joystickNode:getContentSize().width/2, self._joystickNode:getContentSize().height/2)

        self._joystickNode:setColor(cc.c3b(255, 255, 255))
        self._speedState = SpeedState.StayState
        self._mapLayer:onJoystickTouch(self._directionType,self._speedState)

    end

    local function onTouchMoved(touch,event)
        local posX,posY = self._joystickNode:getPosition()
        local p1 = cc.p(posX,posY)
        local p2 = touch:getLocation()

        local rad = Helper.getRad(p1,p2)
        --用户触摸点到摇杆的中心的距离
        local touchRadius = math.sqrt(math.pow(p2.x - p1.x, 2) + math.pow(p2.y - p1.y, 2))
        local normalRadius  = 80
        local quickRadius = 160
        local radius = 0
        if touchRadius < normalRadius then
            radius = touchRadius
        else
            radius = normalRadius
        end

        if touchRadius >= quickRadius then

            --加速状态
            radius = quickRadius;
            self._joystickNode:setColor(cc.c3b(0, 250, 154))
            self._speedState = SpeedState.QuickState

        else
            --普通状态
            self._joystickNode:setColor(cc.c3b(255, 255, 255))
            self._speedState = SpeedState.NormalState
        end

        --设置 摇杆的位置
        local position = cc.p(radius * math.cos(rad) + self._joystickNode:getContentSize().width/2, radius * math.sin(rad) + self._joystickNode:getContentSize().height/2)
        self._joystick:setPosition(position)


        --弧度转化成角度
        local angle = 180 / math.pi * rad;
        if (angle >= 0 and angle < 22.5) or (angle >= 337.5 and angle < 360) then  --右
            self._directionType = DirectionType.RightType
        end
        if (angle >= 22.5 and angle < 67.5) then --右上
            self._directionType = DirectionType.RightUpType
        end
        if (angle >= 67.5 and angle < 112.5) then --上
            self._directionType = DirectionType.UpType
        end
        if (angle >= 112.5 and angle < 157.5) then --左上
            self._directionType = DirectionType.LeftUpType
        end
        if (angle >= 157.5 and angle < 202.5) then --左
            self._directionType = DirectionType.LeftType
        end
        if (angle >= 202.5 and angle < 247.5) then --左下
            self._directionType = DirectionType.LeftDownType
        end
        if (angle >= 247.5 and angle < 292.5) then --下
            self._directionType = DirectionType.DownType
        end
        if (angle >= 292.5 and angle < 337.5) then --右下
            self._directionType = DirectionType.RightDownType
        end

        self._mapLayer:onJoystickTouch(self._directionType,self._speedState)

    end

    local listener = cc.EventListenerTouchOneByOne:create()
    --不能吞没底层事件
    --listener:setSwallowTouches(true);
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED)

    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener,self)
end


