
--------------------------------------------
---- MinMapLayer
--------------------------------------------
MinMapLayer = class("MinMapLayer", function()
    return cc.Layer:create()
end)

function MinMapLayer:ctor(mapLayer,mapId)
    self._mapLayer = mapLayer
    self:init(mapId)
end

function MinMapLayer:init(mapId)



    local mapBaseData = require("config/MapBaseData")
    self._map =  mapBaseData[mapId]

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    -- 小地图
    local minMapBackground = cc.Sprite:create("main/min_map.png")
    local minMapImage = cc.Sprite:create("map/small/"..self._map.model..".jpg")

    local wScale = minMapBackground:getContentSize().width/minMapImage:getContentSize().width
    local hScale = minMapBackground:getContentSize().height/minMapImage:getContentSize().height

    self._mapImageSize = minMapImage:getContentSize()
    self._minMapScale = 1
    if wScale < hScale then
        self._minMapScale = wScale
    else
        self._minMapScale = hScale
    end

    minMapImage:setPosition(origin.x + visibleSize.width,origin.y + visibleSize.height - 30)
    minMapImage:setAnchorPoint(cc.p(1,1))
    minMapImage:setScale(self._minMapScale)
    self:addChild(minMapImage)

    minMapBackground:setPosition(origin.x + visibleSize.width,origin.y + visibleSize.height)
    minMapBackground:setAnchorPoint(cc.p(1,1))
    self:addChild(minMapBackground)

    local mapNameLabel = UIHelper.createTTFLabel(self._map.name,22)
    mapNameLabel:setTextColor( cc.c3b(0, 250, 154))
    mapNameLabel:setAnchorPoint(cc.p(0.5,0.5))
    mapNameLabel:setPosition(minMapBackground:getContentSize().width/2,minMapBackground:getContentSize().height - mapNameLabel:getContentSize().height/2 -8)
    minMapBackground:addChild(mapNameLabel)

    self._mapPosLabel = UIHelper.createTTFLabel("(0,0)",18)
    self._mapPosLabel:setTextColor( cc.c3b(0, 250, 154))
    self._mapPosLabel:setAnchorPoint(cc.p(0.5,0.5))
    self._mapPosLabel:setPosition(minMapBackground:getContentSize().width/2,self._mapPosLabel:getContentSize().height/2 + 8)
    minMapBackground:addChild(self._mapPosLabel)

    -- 绿色的点
    self._minMapPlayer = cc.DrawNode:create()
    self._minMapPlayer:setPosition(0,0)
    self._minMapPlayer:setAnchorPoint(cc.p(0.5,0.5))
    self._minMapPlayer:drawDot(cc.p(0,0),10,cc.c4b(1,1,1,1))
    minMapImage:addChild(self._minMapPlayer)

    --绑定到MapLayer
    self._mapLayer:setMinMapLayer(self)

end


function MinMapLayer:setPlayerPosition(position)
    --缩放倍数计算
    local x =  position.x * (self._mapImageSize.width  / (self._map.width * TileWidth))
    local y =  position.y * (self._mapImageSize.height / (self._map.height * TileHeight))
    self._minMapPlayer:setPosition(x,y)
    local mapPosition = Helper.positionToMapCoordinate(position,self._mapLayer._mapSize)
    self._mapPosLabel:setString("(" .. mapPosition.x .. "," .. mapPosition.y .. ")")
end

