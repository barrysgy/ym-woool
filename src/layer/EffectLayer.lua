
--------------------------------------------
---- EffectLayer
---  特效层：包括技能特效
--------------------------------------------
require("sprite/SkillSprite")

EffectLayer = class("EffectLayer", function()
    return cc.Layer:create()
end)

function EffectLayer:ctor(mapLayer)
    self._mapLayer = mapLayer
    self:init()
end

function EffectLayer:init()

    self._effectTable = {}

    for i = 1,6 do
        local skillSprite = SkillSprite.new(self._mapLayer,self)
        table.insert(self._effectTable,skillSprite)
        cclog("添加技能特效:" .. i)
        self:addChild(skillSprite)
    end

    --伤害范围
    self._hurtRectSprite = cc.DrawNode:create()
    self:addChild(self._hurtRectSprite)

end


function EffectLayer:updateEffect(masterSprite,skill)
    for i = 1,#self._effectTable do
        local skillSprite = self._effectTable[i]
        if not skillSprite:isVisible() then
            cclog("EffectLayer 获取到特效.............")
            skillSprite:setPosition(masterSprite:getPosition())
            skillSprite:updateEffect(masterSprite,skill)
            return
        end
    end

    --没有空闲 取第一个
    cclog("没有空闲 取第一个")
    local skillSprite = self._effectTable[1]
    skillSprite:setPosition(masterSprite:getPosition())
    skillSprite:updateEffect(masterSprite,skill)
end

function EffectLayer:updateHurtRangRect(hurtRect)
    self._hurtRectSprite:clear()
    for i = 1,#hurtRect do
        local rect = hurtRect[i]

        --cclog("updateHurtRangRect:" .. rect.x .."," ..rect.y)
        --绘制伤害矩形
        local points = { cc.p(rect.x, rect.y), cc.p(rect.x + rect.width, rect.y),
                         cc.p(rect.x + rect.width, rect.y + rect.height),cc.p(rect.x, rect.y + rect.height)}

        --绘制多边形(‘顶点数组’, ‘顶点个数’ , ‘填充颜色’ , ‘轮廓粗细’ , ‘轮廓颜色’)    c4f 要除以255
        self._hurtRectSprite:drawPolygon(points, table.getn(points), cc.c4f(1,0.7,0.7,0.3), 1, cc.c4f(1,0.7,0.7,0.6))
    end
end


function EffectLayer:clearHurtRangRect()
    self._hurtRectSprite:clear()
end
