
--------------------------------------------
---- RoleInfoLayer
--------------------------------------------
RoleInfoLayer = class("RoleInfoLayer", function()
    return cc.Layer:create()
end)

function RoleInfoLayer:ctor()
    self:init()
end

function RoleInfoLayer:init()

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local background = cc.Sprite:create("main/role/bg.png")
    background:setPosition(visibleSize.width/2, visibleSize.height/2)
    background:setAnchorPoint(cc.p(0.5,0.5))
    self:addChild(background)

    local roleMenu = UIHelper.createButton("角色",20,"main/ui/menu_normal.png", "main/button/menu_selected.png")
    roleMenu:setPosition(10, background:getContentSize().height - 10)
    roleMenu:setAnchorPoint(cc.p(0,1))
    background:addChild(roleMenu)

    local roleBackground = cc.Sprite:create("main/role/role_bg.png")
    roleBackground:setPosition(120, background:getContentSize().height/2)
    roleBackground:setAnchorPoint(cc.p(0,0.5))
    background:addChild(roleBackground)

    local rightBackground = cc.Sprite:create("main/role/left_bg.png")
    rightBackground:setPosition(130 + roleBackground:getContentSize().width, background:getContentSize().height/2)
    rightBackground:setAnchorPoint(cc.p(0,0.5))
    background:addChild(rightBackground)

    local closeButton = ccui.Button:create("main/ui/close.png")
    closeButton:setPosition(background:getContentSize().width, background:getContentSize().height)
    closeButton:setAnchorPoint(cc.p(1,1))
    background:addChild(closeButton)

    closeButton:addClickEventListener(function(sender)
        self:setVisible(false)
    end)

end

