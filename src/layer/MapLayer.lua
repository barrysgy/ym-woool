
--------------------------------------------
---- MapLayer
--------------------------------------------
---
require ("sprite/TouchableSprite")
require("sprite/PlayerSprite")
require("sprite/TransferSprite")
require("sprite/MonsterSprite")
require("sprite/NPCSprite")
require ("layer/EffectLayer")


MapLayer = class("MapLayer", function()
    return cc.Layer:create()
end)

function MapLayer:ctor(scene,mapId)
    self._scene = scene
    self:init(mapId)
end


function MapLayer:init(mapId)

    --基础数据
    self._npcs = require("config/NPCUpdate")
    self._monsters = require("config/MonsterUpdate")
    self._maps = require("config/MapBaseData")


    self._map =  self._maps[mapId]

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local color = cc.LayerColor:create( cc.c4b(64,64,64,255) )
    self:addChild(color, -1)

    self._tiledMap = cc.FastTMXTiledMap:create("map/tiled/"..self._map.model..".tmx")
    self._tiledMap:setPosition(0, 0)
    self._tiledMap:setScale(1)
    self._tiledMap:setAnchorPoint(cc.p(0.0,0.0))
    self:addChild(self._tiledMap)

    local blockLayer  = self._tiledMap:getLayer("block")
    local alphaLayer  = self._tiledMap:getLayer("alpha")
    --tile的数量
    self._mapSize = blockLayer:getLayerSize()

    --用于智能寻路和判断碰撞遮挡的初始数据
    self._mapPath = {}
    --生成地图路径数据
    for row = 0, self._mapSize.height-1 do
        self._mapPath[row] = {}
        for  col = 0, self._mapSize.width-1 do
            local blockTile = blockLayer:getTileAt(cc.p(col,row))
            local alphaTile = alphaLayer:getTileAt(cc.p(col,row))
            if blockTile ~= nil then
                self._mapPath[row][col] = 0
            elseif alphaTile ~= nil then
                self._mapPath[row][col] = 2
            else
                self._mapPath[row][col] = 1
            end
        end
    end

    --NPC
    self._npcTable = {}
    for i = 1,#self._npcs do
        if self._npcs[i].map == self._map.id then
            local npc = NPCSprite.new(self,self._npcs[i])
            local position = Helper.mapCoordinateToPosition(cc.p(self._npcs[i].x,self._npcs[i].y),self._mapSize)
            npc:updatePosition(position)
            cclog("添加NPC:" .. self._npcs[i].id)
            self:addChild(npc)
            table.insert(self._npcTable,npc)
        end
    end

    --传送门
    self._transfer = TransferSprite.new(self,"zz")
    self:addChild(self._transfer)
    local position = Helper.mapCoordinateToPosition(cc.p(6,9),self._mapSize)
    self._transfer:updatePosition(position)

    --怪物更新
    self._monsterTable = {}
    self:createMonster()

    --刷新怪物
    Helper.schedule(self,function()
        self:refreshMonster()
    end,60,0)

    -- 玩家宠物
    self._petTable = {}

    -- 玩家角色
    self._playerSprite = PlayerSprite.new(self,GameData.currentRole)
    self:addChild(self._playerSprite)
    local position = Helper.mapCoordinateToPosition(cc.p(GameData.currentRole.x,GameData.currentRole.y),self._mapSize)
    self._playerSprite:updatePosition(position)


    --玩家拖尾
    --fade         : 拖尾渐隐时间（秒）
    --minSeg       : 最小的片段长度（渐隐片段的大小）。拖尾条带相连顶点间的最小距离
    --stroke       : 渐隐条带的宽度
    --color        : 片段颜色值
    --path         : 纹理图片的文件名
    --texture      : 纹理图片的对象指针
    --self._motionStreak = cc.MotionStreak:create(10, 10, self._playerSprite._mainSprite:getContentSize().height, cc.c3b(255, 255, 255), "main/sprite.png")
    --self._motionStreak:setPosition(position)
    --self:addChild(self._motionStreak)


    --local testBlock = TouchableSprite.new()
    --testBlock:setTexture("map/tiled/alpha#0_0_48_32.png")
    --testBlock:setAnchorPoint(cc.p(0.5,0.5))
    --testBlock:setPriority(30)
    --testBlock:setPosition(500,500)
    --self:addChild(testBlock, 10)


    --技能特效
    self._effectLayer = EffectLayer.new(self)
    self:addChild(self._effectLayer)

    --路线规划
    self._planPath = cc.DrawNode:create()
    self._planPath:setAnchorPoint(cc.p(0.5,0.5))
    self:addChild(self._planPath)

    -- 镜头跟踪
    if self._camera == nil then

        self._camera = cc.Camera:createOrthographic(visibleSize.width, visibleSize.height, 0, 1)
        self._camera:setCameraFlag(cc.CameraFlag.USER1)
        self:addChild(self._camera)
        self:cameraUpdate()
    end

    --哪些节点跟随镜头移动
    self:setCameraMask(cc.CameraFlag.USER1)

    cclog("地图Layer加载完成...............................................")

end


function MapLayer:cameraUpdate()
    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local posX,posY = self._playerSprite:getPosition()

    --边界限制
    if posX > self._tiledMap:getContentSize().width - visibleSize.width/2  then
        posX = self._tiledMap:getContentSize().width - visibleSize.width/2
    end
    if posX < visibleSize.width/2 then
        posX = visibleSize.width/2
    end

    if posY > self._tiledMap:getContentSize().height - visibleSize.height/2 then
        posY = self._tiledMap:getContentSize().height - visibleSize.height/2
    end
    if posY < visibleSize.height/2 then
        posY = visibleSize.height/2
    end

    if self._camera ~= nil then
        self._camera:setPosition3D(cc.vec3(posX - visibleSize.width/2, posY - visibleSize.height/2, 0.0));
        self._cameraPosition = cc.p(posX - visibleSize.width/2,posY - visibleSize.height/2)
        --cclog("camera:" .. self._cameraPosition.x .. "," .. self._cameraPosition.y)
    end

    --更新小地图
    self:updateMinMap()

end

function MapLayer:updatePlanPath(path)
    cclog("绘制路径规划........................................")
    self._planPath:clear()
    for i = 1,#path do
        if i < #path then
            local pos1 = Helper.mapCoordinateToPosition(cc.p(path[i].x,path[i].y),self._mapSize)
            local pos2 = Helper.mapCoordinateToPosition(cc.p(path[i+1].x,path[i+1].y),self._mapSize)
            self._planPath:drawSegment(pos1,pos2,6,cc.c4f(1,1,1,0.4))
        end
    end
end


function MapLayer:createMonster()

    for i = 1,#self._monsters do
        if self._monsters[i].map == self._map.id then

            for j = 1 ,self._monsters[i].monster_count do

                -- 定义ID y要拷贝
                local monsterData = clone(self._monsters[i])
                monsterData.id = self._monsters[i].id + j - 1
                --位置分散开位置半径radius

                if self._monsters[i].radius > 0 then
                    local position = Helper.randomPosition(cc.p(self._monsters[i].x,self._monsters[i].y),self._monsters[i].radius)
                    monsterData.x = position.x
                    monsterData.y = position.y
                end

                local monster = MonsterSprite.new(self,monsterData)
                local position = Helper.mapCoordinateToPosition(cc.p(monsterData.x,monsterData.y),self._mapSize)
                monster:updatePosition(position)
                --注册点击事件
                self:registerTouchEvent(monster)

                -- 维护怪物
                table.insert(self._monsterTable,monster)

                cclog("添加怪物:" .. self._monsters[i].id)
                self:addChild(monster)

            end

        end
    end
end

function MapLayer:createPet(monster_base_id,position)

    local monsterData = {id = monster_base_id + 100,base_id = monster_base_id,x = position.x,y = position.y,radius = 4,direction = 3,move_speed = 215}

    local monster = MonsterSprite.new(self,monsterData)
    monster:updatePosition(position)
    --注册点击事件
    self:registerTouchEvent(monster)
    -- 维护宠物
    table.insert(self._petTable,monster)
    cclog("添加宠物:" .. monsterData.id)
    self:addChild(monster)
    self:setCameraMask(cc.CameraFlag.USER1)

end

function MapLayer:refreshMonster()
    cclog("复活刷新怪物.................................")
    for i = 1,#self._monsterTable do
        local monster = self._monsterTable[i]
        if  monster._isDeath then
            local mapPosition = cc.p(monster._roleData.x,monster._roleData.y)
            if monster._roleData.radius > 0 then
                mapPosition = Helper.randomPosition(mapPosition,monster._roleData.radius)
            end
            local position = Helper.mapCoordinateToPosition(mapPosition,self._mapSize)
            monster:revive(position)
        end
    end
end

-- 碰撞检测
-- 碰撞检测的node
-- 发生碰撞的位置target point
function MapLayer:isSpriteCollision(sprite,position)

    local posX,posY = sprite:getPosition()
    local targetRect = Helper.getCollisionRect(position)

    local spritePosition = Helper.positionToMapCoordinate(cc.p(posX,posY),self._mapSize)
    local targetPosition = Helper.positionToMapCoordinate(position,self._mapSize)

    sprite._roleNameLabel:setTextColor( cc.c3b(255, 255, 255) )
    sprite._mainSprite:setOpacity(255)

    for row = 0,#self._mapPath-1 do
        for col = 0,#self._mapPath[row]-1 do

            if targetPosition.x == col and targetPosition.y == row then
                if self._mapPath[row][col] == 0 then
                    sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
                    return true
                end
            end

            if spritePosition.x == col and spritePosition.y == row then
                if self._mapPath[row][col] == 2 then
                    sprite._mainSprite:setOpacity(150)
                end
            end
        end
    end

    --检测与其他怪物的碰撞
    for i=1,#self._monsterTable do

        if  not self._monsterTable[i]._isDeath and not sprite._isDeath and  self._monsterTable[i]._roleData.id ~= sprite._roleData.id then
            if cc.rectIntersectsRect(self._monsterTable[i]._collisionRect,targetRect) then
                sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
                --和其他怪物发生碰撞
                --return true
            end
        end
    end

    --检测与玩家的碰撞
    if not sprite._isDeath and sprite._roleData.id ~= self._playerSprite._roleData.id then
        if cc.rectIntersectsRect(self._playerSprite._collisionRect, targetRect) then
            --和角色发生碰撞
            sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
            --return true
        end
    end

    --检测与NPC的碰撞
    for i=1,#self._npcTable do
        if  not self._npcTable[i]._isDeath and not sprite._isDeath and self._npcTable[i]._roleData.id ~= sprite._roleData.id then
            if cc.rectIntersectsRect(self._npcTable[i]._collisionRect,targetRect) then
                sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
                return true
            end
        end
    end

    --角色边界限制
    if position.x > self._tiledMap:getContentSize().width - TileWidth  then
        sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
        return true
    end
    if position.x  < TileWidth then
        sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
        return true
    end

    if position.y > self._tiledMap:getContentSize().height - TileHeight then
        sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
        return true
    end
    if position.y < TileHeight then
        sprite._roleNameLabel:setTextColor( cc.c3b(255, 0, 0) )
        return true
    end

    -- false 表示未碰撞
    return false
end

--判断技能的碰撞
function MapLayer:isSkillCollision(masterSprite,hurtRect)
    local collision = false
    local position = nil

    if masterSprite._roleData.role_type == RoleType.Player or masterSprite._roleData.role_type == RoleType.Pet then
        --判断地图上全部怪物
        for i=1,#self._monsterTable do
            if not self._monsterTable[i]._isDeath  and masterSprite ~= self._monsterTable[i] then
                local collisionRect = self._monsterTable[i]._collisionRect
                for j=1,#hurtRect do
                    if cc.rectIntersectsRect(hurtRect[j], collisionRect) then
                        collision = true
                        local posX,posY = self._monsterTable[i]:getPosition()
                        position = cc.p(posX,posY)
                        cclog("检测技能碰撞:" .. self._monsterTable[i]._roleData.id..","..self._monsterTable[i]._roleData.name)
                        return collision,position
                    end
                end
            end
        end
    else
        local collisionRect = self._playerSprite._collisionRect
        for j=1,#hurtRect do
            if cc.rectIntersectsRect(hurtRect[j], collisionRect) then
                collision = true
                local posX,posY = self._playerSprite:getPosition()
                position = cc.p(posX,posY)
                cclog("检测技能碰撞:"..self._playerSprite._roleData.name)
                return collision,position
            end
        end
    end

    return collision,position

end

--执行伤害
function MapLayer:skillHurt(masterSprite,skill,hurtRect)
    if masterSprite._roleData.role_type == RoleType.Player or masterSprite._roleData.role_type == RoleType.Pet then
        for i=1,#self._monsterTable do
            if not self._monsterTable[i]._isDeath and self._monsterTable[i] ~= masterSprite then
                local collisionRect = self._monsterTable[i]._collisionRect
                for j=1,#hurtRect do
                    if cc.rectIntersectsRect(hurtRect[j], collisionRect) then
                        if not self._monsterTable[i]._isDeath then
                            self._monsterTable[i]:hurt(masterSprite,skill)
                            cclog("怪物受伤:" .. self._monsterTable[i]._roleData.id..","..self._monsterTable[i]._roleData.name)
                        end
                    end
                end
            end
        end
    else
        local collisionRect = self._playerSprite._collisionRect
        for j=1,#hurtRect do
            if cc.rectIntersectsRect(hurtRect[j], collisionRect) then
                if not self._playerSprite._isDeath then
                    self._playerSprite:hurt(masterSprite,skill)
                    cclog("玩家受伤:" .. self._playerSprite._roleData.name)
                end
            end
        end
    end
end

-- 执行死亡
function MapLayer:removeTargetFromSprite(sprite)

    if self._playerSprite._targetSprite == sprite then
        self._playerSprite._targetSprite = nil
    end
    for k, v in pairs(self._monsterTable) do
        if v._targetSprite == sprite then
            v._targetSprite = nil
        end
    end
    for k, v in pairs(self._petTable) do
        if v._targetSprite == sprite then
            v._targetSprite = nil
        end
    end

end

-- 显示伤害数值
function MapLayer:showHurtNumber(number,position)

    local numImage = "res/main/number/hurt.png"
    if  number <= 0 then
        numImage = "res/main/number/3.png"
        number = (-1) * number
    end
    local rect,spans = {0,0,220,30},20
    local numberSprite = require("sprite/HurtSprite").new(numImage,number,rect,spans,true)
    numberSprite:setPosition(position)
    self:addChild(numberSprite,100)
    numberSprite:setVisible(true)
    numberSprite:setCascadeOpacityEnabled(true)
    self:setCameraMask(cc.CameraFlag.USER1)

    local actions = {}
    actions[#actions+1] = cc.DelayTime:create(0.25)
    actions[#actions+1] = cc.Show:create()
    actions[#actions+1] = cc.ScaleTo:create(0.25,1.1)
    actions[#actions+1] = cc.Spawn:create(cc.MoveBy:create(0.4,cc.p(0,80)),cc.FadeOut:create(0.8))
    actions[#actions+1] = cc.CallFunc:create(function()

    end)
    actions[#actions+1] = cc.RemoveSelf:create()
    numberSprite:runAction(cc.Sequence:create(actions))
end

-- 显示地图上特效
function MapLayer:playEffect(path,anchorPoint,position)
    local effectSprite = cc.Sprite:create()
    effectSprite:setAnchorPoint(anchorPoint)
    effectSprite:setPosition(position)
    effectSprite:setLocalZOrder(1)
    self:addChild(effectSprite)
    self:setCameraMask(cc.CameraFlag.USER1)

    local animFrames = Helper.getAnimFrames(path,20)
    local animate = cc.Animate:create(cc.Animation:createWithSpriteFrames(animFrames, 0.10))
    local sequence = cc.Sequence:create(animate,cc.CallFunc:create(function()
    end),cc.RemoveSelf:create())
    effectSprite:runAction(sequence)

end

function MapLayer:registerTouchEvent(sprite)

    local function onTouchBegan(touch,event)

        local target = event:getCurrentTarget()
        local locationInNode = target:convertToNodeSpace(touch:getLocation())
        local s = target:getContentSize()
        local rect = cc.rect(0, 0, s.width, s.height)
        --cclog(string.format("sprite began............................"))
        --cclog(string.format("x = %f, y = %f", locationInNode.x, locationInNode.y))
        --cclog(string.format("camera x = %f, y = %f", self._cameraPosition.x, self._cameraPosition.y))

        locationInNode.x = locationInNode.x + self._cameraPosition.x

        --用于矫正的 用于保证点击的位置在碰撞区域的中心
        locationInNode.y = locationInNode.y + self._cameraPosition.y

        --cclog(string.format("rect w = %f, h = %f", rect.width, rect.height))
        --cclog(string.format("covert x = %f, y = %f", locationInNode.x, locationInNode.y))
        if cc.rectContainsPoint(rect, locationInNode) then
            return true
        end

        return false
    end

    local function onTouchMoved(touch,event )
        --local target = event:getCurrentTarget()
        --local x,y = target:getPosition()
        --target:setPosition(cc.p(x + touch:getDelta().x, y + touch:getDelta().y))
    end


    local function onTouchEnded(touch,event)
        local target = event:getCurrentTarget()
        --cclog("MapLayer Layer onTouchEnded")

        --判断地图上全部怪物
        for i=1,#self._monsterTable do
            if target == self._monsterTable[i] then
                cclog("目标被选择:" .. self._monsterTable[i]._roleData.id..","..self._monsterTable[i]._roleData.name)
                target:setSelected(true)
                --设置玩家目标
                self._playerSprite._targetSprite = target
            end
        end
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    --listener:setSwallowTouches(true)
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED)
    if self._eventDispatcher == nil then
        --要用同一个eventDispatcher
        self.eventDispatcher = self:getEventDispatcher()
    end
    self.eventDispatcher:addEventListenerWithSceneGraphPriority(listener,sprite)
end

--小地图
function MapLayer:setMinMapLayer(minMapLayer)
    self._minMapLayer = minMapLayer
    local posX,posY = self._playerSprite:getPosition()
    self._minMapLayer:setPlayerPosition(cc.p(posX, posY));
end

--方向控制器回调
function MapLayer:onJoystickTouch(directionType,speedState)
    self._scene._skillLayer:closeAutoAttack()
    self._playerSprite:updateAction(directionType,speedState)
end

--技能点击回调
function MapLayer:onSkillTouch(skill,successCallback)
    return self._playerSprite:updateAttackAction(skill,successCallback)
end

--更新小地图玩家位置
function MapLayer:updateMinMap()
    if self._minMapLayer ~= nil then
        local posX,posY = self._playerSprite:getPosition()
        self._minMapLayer:setPlayerPosition(cc.p(posX, posY));
    end
end
