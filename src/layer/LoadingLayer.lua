
--------------------------------------------
---- LoadingLayer
--------------------------------------------
LoadingLayer = class("LoadingLayer", function()
    return cc.Layer:create()
end)


function LoadingLayer:ctor(mapId)
    self:init(mapId)
end

function LoadingLayer:init(mapId)

    local maps = require("config/MapBaseData")
    self._map =  maps[mapId]

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local bg = cc.Sprite:create("loading/1.jpg")
    bg:setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2)
    local scale = visibleSize.width / bg:getContentSize().width
    bg:setScale(scale)
    self:addChild(bg)

    local loadingBackground = cc.Sprite:create("loading/loading_bg.png")
    loadingBackground:setPosition(visibleSize.width/2,origin.y)
    loadingBackground:setAnchorPoint(cc.p(0.5,0.0))
    local loadingScale = visibleSize.width/loadingBackground:getContentSize().width
    loadingBackground:setScale(loadingScale)
    self:addChild(loadingBackground)

    self._progressBar = cc.ProgressTimer:create(cc.Sprite:create("loading/loading_pr.png"))
    self._progressBar:setPosition(0, loadingBackground:getContentSize().height/2)
    self._progressBar:setAnchorPoint(cc.p(0.0,0.5))
    self._progressBar:setBarChangeRate(cc.p(1, 0))
    self._progressBar:setMidpoint(cc.p(0,1))
    self._progressBar:setPercentage(0)
    self._progressBar:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    --self._progressBar:runAction(cc.RepeatForever:create(cc.ProgressTo:create(6, 100)))
    loadingBackground:addChild(self._progressBar)

    self._tipsLabel = UIHelper.createTTFLabel("Loading...",28)
    self._tipsLabel:setTextColor( cc.c3b(0, 250, 154))
    self._tipsLabel:setAnchorPoint(cc.p(0.5,0))
    self._tipsLabel:setPosition(cc.p(visibleSize.width/2, origin.y + 10))
    self:addChild(self._tipsLabel)

    -- logo动画
    local logo = cc.Sprite:create("logo.png")
    logo:setPosition(origin.x + visibleSize.width - logo:getContentSize().width * 0.5 - 40, origin.y + visibleSize.height - logo:getContentSize().height/2 - 40 )
    self:addChild(logo)

    logo:setOpacity(0)
    logo:setScale(0.01)
    logo:runAction(cc.Spawn:create(cc.FadeIn:create(0.5),
        cc.Sequence:create(cc.ScaleTo:create(0.2,1.2),cc.ScaleTo:create(0.2,1),cc.CallFunc:create(function()

        end)
    )))

end

