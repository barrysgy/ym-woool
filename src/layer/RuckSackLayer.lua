
--------------------------------------------
---- RuckSacklayer 背包
--------------------------------------------
RuckSacklayer = class("RuckSacklayer", function()
    return cc.Layer:create()
end)

function RuckSacklayer:ctor(playerSprite)
    self._playerSprite = playerSprite
    self:init()
end

function RuckSacklayer:init()

    self._itemBorderList = {}

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local background = cc.Sprite:create("main/role/bg.png")
    background:setPosition(visibleSize.width/2, origin.y + visibleSize.height/2)
    background:setAnchorPoint(cc.p(0.5,0.5))
    background:setScale(1.2)
    self:addChild(background)

    local roleMenu = UIHelper.createButton("全部",20,"main/ui/menu_normal.png", "main/ui/menu_selected.png")
    roleMenu:setPosition(30,background:getContentSize().height - 126)
    roleMenu:setAnchorPoint(cc.p(0,0))
    background:addChild(roleMenu)

    local gridBackground = cc.Sprite:create("main/role/right_bg.png")
    gridBackground:setPosition(40 + roleMenu:getContentSize().width, background:getContentSize().height/2 - 20)
    gridBackground:setAnchorPoint(cc.p(0,0.5))
    background:addChild(gridBackground)

    local closeButton = ccui.Button:create("main/ui/close.png")
    closeButton:setPosition(background:getContentSize().width, background:getContentSize().height)
    closeButton:setAnchorPoint(cc.p(1,1))
    background:addChild(closeButton)

    closeButton:addClickEventListener(function(sender)
        self:setVisible(false)
    end)

    local nameLabel = UIHelper.createTTFLabel("包裹",20)
    nameLabel:setTextColor( cc.c3b(255, 215, 0))
    nameLabel:setAnchorPoint(cc.p(0.5,0.5))
    nameLabel:setPosition(background:getContentSize().width/2,background:getContentSize().height - 50)
    background:addChild(nameLabel)


    --所有物品
    self._items = require("config/ItemBaseData")

    self._listView = ccui.ListView:create();
    self._listView:setAnchorPoint(cc.p(0.5,0.5))
    self._listView:setPosition(gridBackground:getContentSize().width/2,gridBackground:getContentSize().height/2 - 10);
    self._listView:setContentSize(cc.size(750, 460));
    self._listView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
    self._listView:setBounceEnabled(true);
    self._listView:setItemsMargin(10)


    local column = 8
    local row = 15

    for i = 1,row do
        local layout = ccui.Layout:create();
        layout:setContentSize(cc.size(750, 80));
        --layout:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid);
        --layout:setBackGroundColor(cc.c3b(0, 0, 0));

        for j = (i-1) * column + 1,i * column do
            local index = j - (i-1) * column - 1

            local itemBg = ccui.Button:create("main/role/item_bg.png")
            itemBg:setPosition(60 + index * 90,layout:getContentSize().height/2)

            if j <= #self._items then

                local itemSprite = cc.Sprite:create("item/" .. self._items[j].model .. ".png")
                if itemSprite then
                    itemSprite:setPosition(itemBg:getContentSize().width/2,itemBg:getContentSize().height/2)
                    itemSprite:setAnchorPoint(cc.p(0.5,0.5))
                    itemBg:addChild(itemSprite)

                    local itemLabel = UIHelper.createTTFLabel(self._items[j].name,14)
                    itemLabel:setTextColor( cc.c3b(255, 255, 255))
                    itemLabel:setAnchorPoint(cc.p(0.5,0))
                    itemLabel:setPosition(itemBg:getContentSize().width/2,10)
                    itemBg:addChild(itemLabel)
                else
                    local itemLabel = UIHelper.createTTFLabel(self._items[j].name,14)
                    itemLabel:setTextColor( cc.c3b(255, 255, 255))
                    itemLabel:setAnchorPoint(cc.p(0.5,0.5))
                    itemLabel:setPosition(itemBg:getContentSize().width/2,itemBg:getContentSize().height/2)
                    itemBg:addChild(itemLabel)
                end
            end

            local itemBorder = cc.Sprite:create("main/role/1.png")
            itemBorder:setPosition(itemBg:getContentSize().width/2,itemBg:getContentSize().height/2)
            itemBg:addChild(itemBorder)
            itemBorder:setVisible(false)

            table.insert(self._itemBorderList,itemBorder)

            layout:addChild(itemBg)

            itemBg:addClickEventListener(function(sender)

                for i=1,#self._itemBorderList do
                    if self._itemBorderList[i] ~= itemBorder then
                        self._itemBorderList[i]:setVisible(false)
                    else
                        itemBorder:setVisible(true)
                    end
                end

                self._playerSprite:changeClothes(self._items[j])

            end)
        end

        self._listView:pushBackCustomItem(layout);
    end
    gridBackground:addChild(self._listView);




end

