
--------------------------------------------
---- TestLayer
--------------------------------------------
TestLayer = class("TestLayer", function()
    return cc.Layer:create()
end)

function TestLayer:ctor()
    self:init()
end

function TestLayer:init()

    local joystickNode = cc.Sprite:create("logo.png")
    joystickNode:setPosition(joystickNode:getContentSize().width/2 + 100, joystickNode:getContentSize().height + 100)
    joystickNode:setAnchorPoint(cc.p(0.5,0.5))
    self:addChild(joystickNode)

end

