
--------------------------------------------
---- MainUILayer
--------------------------------------------

require("layer/RoleInfoLayer")
require("layer/RuckSackLayer")

MainUILayer = class("MainUILayer", function()
    return cc.Layer:create()
end)

function MainUILayer:ctor(parent)
    self._mapLayer = parent
    self:init()
end

function MainUILayer:init()

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    -- 角色头像
    self._roleHead = ccui.Button:create("main/head/1.png")
    self._roleHead:setPosition(origin.x + 15,origin.y + visibleSize.height - 10)
    self._roleHead:setAnchorPoint(cc.p(0,1))
    self:addChild(self._roleHead)

    local roleHeadBackground = cc.Sprite:create("main/role_bg.png")
    roleHeadBackground:setPosition(origin.x,origin.y + visibleSize.height)
    roleHeadBackground:setAnchorPoint(cc.p(0,1))
    self:addChild(roleHeadBackground)

    self._levelLabel = UIHelper.createTTFLabel("1",16)
    self._levelLabel:setTextColor( cc.c3b(255, 215, 0))
    self._levelLabel:setAnchorPoint(cc.p(0.5,0.5))
    self._levelLabel:setPosition(22,27)
    roleHeadBackground:addChild(self._levelLabel)

    local combat = cc.Sprite:create("main/combat.png")
    combat:setPosition(110,roleHeadBackground:getContentSize().height - 30)
    combat:setAnchorPoint(cc.p(0,0.5))
    roleHeadBackground:addChild(combat)

    local mainBottom = cc.Sprite:create("main/main_bottom.png")
    mainBottom:setPosition(origin.x + visibleSize.width/2,origin.y)
    mainBottom:setAnchorPoint(cc.p(0.5,0))
    mainBottom:setScale(1.2)
    self:addChild(mainBottom)

    self._hpProgress = cc.ProgressTimer:create(cc.Sprite:create("main/hp_progress.png"))
    self._hpProgress:setAnchorPoint(cc.p(0,0))
    self._hpProgress:setPosition(60,22)
    self._hpProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    self._hpProgress:setPercentage(0)
    self._hpProgress:setMidpoint(cc.p(1,0))
    self._hpProgress:setBarChangeRate(cc.p(0, 1))
    --self._hpProgress:runAction(cc.RepeatForever:create(cc.ProgressTo:create(20, 100)))

    self._mpProgress = cc.ProgressTimer:create(cc.Sprite:create("main/mp_progress.png"))
    self._mpProgress:setAnchorPoint(cc.p(0,0))
    self._mpProgress:setPosition(100,22)
    self._mpProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    self._mpProgress:setPercentage(0)
    self._mpProgress:setMidpoint(cc.p(1,0))
    self._mpProgress:setBarChangeRate(cc.p(0, 1))
    --self._mpProgress:runAction(cc.RepeatForever:create(cc.ProgressTo:create(20, 100)))

    self._expProgress = cc.ProgressTimer:create(cc.Sprite:create("main/exp.png"))
    self._expProgress:setAnchorPoint(cc.p(0,0))
    self._expProgress:setPosition(mainBottom:getContentSize().width/2 - self._expProgress:getContentSize().width/2,4)
    self._expProgress:setMidpoint(cc.p(0,1))
    self._expProgress:setBarChangeRate(cc.p(1, 0))
    self._expProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    self._expProgress:setPercentage(0)
    --self._expProgress:runAction(cc.RepeatForever:create(cc.ProgressTo:create(6, 100)))

    mainBottom:addChild(self._hpProgress)
    mainBottom:addChild(self._mpProgress)
    mainBottom:addChild(self._expProgress)


    local rucksack = ccui.Button:create("main/rucksack.png")
    local setup = ccui.Button:create("main/setup.png")

    rucksack:setPosition(40,85)
    setup:setPosition(40,40)
    mainBottom:addChild(rucksack)
    mainBottom:addChild(setup)


    self:updateUI()


    self._roleHead:addClickEventListener(function(sender)
        --角色信息
        if self._roleInfolayer == nil then
            self._roleInfolayer = RoleInfoLayer.new()
            self:addChild(self._roleInfolayer)
        end

        self._roleInfolayer:setVisible(not self._roleInfolayer:isVisible())
    end)

    rucksack:addClickEventListener(function(sender)
        --角色信息
        if self._ruckSacklayer == nil then
            self._ruckSacklayer = RuckSacklayer.new(self._mapLayer._playerSprite)
            self:addChild(self._ruckSacklayer)
        end

        self._ruckSacklayer:setVisible(not self._ruckSacklayer:isVisible())

    end)
end

function MainUILayer:updateUI()
    self._levelLabel:setString(self._mapLayer._playerSprite._roleData.level)
    self._expProgress:setPercentage(self._mapLayer._playerSprite._roleData.exp/self._mapLayer._playerSprite._roleData.max_exp * 100)
    self._hpProgress:setPercentage(self._mapLayer._playerSprite._roleData.hp/self._mapLayer._playerSprite._roleData.max_hp * 100)
    self._mpProgress:setPercentage(self._mapLayer._playerSprite._roleData.mp/self._mapLayer._playerSprite._roleData.max_mp * 100)
end