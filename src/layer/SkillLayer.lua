
--------------------------------------------
---- SkillLayer
--------------------------------------------
SkillLayer = class("SkillLayer", function()
    return cc.Layer:create()
end)

function SkillLayer:ctor(parent)
    self._mapLayer = parent
    self:init()
end

function SkillLayer:initSpriteFrameCache()

    local roleSkill = GameData.currentRole.skill
    local cache = cc.SpriteFrameCache:getInstance()
    for i=1,#roleSkill do
        cache:addSpriteFrames("skill/plist/s_" .. string.sub(roleSkill[i],1,4) .. "@0.plist")
        cache:addSpriteFrames("skill/plist/s_" .. string.sub(roleSkill[i],1,4) .. "@1.plist")
        cclog("加载技能资源：" .. roleSkill[i] ..":" ..string.sub(roleSkill[i],1,4))
    end
end

function SkillLayer:init()

    self:initSpriteFrameCache()
    self._skillTable = {}
    self._skillCDSpriteTable = {}

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    --自动攻击按钮
    self._autoAttack = ccui.Button:create("main/auto_fight_close.png","main/auto_fight_open.png")
    self._autoAttack:setPosition(visibleSize.width - self._autoAttack:getContentSize().width - 20,visibleSize.height - 200)
    self._autoAttack:setAnchorPoint(cc.p(0.5,0.5))
    self:addChild(self._autoAttack)
    self._autoAttackOpen = false
    self._autoAttack:addClickEventListener(function(sender)
        if self._autoAttackOpen then
            self:closeAutoAttack()
        else
            self:startAutoAttack()
        end
    end)

    -- 配置的技能
    local roleSkill = GameData.currentRole.skill

    local skillLevelData = require("config/SkillLevelData")
    local skillBaseData = require("config/SkillBaseData")

    for i = 1,#roleSkill do
        for j = 1,#skillLevelData do
            if skillLevelData[j].id == roleSkill[i] then
                self._skillTable[i] = skillLevelData[j]
                for k = 1,#skillBaseData do
                    if skillBaseData[k].id == skillLevelData[j].base_id then
                        self._skillTable[i].icon = skillBaseData[k].icon
                        self._skillTable[i].sound = skillBaseData[k].sound
                        self._skillTable[i].skill_type = skillBaseData[k].skill_type
                        self._skillTable[i].attack_type = skillBaseData[k].attack_type
                        self._skillTable[i].use_distance = skillBaseData[k].use_distance
                        self._skillTable[i].attack_style = skillBaseData[k].attack_style
                        self._skillTable[i].hurt_range = skillBaseData[k].hurt_range
                        break
                    end
                end

                break
            end
        end
    end

    -- 主技能背景
    local primaryNode = cc.Sprite:create("skill/primary.png")
    local position = cc.p(origin.x + visibleSize.width - primaryNode:getContentSize().width/2 - 60, origin.y + primaryNode:getContentSize().height/2 + 60)
    primaryNode:setPosition(position)
    primaryNode:setAnchorPoint(cc.p(0.5,0.5))
    self:addChild(primaryNode)

    local posX,posY = primaryNode:getPosition()
    local rad = 0
    for i= 1,#self._skillTable do

        --技能基础数据
        local userSkill =  self._skillTable[i]

        --技能图标
        local skillSprite = ccui.Button:create("skill/icon/"..userSkill.icon..".png")
        skillSprite:setTitleText(userSkill.name)
        skillSprite:setTitleFontSize(18)

        local skillSpriteCD = cc.ProgressTimer:create(cc.Sprite:create("skill/shadow.png"))
        skillSpriteCD:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
        skillSpriteCD:setPercentage(0)

        self._skillCDSpriteTable[i] = skillSpriteCD

        if i == 1 then
            skillSprite:setPosition(primaryNode:getContentSize().width/2,primaryNode:getContentSize().height/2)
            skillSprite:setAnchorPoint(cc.p(0.5,0.5))
            primaryNode:addChild(skillSprite)

            skillSpriteCD:setPosition(primaryNode:getContentSize().width/2,primaryNode:getContentSize().height/2)
            skillSpriteCD:setAnchorPoint(cc.p(0.5,0.5))
            primaryNode:addChild(skillSpriteCD)

        else
            local radius = 160
            local k = i
            --另外九个技能
            if i>=2 and i<=5 then
                if i==2 or i==5 then
                    radius = 180
                else
                    radius = 160
                end
                k = i
                rad = (34 + 40 * (k-1)) * math.pi /180
            elseif  i>=6 and i<=10 then
                if i==6 or i==10 then
                    radius = 300
                else
                    radius = 280
                end
                k = i - 4
                rad = (56 + 26 * (k-1)) * math.pi /180

            elseif  i>=11 and i<=17 then
                if i==11 or i==17 then
                    radius = 420
                else
                    radius = 400
                end
                k = i - 9
                rad = (70 + 16 * (k-1)) * math.pi /180
            end

            --技能背景
            local secondaryNode = nil
            if i>=2 and i<=5 then
                secondaryNode = cc.Sprite:create("skill/secondary.png")
            else
                secondaryNode = cc.Sprite:create("skill/third.png")
            end
            local position = cc.p(radius * math.cos(rad) + posX, radius * math.sin(rad) + posY)
            secondaryNode:setPosition(position)
            secondaryNode:setAnchorPoint(cc.p(0.5,0.5))
            self:addChild(secondaryNode)

            skillSprite:setPosition(secondaryNode:getContentSize().width/2,secondaryNode:getContentSize().height/2)
            skillSprite:setAnchorPoint(cc.p(0.5,0.5))
            secondaryNode:addChild(skillSprite)

            skillSpriteCD:setPosition(secondaryNode:getContentSize().width/2,secondaryNode:getContentSize().height/2)
            skillSpriteCD:setAnchorPoint(cc.p(0.5,0.5))
            secondaryNode:addChild(skillSpriteCD)
        end

        skillSprite:addTouchEventListener(function(sender,eventType)
            if eventType == ccui.TouchEventType.began then
                skillSprite:setOpacity(150)

            elseif eventType == ccui.TouchEventType.moved then

            elseif eventType == ccui.TouchEventType.ended then

                skillSprite:setOpacity(255)

                -- 冷却
                if  not self._skillTable[i].is_cool_time then
                    self._mapLayer:onSkillTouch(self._skillTable[i],function()
                        self._skillTable[i].is_cool_time = true
                        self._skillCDSpriteTable[i]:setPercentage(100)

                        local sequenceCD = cc.Sequence:create(cc.ProgressTo:create(self._skillTable[i].cool_time/1000, 0),cc.CallFunc:create(function()
                            self._skillTable[i].is_cool_time = false
                            self._skillCDSpriteTable[i]:setPercentage(0)
                        end))
                        self._skillCDSpriteTable[i]:runAction(sequenceCD)
                    end)
                else
                    cclog("技能冷却中 .......")
                    UIHelper.showToast(self,"技能冷却中")
                end

            elseif eventType == ccui.TouchEventType.canceled then
                skillSprite:setOpacity(255)
            end
        end)



    end


    --触摸处理函数
    --[[
    local function onTouchBegan( touch,event )

        cclog("Skill Layer onTouchBegan")
        local rect   = cc.rect(0, 0, 500, 500)
        if cc.rectContainsPoint(rect, touch:getLocation()) then
            return true
        end
        return false

    end

    local function onTouchEnded(touch,event)
        local target = event:getCurrentTarget()
        return false
    end

    local function onTouchMoved(touch,event )
        --local target = event:getCurrentTarget()
        --local x,y = target:getPosition()
        --target:setPosition(cc.p(x + touch:getDelta().x, y + touch:getDelta().y))
    end

    local function onTouchCancelled(touch,event)
        local target = event:getCurrentTarget()
    end

    -- 创建触摸监听事件
    local listener = cc.EventListenerTouchOneByOne:create()
    -- 吞没事件
    --listener:setSwallowTouches(true);
    --将触摸结束事件与处理函数绑定
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(onTouchCancelled,cc.Handler.EVENT_TOUCH_CANCELLED)

    --获取eventDispatcher
    local eventDispatcher = self:getEventDispatcher()
    --将监听事件绑定到精灵上
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self)
    ]]--

end

function SkillLayer:startAutoAttack()
    self._autoAttackOpen = true
    self._autoAttack:setBrightStyle(1)
    self._mapLayer._playerSprite:startAutoAttack(self)

    -- 宠物自动攻击
    for i = 1,#self._mapLayer._petTable do
        self._mapLayer._petTable[i]:startAutoAttack(nil)
    end

end

function SkillLayer:closeAutoAttack()
    self._autoAttackOpen = false
    self._autoAttack:setBrightStyle(0)
    self._mapLayer._playerSprite:closeAutoAttack()
end