
-- cclog
cclog = function(...)
    -- 在logcat中打印用release_print
    release_print(string.format(...))
end


Helper = {
    performWithDelay = nil,
    schedule = nil,
    getRad = nil,
    getSpriteRad = nil,
    getSpriteFrameProperty = nil,
    getCollisionRect = nil,
    randomPosition = nil,
    positionToMapPosition = nil,
    positionToMapCoordinate = nil,
    mapCoordinateToPosition = nil,
    computeHurtRect = nil,
    getDirection = nil,
    getMapDirection = nil,
    getDistance = nil,
    getAnimFrames = nil
}

Helper.performWithDelay = function(node, callback, delay)
    local delay = cc.DelayTime:create(delay)
    local sequence = cc.Sequence:create(delay, cc.CallFunc:create(callback))
    node:runAction(sequence)
end

Helper.schedule = function(node, callback, delay,tag)
    local delay = cc.DelayTime:create(delay)
    local sequence = cc.Sequence:create(delay, cc.CallFunc:create(callback))
    local action = cc.RepeatForever:create(sequence)
    node:runAction(action)
    if tag > 0 then
        action:setTag(tag)
    end

end

--获取以p1为圆心，p2p1与x轴的弧度值
Helper.getRad = function(p1,p2)
    local xx = p2.x - p1.x;
    local yy = p2.y - p1.y;
    --斜边
    local xie = math.sqrt(math.pow(xx, 2) + math.pow(yy, 2));
    --yy >= 0 弧度在于 0 到 π 之间。(0~180°)
    --yy < 0 弧度在于 π 到 2π 之间。(180°~360°)
    if yy >= 0 then
        return math.acos(xx / xie)
    else
        return math.pi * 2 - math.acos(xx / xie)
    end
end

Helper.getDistance = function(p1,p2)
    local xx = p2.x - p1.x;
    local yy = p2.y - p1.y;
    return math.sqrt(math.pow(xx, 2) + math.pow(yy, 2));
end

Helper.getCollisionRect = function(position)
    return cc.rect(position.x - TileWidth/2,position.y - TileHeight/2,TileWidth,TileHeight)
end


Helper.getSpriteRad = function(direction)

    local rad = 0
    if direction == DirectionType.RightType then
        rad = 0
    elseif direction == DirectionType.RightUpType then
        rad = 45 * math.pi /180
    elseif direction == DirectionType.UpType then
        rad = 90 * math.pi /180
    elseif direction == DirectionType.LeftUpType then
        rad = 135 * math.pi /180
    elseif direction == DirectionType.LeftType then
        rad = math.pi
    elseif direction == DirectionType.LeftDownType then
        rad = 225 * math.pi /180
    elseif direction == DirectionType.DownType then
        rad = 270 * math.pi /180
    elseif direction == DirectionType.RightDownType then
        rad = 315 * math.pi /180
    end

    return rad
end

--role plist的方向
Helper.getSpriteFrameProperty = function(direction)

    local rad = 0
    local rolePngName = 0
    local flippedX = false
    if direction == DirectionType.RightType then
        rad = 0
        rolePngName = "200"
    elseif direction == DirectionType.RightUpType then
        rad = 45 * math.pi /180
        rolePngName = "100"
    elseif direction == DirectionType.UpType then
        rad = 90 * math.pi /180
        rolePngName = "000"
    elseif direction == DirectionType.LeftUpType then
        rad = 135 * math.pi /180
        rolePngName = "100"
        flippedX = true
    elseif direction == DirectionType.LeftType then
        rad = math.pi
        rolePngName = "200"
        flippedX = true
    elseif direction == DirectionType.LeftDownType then
        rad = 225 * math.pi /180
        rolePngName = "300"
        flippedX = true
    elseif direction == DirectionType.DownType then
        rad = 270 * math.pi /180
        rolePngName = "400"
    elseif direction == DirectionType.RightDownType then
        rad = 315 * math.pi /180
        rolePngName = "300"
    end

    return rad,rolePngName,flippedX
end

Helper.getDirection = function(p1,p2)
    local p = {}
    p.x = p2.x - p1.x
    p.y = p2.y - p1.y
    local angle = math.atan2(p.y,p.x) * 180/math.pi
    if angle < 0 then
        angle = 360 + angle
    end
    cclog("目标与玩家的角度：" .. angle)
    local direction = DirectionType.UpType
    if angle >= 337.5 or angle <= 22.5 then
        direction =  DirectionType.RightType
    elseif angle > 22.5 and angle <=67.5 then
        direction =  DirectionType.RightUpType
    elseif angle > 67.5 and angle <= 112.5 then
        direction =  DirectionType.UpType
    elseif angle > 112.5 and angle <= 157.5 then
        direction =  DirectionType.LeftUpType
    elseif angle > 157.5 and angle <= 202.5 then
        direction =  DirectionType.LeftType
    elseif angle > 202.5 and angle <= 247.5 then
        direction =  DirectionType.LeftDownType
    elseif angle > 247.5 and angle <= 292.5 then
        direction =  DirectionType.DownType
    elseif angle > 292.5 and angle <= 337.5 then
        direction =  DirectionType.RightDownType
    end
    cclog("目标与玩家的方向：" .. direction)
    return direction
end


Helper.getMapDirection = function(p1,p2)
    local p = {}
    p.x = p2.x - p1.x
    p.y = p2.y - p1.y
    local angle = math.atan2(p.y,p.x)*180/math.pi
    if angle < 0 then
        angle = 360 + angle
    end
    cclog("目标与玩家的角度：" .. angle)
    local direction = DirectionType.UpType
    if angle >= 337.5 or angle <= 22.5 then
        direction =  DirectionType.RightType
    elseif angle > 22.5 and angle <=67.5 then
        direction =  DirectionType.RightDownType
    elseif angle > 67.5 and angle <= 112.5 then
        direction =  DirectionType.DownType
    elseif angle > 112.5 and angle <= 157.5 then
        direction =  DirectionType.LeftDownType
    elseif angle > 157.5 and angle <= 202.5 then
        direction =  DirectionType.LeftType
    elseif angle > 202.5 and angle <= 247.5 then
        direction =  DirectionType.LeftUpType
    elseif angle > 247.5 and angle <= 292.5 then
        direction =  DirectionType.UpType
    elseif angle > 292.5 and angle <= 337.5 then
        direction =  DirectionType.RightUpType
    end
    cclog("目标与玩家的方向：" .. direction)
    return direction
end

----技能伤害范围计算
----node是技能特效 最后的位置
Helper.computeHurtRect = function(position,direction,skill)
    --预判技能伤害范围
    local hurtRect = {}

    if skill.attack_style == AttackStyle.attack or skill.attack_style == AttackStyle.attackMove then

        local rad = Helper.getSpriteFrameProperty(direction)
        --往这个方向 生成一串矩形

        local rect   = cc.rect(position.x - TileWidth/2, position.y - TileHeight/2, TileWidth, TileHeight)
        table.insert(hurtRect,rect)

        for i = 1,skill.hurt_range do

            local stepX = math.cos(rad)  *  i * TileWidth
            local stepY = math.sin(rad)  *  i * TileHeight

            local rect   = cc.rect(position.x + stepX - TileWidth/2, position.y + stepY - TileHeight/2, TileWidth, TileHeight)
            table.insert(hurtRect,rect)
        end
    elseif skill.attack_style == AttackStyle.casting
            or skill.attack_style == AttackStyle.attackSurround
            or skill.attack_style == AttackStyle.castingSurround
            or skill.attack_style == AttackStyle.castingDuration then

        --生成周围矩形
        local width = ((skill.hurt_range-1) * 2 + 1) * TileWidth
        local height = ((skill.hurt_range-1) * 2 + 1) * TileHeight

        local rect   = cc.rect(position.x - width/2, position.y - height/2, width, height)
        table.insert(hurtRect,rect)
    end

    return hurtRect
end

--随机位置
Helper.randomPosition = function(position,radius)
    local randomX = math.random(0, radius) - radius
    local randomY = math.random(0, radius) - radius
    return cc.p(position.x + randomX,position.y + randomY)
end

--本地坐标左下角(0,0)转换地图块坐标 左上角为(0,0)            --floor +0.5 向下取整 四舍五入   ceil向上取整
Helper.positionToMapCoordinate = function(position,mapSize)
    local x = math.floor(position.x / TileWidth);
    local y = math.floor(((mapSize.height * TileHeight) - position.y) / TileHeight);
    return cc.p(x,y)
end

Helper.mapCoordinateToPosition = function(position,mapSize)
    local x = (position.x + 1) * TileWidth - TileWidth/2;
    local y = (mapSize.height - position.y - 1) * TileHeight + TileHeight/2;
    return cc.p(x,y)
end

Helper.positionToMapPosition = function(position,mapSize)
    local x = position.x / TileWidth;
    local y = ((mapSize.height * TileHeight) - position.y) / TileHeight
    return cc.p(x,y)
end

Helper.getAnimFrames = function(path,count)
    local cache = cc.SpriteFrameCache:getInstance()
    local animFrames = {}
    for j = 1,count do
        local frame = cache:getSpriteFrame( string.format(path .. "%02d.png", j-1) )
        if frame then
            animFrames[j] = frame
        end
    end
    return animFrames
end